import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, BatchNormalization
from tensorflow.keras import backend as K
import os
import logging
import pickle
import numpy as np
from sklearn.preprocessing import OneHotEncoder

logger = logging.getLogger('root')

# número de fatures para os pesos treinados
N_FEATURES = 1000
# Número de classes para a os pesos treinados
N_CLASSES = 27

class TinyRNA:
    def __init__(self):
        # the folder in which the model and weights are stored
        # self.model_folder = os.path.join(os.path.abspath("src"), "static")
        cur_path = os.path.dirname(__file__)
        self.weights_folder = os.path.join(cur_path, 'weights')
        self.classes_encoded_folder = os.path.join(cur_path, 'classes_encoded')

        self.model = None
        self.encoder = None

        self.input_dim = None
        self.n_classes = None

        self.pretrained_models = {
            'tiny_rna' : {
                'classes' : 'classes_operacao_credito_processo_licitatorio_juridico.pkl',
                'weigths' : 'tiny_rna.h5'
            }
        }
        
        logging.info("[INFO][TinyRNA] Rede inicializada")

    def build(self, input_dim, n_classes, n_filters=16, dropout=0.2):
        self.input_dim = input_dim
        self.n_classes = n_classes

        model = Sequential()
        model.add(Dense(n_filters, input_dim=input_dim, activation='relu'))
        model.add(Dropout(dropout))
        model.add(BatchNormalization())

        model.add(Dense(n_classes, activation='softmax'))

        self.model = model

    def load(self, model_name):
        """
        :param model_name: nome do modelo pretreinado
        :return:
        """
        if model_name not in self.pretrained_models:
            raise Exception('Modelo {} desconhecido'.format(model_name))
            
        weights_name = self.pretrained_models[model_name]['weigths']
        weights_path = os.path.join(self.weights_folder, weights_name)
        self.model.load_weights(weights_path)

        self.load_encoder(model_name)

        logging.info("Neural Network loaded: ")
        logging.info('\t' + "Neural Network weights: " + weights_name)
        return True

    def load_encoder(self, model_name):
        if model_name not in self.pretrained_models:
            raise Exception('Encoder {} desconhecido'.format(model_name))

        encoder_name = self.pretrained_models[model_name]['classes']
        encoder_name = os.path.join(self.classes_encoded_folder, encoder_name)

        self.encoder = OneHotEncoder(sparse=False)
        self.encoder.categories_ = pickle.load(open(encoder_name, 'rb'))

    def predict(self, X):
        y = self.model.predict(X)
        return y
        
    def decode_predictions(self, predictions):

        predictions_decoded = []
        for prediction in predictions:
            
            decoded = {}
            prediction_decoded = self.decode_prediction(prediction)
            decoded['all'] = prediction_decoded
            decoded['max'] =  {
                'label': self.encoder.inverse_transform(prediction.reshape(1, -1))[0][0],
                'probability' : float(prediction.max())
            }

            predictions_decoded.append(decoded)
            
        return predictions_decoded
    
    def decode_prediction(self, prediction):
        predictions_decoded = []
        for i, prob in enumerate(prediction):
            label_pred = np.zeros(self.n_classes)
            label_pred[i] = 1

            label_text = self.encoder.inverse_transform([ label_pred ])
            prediction_decoded = {'label' : label_text[0][0], 'probability': float(prob)}
            predictions_decoded.append(prediction_decoded)

        return predictions_decoded

    def get_base_model(self):
        """Recupera o modelo base, encapsulado pela classe

        Returns:
            Model: modelo (keras) base
        """
        return self.model

def init_base_model():
    model = TinyRNA()
    model.build(N_FEATURES, N_CLASSES)
    model.load('tiny_rna')
    
    return model