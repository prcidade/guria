from sys import maxsize
import cv2
import numpy as np
from PIL import Image

IMAGE_MAX_SIZE = 14336

def load_img(path):
    """Carrega uma imagem do disco para a memória

    Args:
        path (str): Caminho para a imagem

    Returns:
        PIL.Image: Imagem carregada na memória
    """
    img = cv2.imread(path)
    if img is None:
        raise FileNotFoundError('Imagem não encontrada')
    
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img

def convert_to_grayscale(image):
    """Converte uma imagem RGB para escala de cinza

    Args:
        image (PIL.Image): Imagem para ser convertida

    Returns:
        PIL.Image: Imagem em tons de cinza
    """
    return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

def rotate_bound(image, angle):
    """Rotaciona a imagem em um dado ângulo

    Args:
        image (np.array): Imagem a ser rotacionada
        angle (float): angulo de rotação

    Returns:
        np.array: imagem rotacionada
    """
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)
   
    M = cv2.getRotationMatrix2D((cX, cY), angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
   
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
   
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
   
    return cv2.warpAffine(image, M, (nW, nH))


def show_img(img, size=(300, 400), title="Show Image", wait_key=True):
    """Exibe uma imagem em um frame de memória

    Args:
        img (np.array): Imagem para ser exibida
        size (tuple, optional): Tamanho da janela de exibição. Defaults to (300, 400).
        title (str, optional): Título da janela de de exibição. Defaults to "Show Image".
        wait_key (bool, optional): Flag indicando que o sistema deverá aguardar a interação do usuário para fechar. Defaults to True.
    """
    
    img_temp = cv2.resize(img, size) # Resize image
    cv2.imshow(title, img_temp)   # Show image
    
    if wait_key:
        cv2.waitKey(0)   

def resize_img(img, size):
    """Redimensiona uma imagem para o tamanho informado.

    Args:
        img (np.array): imagem pra ser redimensionada
        size (tuple): Tamanho para redimensionar a imagem

    Returns:
        PIL.Image: Imagem redimensionada
    """
    img_temp = cv2.resize(img, size) # Resize image

    return img_temp

def thumbnail(img, size):
    """Redimensiona uma imagem respeitando o aspect-radio da imagem.

    Args:
        img (PIL.Image): imagem para ser redimensionada
        size (tuple): tamanho para redimensionar a imagem

    Returns:
        PIL.Image: Imagem redimensionada
    """
    img_temp = np.asarray(img).copy()
    img_temp = Image.fromarray(img)

    img_temp.thumbnail(size)

    return img_temp

def is_img_gt_max_size(img):
    max_size = get_img_max_size()
    if img.shape[0] > max_size or img.shape[1] > max_size:
        return True
    return False

def get_img_max_size():
    """Recupera o tamanho máximo padão das imagens.

    Returns:
        int: Dimensão máxima da imagem
    """
    return IMAGE_MAX_SIZE
