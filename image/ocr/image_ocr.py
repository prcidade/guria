import cv2 
import pytesseract
import re


def get_angle_text_orientation(img, ignore_errors=False):
    """Recupera o angulo da orientação do texto

    Args:
        img (Image): imagem para ter o angulo do texto analisado 

    Returns:
        float: angulo de rotação do texto
    """

    try:
        newdata = pytesseract.image_to_osd(img)
        angle = re.search(r'(?<=Rotate: )\d+', newdata).group(0)
    
        return float(angle)
    except:
        if ignore_errors:
            return 0.0

        raise

    
def get_text_from_image(img):
    """Recupera o texto da imagem

    Args:
        img (Image): imagem para se extrair o texto

    Returns:
        str: texto extraído
    """
    custom_config = '-l por'
    text = pytesseract.image_to_string(img, config=custom_config)
    return text
