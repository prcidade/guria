""" Pipeline para preparar os dados para treinamento. 
Utiliza threads para realizar as operações em paralelo, já que alguns processos podem demandar algum tempo.

O termo projeto é utilizado para identificar uma entrada de informação pertinente ao ciclo de vida do projeto,
podendo ser Operação de Crédito, Processo Licitatório (Jurídico), Processo Licitatório (Técnico).

O termo modulo especifica qual parte do ciclo de projeto a informação diz respeito.

O Pipeline enfilera os itens a serem processados e o passo seguinte utiliza a fila como entrada do processo.

Fluxo: 
    1 - Download dos arquivos:
        São carregados, através dos dataframes, os projetos já processados
        Através da API do Portal dos Municípios são carregados todos os projetos disponíveis
        São filtrados os novos projetos para download
        Cada item baixado é enfileirado para ser processado pelo próximo passo.
    2 - Descompactação:
        Os itens baixados e enfileirados são descompactados.
        Cada item descompactado é enfileirado, contendo o caminho da pasta descompactada e o modulo que diz respeito
        A Descompactação continua enquanto houverem itens na fila.
    3 - Converter para imagem:
        Os itens descompactados são convertidos para imagens.
        Para cada conversão é enfileirada a informação da pasta em que foram salvas as imagens e o módulo que diz respeito
        A conversão continua enquanto houverem itens na fila.
    4 - Extrair texto das imagens:
        Cada item enfileirado do passo anterior passará pelo processo de extração
        As informações extraídas enfileiradas para serem escritas em disco, em um arquivo CSV
    5 - Escrita em disco
        Os textos extraídos são escritos em um arquivo CSV
"""

import download_documents
import unzip_files
import convert_files_to_dataframe
import process_dataframe

import concurrent.futures
import queue
import threading
import logging
import pandas as pd
import csv

from datetime import date
import time
import random 

import os
import sys
sys.path.append('../')

CLEAR_OUTPUT_DIR_BEFORE_START = False
MAX_QUEUE_SIZE = 30

NUM_THREADS_DESCOMPACTAR = 1
NUM_THREADS_CONVERT_TO_IMAGE = 1
NUM_THREADS_CONVERT_TO_TEXT = 4
NUM_THREADS_CONVERT_TO_DATAFRAME = 1
NUM_MAX_WORKERS =  NUM_THREADS_DESCOMPACTAR + NUM_THREADS_CONVERT_TO_IMAGE + NUM_THREADS_CONVERT_TO_TEXT + NUM_THREADS_CONVERT_TO_DATAFRAME + 1


NAME_MODULE_OPERACAO_CREDITO = 'operacao_credito'
NAME_MODULE_PROCESSO_LICITATORIO = 'processo_licitatorio'
NAME_MODULE_PROCESSO_LICITATORIO_JURIDICO = 'processo_licitatorio_juridico' 
NAME_MODULE_PROCESSO_LICITATORIO_TECNICO = 'processo_licitatorio_tecnico' 

OUTPUT_DATAFRAME_DIR = '../../dataset/dataframe/raw' 
OUTPUT_DATAFRAME = os.path.join(OUTPUT_DATAFRAME_DIR, '{}_raw_dataframe.csv'.format(date.today().strftime('%Y-%m-%d')))

def baixar_arquivos(queue_downloaded_files, event_download_complete):
    """Carrega as Operações de Crédito e Processos Licitatórios que atendam os critérios para uso no treinamento e
        baixa os arquivos do Portal dos Municípios e coloca em uma fila para serem descompactados
        Arquivos já baixados são desconsiderados
    Args:
        queue_downloaded_files (Queue): Fila de arquivos baixados para serem descompactados
        event_download_complete (Event): Evento indicando que o download dos arquivos está completo
    """
    
    logging.info('Carregando informações já processadas')

    df = process_dataframe.load_all_dataframes(ignore_errors=True)

    logging.info('Iniciando downloads')
    operacoes_credito_disponiveis = download_documents.get_operacoes_credito_disponiveis()
    operacoes_credito_utilizadas = df[df['modulo'] == NAME_MODULE_OPERACAO_CREDITO]['projeto'].unique().tolist()
    operacoes_credito_novas = download_documents.filtrar_operacoes_credito_novas(operacoes_credito_disponiveis, operacoes_credito_utilizadas)    

    logging.info('[API] Operações de Crédito disponíveis: {}'.format(len(operacoes_credito_disponiveis)))
    logging.info('[API] Operações de Crédito já utilizadas: {}'.format(len(operacoes_credito_utilizadas)))
    logging.info('[API] Novas Operações de Crédito: {}'.format(len(operacoes_credito_novas)))
    
    for operacao_credito_nova in operacoes_credito_novas:

        logging.info('[Baixando]: {}'.format(operacao_credito_nova['url']))
        file_path = download_documents.baixar_documentos_nova_entrada_para(operacao_credito_nova, download_documents.OUTPUT_PATH_DOWNLOAD_OPERACAO_CREDITO)
        logging.info('[Baixado]: {}'.format(file_path))
        queue_downloaded_files.put({
            'file_path': file_path, 
            'unzip_to': unzip_files.DEFAULT_UNZIP_DIR_OPERACAO_CREDITO,
            'module': NAME_MODULE_OPERACAO_CREDITO,
        })

    processos_licitatorios_disponiveis = download_documents.get_processos_licitatorios_disponiveis()
    processos_licitatorios_utilizados = df[df['modulo'] == NAME_MODULE_PROCESSO_LICITATORIO_JURIDICO | df['modulo'] == NAME_MODULE_PROCESSO_LICITATORIO_TECNICO]['projeto'].unique().tolist()
    processos_licitatorios_novos = download_documents.filtrar_processos_licitatorios_novos(processos_licitatorios_disponiveis, processos_licitatorios_utilizados)

    logging.info('[API] Processos Licitatórios disponíveis: {}'.format(len(processos_licitatorios_disponiveis)))
    logging.info('[API] Processos Licitatórios já utilizados: {}'.format(len(processos_licitatorios_utilizados)))
    logging.info('[API] Novos Processos Licitatórios: {}'.format(len(processos_licitatorios_novos)))
    
    for processo_licitatorio_novo in processos_licitatorios_novos:

        logging.info('[Baixando]: {}'.format(processo_licitatorio_novo['url']))
        file_path = download_documents.baixar_documentos_nova_entrada_para(processo_licitatorio_novo, download_documents.OUTPUT_PATH_DOWNLOAD_PROCESSO_LICITATORIO)
        logging.info('[Baixado]: {}'.format(file_path))
        queue_downloaded_files.put({
            'file_path': file_path, 
            'unzip_to': unzip_files.DEFAULT_UNZIP_DIR_PROCESSO_LICITATORIO,
            'module': NAME_MODULE_PROCESSO_LICITATORIO,
        })
        
    logging.info('[INFO] Fim dos downloads')
    event_download_complete.set()

def descompactar_arquivo(queue_downloaded_files, queue_unziped_files, event_download_complete, event_unzip_complete):
    """Descompacta os arquivos baixados para um diretório e enfilera-os para serem convertidos em imagens

    Args:
        queue_downloaded_files (Queue): Fila de arquivos baixados para serem descompactados
        queue_unziped_files (Queue): Fila de arquivos descompactados, prontos para serem convertidos em imagens
        event_download_complete (Event): Evento indicando que não haverá novos arquivos para serem baixados
        event_unzip_complete (Event): Evento indicando a conclusão da descompactação dos arquivos
    """
    while not event_download_complete.is_set() and not queue_downloaded_files.empty():
        ziped_info = queue_downloaded_files.get() 

        try:

            logging.info('[Descompactando]: {} - Qsize: {}'.format(ziped_info['file_path'], queue_downloaded_files.qsize()))
            unziped_to_folder = unzip_files.unzip(ziped_info['file_path'], ziped_info['unzip_to'], create_new_folder=True)
            queue_unziped_files.put({
                    'unziped_folder': unziped_to_folder,
                    'module': ziped_info['module']
            })

        except:
            logging.exception('[ERROR][descompactar_arquivo] {}'.format(ziped_info['file_path']))
        
        queue_downloaded_files.task_done()

    logging.info('[INFO] Fim da descompactação')
    event_unzip_complete.set()


def convert_to_image(queue_unziped_files, queue_images_files, event_unzip_complete, event_convert_files_to_images_complete):
    """Conversão dos arquivos descompactados em imagens;
    Após a conversão os arquivos são enfileirados para ter seu texto extraído

    Args:
        queue_unziped_files (Queue): Fila de arquivos descompactados
        queue_images_files (Queue): Fila de imagens convertidas para ter o texto extraído
        event_unzip_complete (Event): Evento indicando que não há mais arquivos para serem descompactados
        event_convert_files_to_images_complete (Event): Evento indicando que todos arquivos foram
    """
    while not event_unzip_complete.is_set() and not queue_unziped_files.empty():
        unziped_folder_info = queue_unziped_files.get()

        try:    
            unziped_folder = unziped_folder_info['unziped_folder']
            logging.info('[Convertendo]: {} - Qsize: {}'.format(unziped_folder, queue_unziped_files.qsize()))

            # Extrai o nome da pasta para descompactar a partir do zip, desconsiderando a extensão
            output_folder_name = os.path.basename(os.path.normpath(unziped_folder)).split('.')[0]

            # ajusta o diretório de saída das imagens
            if unziped_folder_info['module'] == NAME_MODULE_OPERACAO_CREDITO:
                output_folder = os.path.join(convert_files_to_dataframe.DEFAULT_OUTPUT_DIR_IMAGES_OPERACAO_CREDITO, output_folder_name)

                convert_files_to_dataframe.convert_files_to_image(unziped_folder, output_folder)
                logging.info('[Arquivos Convertidos] {}'.format(output_folder))

                queue_images_files.put({
                    'folder': output_folder,
                    'module': unziped_folder_info['module']
                })

            else:
                output_folder = os.path.join(convert_files_to_dataframe.DEFAULT_OUTPUT_DIR_IMAGES_PROCESSO_LICITATORIO_JURIDICO, output_folder_name)
                unziped_folder = os.path.join(unziped_folder, 'juridico')

                convert_files_to_dataframe.convert_files_to_image(unziped_folder, output_folder)
                logging.info('[Arquivos Convertidos] {}'.format(output_folder))

                queue_images_files.put({
                    'folder': output_folder,
                    'module': unziped_folder_info['module']
                })

                # Documentação Técnica
                output_folder = os.path.join(convert_files_to_dataframe.DEFAULT_OUTPUT_DIR_IMAGES_PROCESSO_LICITATORIO_TECNICO, output_folder_name)
                unziped_folder = os.path.join(unziped_folder, 'juridico')

                convert_files_to_dataframe.convert_files_to_image(unziped_folder, output_folder)
                logging.info('[Arquivos Convertidos] {}'.format(output_folder))

                queue_images_files.put({
                    'folder': output_folder,
                    'module': unziped_folder_info['module']
                })

            logging.info('[Arquivos Convertidos] Aguardando OCR: {}'.format(queue_images_files.qsize()))

        except:
            logging.exception('[ERROR][convert_to_image]')

        queue_unziped_files.task_done()

    logging.info('[INFO] Conversão de arquivos completa')
    event_convert_files_to_images_complete.set()


def convert_images_to_text(queue_images_files, queue_text_extracted, event_convert_files_to_images_complete, event_extract_text_complete):
    """Converte as imagens para texto. Se os texto já estiver escrito no dataframe de saída a extração é ignorada. 

    Args:
        queue_images_files (Queue): Fila de imagens para ter o texto extraído
        queue_text_extracted (Queue): Fila
        event_convert_files_to_images_complete ([type]): [description]
        event_extract_text_complete ([type]): [description]
    """

    df = process_dataframe.load_all_dataframes(ignore_errors=True)
    

    while not event_convert_files_to_images_complete.is_set() and not queue_images_files.empty():
        image_data = queue_images_files.get()
        try:

            folder_documentos = image_data['folder']
            modulo = image_data['module']
            projeto = os.path.basename(os.path.normpath(folder_documentos)).split('.')[0]

            # Verifica se a imagem já teve o texto extraído
            is_convertido = df[(df['modulo'] == modulo) & (df['projeto'] == projeto) ].any().any()

            if not is_convertido:
                logging.exception('[Texto] Imagem para texto: {} - Qsize: {}'.format(folder_documentos, queue_images_files.qsize()))
                data = convert_files_to_dataframe.extract_text_from_project(folder_documentos, projeto, modulo)

                queue_text_extracted.put(data)


        except:
            logging.exception('[ERROR][convert_images_to_text]')
        
        queue_images_files.task_done()

    logging.info('[INFO] Todos textos extraídos')
    event_extract_text_complete.set()

def write_text_to_dataframe(queue_text_extracted, event_extract_text_complete):
    """Grava o texto extraído em um arquivo CSV. 
    Apesar do alto número de IO, gravar o texto extraído evita que seja necessário reprocessar todo o documento

    Args:
        queue_text_extracted (Queue): Fila com o texto para ser gravado
        event_extract_text_complete (Event): Evento indicando que não haverá mais texto extraído
    """
    
    is_new_file = not os.path.exists(OUTPUT_DATAFRAME)
    with open(OUTPUT_DATAFRAME, 'a+', encoding='utf-8') as f:

        writer = csv.DictWriter(f, fieldnames=process_dataframe.CSV_HEADER)
        if is_new_file:
            writer.writeheader()

        while not event_extract_text_complete.is_set() or not queue_text_extracted.empty():
            data = queue_text_extracted.get()

            writer.writerows(data)

        queue_text_extracted.task_done()

    logging.info('[INFO] Dataframe final salvo em: {}'.format(OUTPUT_DATAFRAME))


if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    if CLEAR_OUTPUT_DIR_BEFORE_START:
        download_documents.clear_output_dir()
    

    if not os.path.exists(OUTPUT_DATAFRAME_DIR):
        os.makedirs(OUTPUT_DATAFRAME_DIR)

    queue_downloaded_files = queue.Queue(maxsize=MAX_QUEUE_SIZE)
    queue_unziped_files = queue.Queue(maxsize=MAX_QUEUE_SIZE)
    queue_images_files = queue.Queue(maxsize=MAX_QUEUE_SIZE)
    queue_text_extracted = queue.Queue(maxsize=MAX_QUEUE_SIZE)

    event_download_complete = threading.Event()
    event_unzip_complete = threading.Event()
    event_convert_files_to_images_complete = threading.Event()
    event_extract_text_complete = threading.Event()

    threads = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=NUM_MAX_WORKERS) as executor:
        
        executor.submit(baixar_arquivos, queue_downloaded_files, event_download_complete)
        
        for i in range(NUM_THREADS_DESCOMPACTAR):
            threads.append(
                executor.submit(descompactar_arquivo, queue_downloaded_files, queue_unziped_files, event_download_complete, event_unzip_complete)
            )

        for i in range(NUM_THREADS_CONVERT_TO_IMAGE):
            threads.append(
                executor.submit(convert_to_image, queue_unziped_files, queue_images_files, event_unzip_complete, event_convert_files_to_images_complete)
            )

        for i in range(NUM_THREADS_CONVERT_TO_TEXT):
            threads.append(
                executor.submit(convert_images_to_text, queue_images_files, queue_text_extracted, event_convert_files_to_images_complete, event_extract_text_complete)
            )

        for i in range(NUM_THREADS_CONVERT_TO_DATAFRAME):
            threads.append(
                executor.submit(write_text_to_dataframe, queue_text_extracted, event_extract_text_complete)
            )

        for t in threads:
            t.result()
    
    logging.info('Execução completa')
