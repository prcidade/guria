""" Realiza a limpeza, correções, processamento e operações básicas sobre os dataframes"""
import pandas as pd
import janitor
import pandas_flavor as pf
import numpy as np
from glob import glob
import os
import sys
from datetime import date
import json

sys.path.append('../')
from text.process import text_processor

import argparse
import logging

DEFAULT_DIR_DATAFRAME = os.path.join('..', '..', 'dataset', 'dataframe')
DEFAULT_DIR_RAW_DATAFRAME = os.path.join(DEFAULT_DIR_DATAFRAME, 'raw')
DEFAULT_DIR_PROCESSED_DATAFRAME = os.path.join(DEFAULT_DIR_DATAFRAME, 'processed')
DEFAULT_DIR_TRAIN_DATAFRAME = os.path.join(DEFAULT_DIR_DATAFRAME, 'train')

# Cabeçalho padrão dos arquivos CSV
CSV_HEADER = [ 'projeto', 'texto', 'tipo', 'modulo' ]
CSV_PROCESSED_HEADER = [
    'projeto', 
    'texto',	
    'tipo',	
    'modulo', 
    'texto_limpo', 
    'texto_len', 
    'texto_limpo_len', 
    'qtd_palavras_texto', 
    'qtd_palavras_texto_limpo'
]

def get_unique_projetos_from_csv(csv_path : str) -> list:
    """Recupera os projetos unicos a partir de um CSV
    O termo "projeto" é utilizado para identificar a fonte dos dados dentro do datraframe
    Mas pode ser referir a uma Operação de Crédito, Processo Licitatório, ou qualquer outro módulo do ciclo de vida do projeto

    Args:
        csv_path (str): caminho para o arquivo CSV

    Returns:
        list: Lista com os projetos únicos
    """
    
    df = load_dataframe(csv_path)

    return get_unique_from_dataframe(df, 'projeto')

def get_unique_from_dataframe(df, column_name : str) -> list:
    """Recupera os itens únicos de uma coluna do Dataframe 

    Args:
        df (Dataframe): Dataframe contendo as informações
        column_name (str): Nome da coluna para se recuperar as informações únicas

    Returns:
        list: lista contendo os itens únicos da coluna
    """
    return df[column_name].unique().tolist()


def load_all_dataframes(ignore_errors=False):
    """Carrega todos os dataframes disponíveis no diretório padrão do dataset

    Returns:
        pd.Dataframe : Dataframe com todos os CSV carregados
    """
    dataframes = []
    folders = os.listdir(DEFAULT_DIR_DATAFRAME)
    for folder in folders:
        # desconsidera arquivos na pasta raiz dos dataframes
        path = os.path.join(DEFAULT_DIR_DATAFRAME, folder)
        if os.path.isfile(path):
            continue
        try:
            dataframes.append(load_dataframes_in_dir(path, ignore_errors=ignore_errors))
        except:
            logging.exception('[process_dataframe][load_dataframe]')

    if len(dataframes) == 0:
        return pd.DataFrame(columns=CSV_HEADER)

    df = pd.concat(dataframes, axis=0, ignore_index=True, sort=False)
    
    return df 

def load_all_raw_dataframes(ignore_errors=False):
    """Carrega os dataframes sem processamento no diretório

    Args:
        ignore_errors (bool, optional): Se verdadeiro ignora os erros de leitura. Defaults to False.

    Returns:
        pd.Dataframe: Dataframe com os CSV do diretório
    """
    return load_dataframes_in_dir(DEFAULT_DIR_RAW_DATAFRAME, ignore_errors=ignore_errors)


def load_all_train_dataframes(ignore_errors=False):
    """Carrega os dataframes disponíveis no diretório de treino

    Args:
        ignore_errors (bool, optional): Se verdadeiro ignora os erros de leitura. Defaults to False.

    Returns:
        pd.Dataframe: Dataframe com os CSV do diretório
    """
    return load_dataframes_in_dir(DEFAULT_DIR_TRAIN_DATAFRAME, columns=CSV_PROCESSED_HEADER, ignore_errors=ignore_errors)

def load_all_processed_dataframes(ignore_errors=False):
    """Carrega os dataframes processados disponíveis no diretório 

    Args:
        ignore_errors (bool, optional): Se verdadeiro ignora os erros de leitura. Defaults to False.

    Returns:
        pd.Dataframe: Dataframe com os CSV do diretório
    """
    return load_dataframes_in_dir(DEFAULT_DIR_PROCESSED_DATAFRAME, columns=CSV_PROCESSED_HEADER, ignore_errors=ignore_errors)

def load_dataframes_in_dir(path, columns=CSV_HEADER, ignore_errors=False):
    """Carrega todos os dataframes salvos no diretórios de saída padrão para a memória em um único dataframe

    Returns:
        pd.Dataframe: Dataframe com todas as informações do diretório de saída
    """
    try:
        csv_files = glob(os.path.join(path, '*.csv'))
        dataframes = []
        for csv_file in csv_files:
            logging.info('Carregando {}'.format(csv_file))
            dataframes.append(load_dataframe(csv_file, columns=columns, ignore_errors=ignore_errors))
        
        if len(dataframes) > 0:
            df = pd.concat(dataframes, axis=0, ignore_index=True, sort=False)
        elif ignore_errors:
            return pd.DataFrame(columns=CSV_HEADER)
        else:
            raise ValueError('Não há dataframe no diretório: {}'.format(path))

        return df
    except:
        logging.exception('[process_dataframe][load_dataframes_in_dir] {}'.format(path))
        if ignore_errors:
            return pd.DataFrame(columns=CSV_HEADER)
        
        raise


def load_dataframe(csv_path, columns=CSV_HEADER, ignore_errors=False):
    """Carrega um dataframe a partir de um arquivo CSV

    Args:
        csv_path (str): Caminho para o arquivo CSV

    Returns:
        pd.Dataframe: Dataframe carregado
    """
    try:
        header_type = identify_header(csv_path)
        if header_type is None:
            df =  pd.read_csv(csv_path, header=header_type)
            df.columns = columns
        else:
            df = pd.read_csv(csv_path, header=header_type, usecols=columns)

        return df
    except:
        # logging.exception('[process_dataframe][load_dataframe] {}'.format(csv_path))
        if ignore_errors:
            return pd.DataFrame(columns=columns)
        
        raise

def identify_header(csv_path, n=1):
    """Identifica se o arquivo CSV possui cabeçalho ou não
    Se possuir retorna 'infer' para ser inferido pelo Pandas
    Se não possuir retorna None

    Args:
        csv_path (str): caminho para o dataframe
        n (int, optional): número de linhas para serem carregadas para a memória. Defaults to 1.

    Returns:
        str: 'infer' se possuir cabeçalho ou None caso contrário
    """
    df = pd.read_csv(csv_path, header=None, nrows=n)
    first_csv_row = df.iloc[0].tolist()

    has_header = False
    for h in CSV_HEADER:
        if h in first_csv_row:
            has_header = True
            break
    
    return 'infer' if has_header else None


def save_dataframe(df, output_path : str):
    """Salva o dataframe no caminho informado

    Args:
        df (pd.Dataframe): dataframe para ser salvo
        output_path (str): Caminho do arquivo de saída
    """
    df.to_csv(output_path, index=False)


def delete_processed_dataframes():
    """Exclui os dataframes processados
    """
    delete_dataframes_in_dir(DEFAULT_DIR_PROCESSED_DATAFRAME)

def delete_dataframes_in_dir(path: str):
    """Exclui os dataframes em um diretório

    Args:
        path (str): Diretório com os dataframes
    """
    csv_files = glob(os.path.join(path, '*.csv'))

    for csv_file in csv_files:
        os.remove(csv_file)

@pf.register_dataframe_method
def str_remove_break_line(df, column_name: str, *args, **kwargs):
    """Remove a quebra de linha do texto da coluna informada"""

    df[column_name] = df[column_name].str.replace('\n', ' ', *args, **kwargs)
    return df

@pf.register_dataframe_method
def str_clear(df, column_name: str, column_target: str = None):
    """Limpa o texto informado, removento stop words. 
    Se informado o parâmetro 'column_target' o resultado será inserido nessa coluna"""
    
    if column_target is None:
        column_target = column_name
    
    df[column_target] = df[column_name].astype(str).apply(text_processor.clear_words)
    return df

@pf.register_dataframe_method
def create_column_len(df, column_name: str, sufix: str = '_len'):
    """Cria uma coluna com o sufixo 'len' com o tamanho da coluna informada em 'column_name'"""
    
    column_target = column_name + sufix
    df[column_target] = df[column_name].astype(str).apply(len)
    return df


@pf.register_dataframe_method
def str_stemming(df, column_name: str, column_target: str = None):
    """Aplica técnica de Stemm na coluna informada em 'column_name'. 
    Se informado o parâmetro 'column_target' o resultado será inserido nessa coluna"""
    
    if column_target is None:
        column_target = column_name
    
    df[column_target] = df[column_name].astype(str).apply(text_processor.stem_sentence)
    return df


@pf.register_dataframe_method
def count_words(df, column_name: str, column_target: str = None):
    """Conta a quantidade de palavras na coluna informada em 'column_name'. 
    Se informado o parâmetro 'column_target' o resultado será inserido nessa coluna"""
    
    if column_target is None:
        column_target = column_name
    
    df[column_target] = df[column_name].apply(lambda x: len(str(x).split()))
    return df


@pf.register_dataframe_method
def ajustar_tipo_documento(df, mapeamento_tipos: list):
    """Corrige os tipos de documentos do dataframe para o mapeamento informado"""
    df['tipo'] = df['tipo'].replace(mapeamento_tipos)
    
    return df


@pf.register_dataframe_method
def remover_documentos_em_modulos_errados(df, mapeamento_documentos_errados: list):
    """Remove os documentos cadastrados em modulos errados"""
    for modulo in mapeamento_documentos_errados:
        df = df.drop(
            df[
                (df['modulo'] == modulo) \
                & (df['tipo'].isin(mapeamento_documentos_errados[modulo]))
            ].index
        )
    
    return df

@pf.register_dataframe_method
def remover_documentos_com_tipologia_errada(df, mapeamento_tipos_errados: list):
    """Remove documentos cadastrados com tipologia errada"""
    for tipo in mapeamento_tipos_errados:
        df = df.drop(
            df[
                (df['tipo'] == tipo) \
                & (df['projeto'].isin(mapeamento_tipos_errados[tipo]))
            ].index
        )
    
    return df


@pf.register_dataframe_method
def remove_column_if_exists(df, columns_names: list):
    """Remove uma coluna se ela existir"""
    columns = df.columns
    for c in columns_names:
        if c in columns:
            df = df.drop(columns=[c])
    
    return df

def clear_dataframe(df, mapeamento_alteracao_nome_tipos={}, documentos_em_modulos_errados={}, documentos_com_tipologia_errada={}):
    """Limpa os dados errados do dataframe e gera um novo dataframe com colunas processadas
    Os tipos de documentos que sofreram alteração ao longo do tempo são corrigidos
    Os documentos cadastrados em módulos errados são removidos
    Os documentos cadastrados com tipo errados são removidos
    São removidas as stopwords e é criada uma nova coluna "texto_limpo" 
    Colunas com o tamanho do texto e texto limpo são geradas
    Colunas com a quantidade de palavras para o texto e para o texto limpo são geradas
    São removidos projetos dupicados
    são removidos Nan
    Args:
        df (pd.Dataframe): Dataframe para ser limpo
        mapeamento_alteracao_nome_tipos (dict): Mapeamento do nome do tipo alterado no decorrer do tempo 
                                                para o tipo que será utilizado no dataframe
                                                A chave é o nome que deve ser alterado e o valor o novo nome
        documentos_em_modulos_errados (dict): Documentos cadastrados em módulos errados. Serão removidos do dataframe.
                                                A chave deve se o nome do módulo e o valor uma lista com os nomes dos
                                                tipos que não pertencem ao módulo
        documentos_com_tipologia_errada (dict): Documentos cadastrados com tipologia errada. Serão removidos do dataframe.
                                                A chave deve corresponder a coluna 'tipo' do dataframe e o valor deve
                                                ser uma lista que corresponda com a coluna 'projeto' que está errado
                                                Sempre que houver um documento com o 'tipo' e 'projeto' que correspondam
                                                aos valores do dicionário serão removidos do dataframe

    Returns:
        pd.Dataframe: Dataframe limpo
    """
    df = ( 
           df.ajustar_tipo_documento(mapeamento_alteracao_nome_tipos)
            .remover_documentos_em_modulos_errados(documentos_em_modulos_errados)
            .remover_documentos_com_tipologia_errada(documentos_com_tipologia_errada)
            .str_remove_break_line(column_name="texto")
            .str_clear(column_name='texto', column_target='texto_limpo')
            .create_column_len(column_name='texto')
            .create_column_len(column_name='texto_limpo')
            .count_words(column_name='texto', column_target='qtd_palavras_texto')
            .count_words(column_name='texto_limpo', column_target='qtd_palavras_texto_limpo')
            .drop_duplicates(subset=['projeto', 'tipo'])
            .dropna()
            .clean_names()
            .remove_column_if_exists(columns_names=["unnamed_0"])
            .reindex()
    )

    return df

def load_json_or_default(json_path: str) -> dict:
    """Carrega um arquivo json se existir ou um dicionário vazio caso contrário

    Args:
        json_path (str): caminho para o arquivo json

    Returns:
        dict: Json carregado ou dicionário vazio
    """
    data = {}
    if os.path.exists(json_path):
        with open(json_path, 'r') as json_file:
            data = json.load(json_file)

    return data

if __name__ == '__main__':
    format = " [%(levelname)s] %(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    parser = argparse.ArgumentParser(description='Processa os dataframes com texto extraído das imagens (raw) e transforma em um dataframe pronto para ser utilizado no treino')
    
    parser.add_argument('--input-dir', help='Diretório com os dataframes para serem processados', 
                            default=DEFAULT_DIR_RAW_DATAFRAME)
    
    parser.add_argument('--output-dir', help='Diretório para salvar o dataframe final', 
                            default=os.path.join(DEFAULT_DIR_PROCESSED_DATAFRAME, '{}_dataframe_processado.csv'.format(date.today().strftime('%Y-%m-%d'))))
    
    parser.add_argument('--keep-original', help="Se informado mantém os arquivos originais", action='store_true')
    
    args = parser.parse_args()

    output_dir = args.output_dir
    if not output_dir.endswith('.csv'):
        logging.error('O arquivo de saída deve ser um CSV')
        sys.exit()

    
    mapeamento_alteracao_nome_tipos = load_json_or_default(os.path.join(DEFAULT_DIR_DATAFRAME, 'mapeamento_alteracao_nome_tipos.json'))
    documentos_em_modulos_errados = load_json_or_default(os.path.join(DEFAULT_DIR_DATAFRAME, 'documentos_em_modulos_errados.json'))
    documentos_com_tipologia_errada = load_json_or_default(os.path.join(DEFAULT_DIR_DATAFRAME, 'documentos_com_tipologia_errada.json'))

    input_dir = args.input_dir
    df = load_dataframes_in_dir(input_dir)
    
    logging.info('Nº de linhas carregadas: {}'.format(len(df.index)))
    df = clear_dataframe(df, 
                            mapeamento_alteracao_nome_tipos=mapeamento_alteracao_nome_tipos, 
                            documentos_em_modulos_errados=documentos_em_modulos_errados, 
                            documentos_com_tipologia_errada=documentos_com_tipologia_errada)

    logging.info('Dataframe limpo - Nº de linhas após limpeza: {}'.format(len(df.index)))

    save_dataframe(df, output_dir)
    logging.info('Dataframe salvo em: {}'.format(output_dir))

    if not args.keep_original:
        logging.info('Excluindo dataframes de entrada')
        dataframes = glob(os.path.join(input_dir, '*.csv'))
        for dataframe in dataframes:
            os.remove(dataframe)

    logging.info('Operação realizada com sucesso')


    
