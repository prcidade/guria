import sys
sys.path.append('../')

import os
import process_dataframe
from model.tiny_rna import TinyRNA
from model import tiny_rna
from text.feature_extraction import feature_extractor 
from text.feature_extraction.feature_extractor import TfidfFileExtractor
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.utils import class_weight
from sklearn.metrics import classification_report, precision_score, recall_score, f1_score
import matplotlib.pyplot as plt
from tensorflow import keras
from tensorflow.keras.callbacks import EarlyStopping
import numpy as np
import pandas as pd
from datetime import datetime
import pickle
import csv
import argparse
import logging

DEFAULT_OUTPUT_DIR = os.path.join('..', '..', 'dataset', 'output', '{}'.format(datetime.now().strftime("%Y-%m-%d")))
MODEL_NAME = 'tiny_rna'


def prepare_model(model, load_weights_in_use=False):
    model.build(tiny_rna.N_FEATURES, tiny_rna.N_CLASSES)
    if load_weights_in_use:
        model.load(MODEL_NAME)
    
    base_model = model.get_base_model()
    optimizer = keras.optimizers.SGD(learning_rate=0.001, momentum=0.9)
    
    base_model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    return base_model

def load_processed_dataframe(path: str = None):
    if path is None:
        df = process_dataframe.load_all_processed_dataframes()
    else:
        df = process_dataframe.load_dataframes_in_dir(path)

    return df

def prepare_samples_to_model(df,vectorizer, encoder):

    X = df['texto_limpo'].values.astype('U')
    
    vectorizer.fit(X)
    X = vectorizer.transform(X)
    
    y = encoder.transform(df.tipo.values.reshape(len(df.tipo.values), 1))

    return X, y


def fit_new_encoder(df):
    encoder = OneHotEncoder(sparse=False)
    
    y = encoder.fit(df.tipo.values.reshape(len(df.tipo.values), 1))
    
    return encoder

def save_history(history, output):
    acc = history.history['acc']
    loss = history.history['loss']

    x = np.arange(1, len(acc) + 1)
    
    plt.figure(figsize=(12, 5))
    plt.subplot(1, 2, 1)
    # Subtrai 0.5 para corrigir o deslocamento provocado pelo calculo sobreo conjunto de treino
    plt.plot(x - 0.5, acc, 'b', label='Training acc')

    if 'val_acc' in history.history:
        val_acc = history.history['val_acc']
        plt.plot(x, val_acc, 'r', label='Validation acc')
    

    plt.title('Accuracy')
    plt.legend()
    
    plt.subplot(1, 2, 2)
    plt.plot(x, loss, 'b', label='Training loss')
    
    if 'val_loss' in history.history:
        val_loss = history.history['val_loss']
        plt.plot(x, val_loss, 'r', label='Validation loss')
    
    plt.title('Loss')
    plt.legend()

    plt.savefig(output)
    return output

if __name__ == '__main__':

    format = " [%(levelname)s] %(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    parser = argparse.ArgumentParser(description='Realiza o treinamento de um modelo com o dataframe processado. Um sumário do treino será escrito em um arquivo CSV.')
    
    parser.add_argument('--input-dir', 
                            help='Diretório com os dataframes processados, para treinamento da rede.', 
                            default=None)
    
    parser.add_argument('--output-dir', 
                            help='Diretório para salvar as saídas do treinamento. Não é utilizado para salvar o dataframe de treino.', 
                            default=DEFAULT_OUTPUT_DIR)
    
    parser.add_argument('--load-weights-in-use', 
                        help="Se informado carrega os pesos utilizados pela rede atualmente para continuar o treinamento.", 
                        action='store_true')

    parser.add_argument('--test-size', 
                            help='Porcentagem do dataset para ser utilizado como teste. Se for zero utiliza todas as amostras no treino.', 
                            type=float,
                            default=0.2)

    parser.add_argument('--epochs', 
                            help='Número de épocas de treino', 
                            type=int,
                            default=100)
    
    parser.add_argument('--ignore-early-stopping', 
                        help="Se informado não utiliza Early Stopping no treinamento", 
                        action='store_true')

    parser.add_argument('--early-stopping-patince', 
                            help='Número de épocas de paciência para o Early Stopping. Se informado com a flag --ignore-early-stopping é desconsiderado.', 
                            type=int,
                            default=5)
    
    parser.add_argument('--batch-size', 
                            help='Tamanho do batch utilizado para treino', 
                            type=int,
                            default=32)

    parser.add_argument('--keep-original-dataframes', 
                        help="Se informado mantém os arquivos originais dos dataframes na pasta original. Caso contrário serão excluídos.", 
                        action='store_true')
    
    parser.add_argument('--reencode', 
                        help="Se informado treina novamente o encoder das classes.", 
                        action='store_true')

    parser.add_argument('--save-plot', 
                        help="Se informado salva gráficos do treinamento.", 
                        action='store_true')

    parser.add_argument('--save-vocabulary', 
                        help="Se informado salva o vocabulario extraido.", 
                        action='store_true')

    parser.add_argument('--save-encoder', 
                        help="Salva o encoder utilizado no treino.", 
                        action='store_true')

    args = parser.parse_args()


    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    model = TinyRNA()

    df = load_processed_dataframe(args.input_dir)

    if args.reencode:
        encoder = fit_new_encoder(df)
    else: 
        model.load_encoder(MODEL_NAME)
        encoder = model.encoder
        
    categories = encoder.categories_[0]

    vectorizer = TfidfFileExtractor(max_features=tiny_rna.N_FEATURES)
    X, y = prepare_samples_to_model(df, vectorizer, encoder)

    model = prepare_model(model, load_weights_in_use=args.load_weights_in_use)
    
    validation_data = None
    X_train = X 
    y_train = y
    X_test = np.array([])
    y_test = np.array([])

    if args.test_size > 0:
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=args.test_size)
        validation_data = (X_test, y_test)

    logging.info("Amostras de Treino:  {}".format(X_train.shape[0]))
    logging.info("Amostras de Teste:  {:.4f}".format(X_test.shape[0]))

    y_integers = np.argmax(y_train, axis=1)

    class_weights = class_weight.compute_class_weight('balanced', np.unique(y_integers) , y_integers)
    d_class_weights = dict(enumerate(class_weights))
    # ajusta os pesos para as categorias inexistêntes no conjunto de treino
    for i in range(len(categories)):
        if i not in d_class_weights:
            d_class_weights[i] = 0.0

    callbacks = []
    if not args.ignore_early_stopping:
        monitor = 'val_loss'
        # se os dados forem utilizados todos para treino monitora o loss para early stoping
        if args.test_size == 0:
            monitor = 'loss'

        es = EarlyStopping(monitor=monitor, mode='min', verbose=1, patience=args.early_stopping_patince)
        callbacks.append(es)
    
    history = model.fit(
        X_train, 
        y_train, 
        epochs=args.epochs, 
        validation_data=validation_data, 
        batch_size=args.batch_size, 
        class_weight=d_class_weights,
        callbacks=callbacks
    )

    train_loss, train_accuracy = model.evaluate(X_train, y_train, verbose=False)

    y_pred = model.predict(X_train)
    y_true = y_train.argmax(axis=1)
    train_report = classification_report(y_true, y_pred.argmax(axis=1), output_dict=True)

    logging.info("Acc Treino: {:.4f}".format(train_accuracy))
    logging.info("Loss Treino: {:.4f}".format(train_loss))

    test_loss = 0
    test_accuracy = 0
    test_report = {}
    if args.test_size > 0:
        test_loss, test_accuracy = model.evaluate(X_test, y_test, verbose=False)
        logging.info("Acc Teste:  {:.4f}".format(test_accuracy))
        logging.info("Loss Teste:  {:.4f}".format(test_loss))

        y_pred = model.predict(X_test)
        y_true = y_test.argmax(axis=1)
        test_report = classification_report(y_true, y_pred.argmax(axis=1), output_dict=True)

    last_epoch = len(history.history['loss'])
    logging.info('Última época do treino: {}'.format(last_epoch))

    # Salva os pesos resultantes do treino
    weights_path = os.path.join(args.output_dir, '{}_test-size:{}_{}.h5'.format(datetime.now().strftime("%Y-%m-%d"), args.test_size, MODEL_NAME))
    model.save_weights(weights_path)
    logging.info('Pesos salvos em: {}'.format(weights_path))

    # Salva o dataframe
    final_dataframe_name = '{}_dataframe_treino.csv'.format(datetime.now().strftime("%Y-%m-%d"))
    final_dataframe_path = os.path.join(process_dataframe.DEFAULT_DIR_TRAIN_DATAFRAME, final_dataframe_name)
    process_dataframe.save_dataframe(df, final_dataframe_path)
    logging.info('Dataframe salvo em: {}'.format(final_dataframe_path))
    
    # Salva o vocabulario vetorizado
    if args.save_vocabulary:        
        vectorizer.save(os.path.join(args.output_dir, '{}_tfidf_vocabulary.pkl'.format(datetime.now().strftime("%Y-%m-%d"))))

    # Salva os gráficos de treino
    if args.save_plot:
        plot_path = os.path.join(args.output_dir, '{}_history.png'.format(datetime.now().strftime("%Y-%m-%d")))
        save_history(history, plot_path)
        logging.info('Imagens salvas em: {}'.format(plot_path))

    # Salva as classes do encoder
    if args.save_encoder:
        encoder_path = os.path.join(args.output_dir, '{}_encoder.pkl'.format(datetime.now().strftime("%Y-%m-%d")))
        pickle.dump(encoder.categories_, open(encoder_path, "wb"))

        logging.info('Encoder salvo em: {}'.format(encoder_path))

    if not args.keep_original_dataframes:
        process_dataframe.delete_processed_dataframes()

    filename = os.path.join(args.output_dir, 'relatorio_treino.csv')
    file_exists = os.path.isfile(filename)

    with open(filename, 'a') as csvfile:
        headers = ['Data', 'Total Amostras', 'Amostras treino', 'Amostras Teste', 'Numero Classes', 
                    'Acc Treino', 'Loss Treino', 'Acc Teste', 'Loss Teste', 
                    'Epocas Treinadas', 'Epocas Programadas', 'Early Stopping', 'Batch Size',
                    'Dataframe', 'Pesos', 'Model Name', 'Transfer Learning']
        writer = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n',fieldnames=headers)

        if not file_exists:
            writer.writeheader() 

        writer.writerow({
            'Data': datetime.now(),
            'Total Amostras': len(df.index),
            'Amostras treino': X_train.shape[0],
            'Amostras Teste': X_test.shape[0],
            'Numero Classes': tiny_rna.N_CLASSES,
            'Acc Treino': train_accuracy,
            'Loss Treino': train_loss,
            'Acc Teste': test_accuracy,
            'Loss Teste': test_loss,
            'Epocas Treinadas': last_epoch,
            'Epocas Programadas': args.epochs,
            'Early Stopping': 0 if args.ignore_early_stopping else 1,
            'Batch Size': args.batch_size,
            'Dataframe': final_dataframe_path,
            'Pesos': weights_path,
            'Model Name': MODEL_NAME,
            'Transfer Learning': 1 if args.load_weights_in_use else 0,
        })


    df = pd.DataFrame(train_report).transpose()
    df.to_csv(os.path.join(args.output_dir, '{}_train_report.csv'.format( datetime.now().strftime("%Y-%m-%d") )))

    if len(test_report) > 0:
        df = pd.DataFrame(test_report).transpose()
        df.to_csv(os.path.join(args.output_dir, '{}_test_report.csv'.format( datetime.now().strftime("%Y-%m-%d") )))

    logging.info('Treino concluído')
