'''
Prepara um dataset para análise de texto
Converte os arquivos em PDF para imagem e posteriormente faz um OCR para extrair o texto e então salva em CSV
O diretório de entrada deve ser condizente com o exemplo abaixo:
/dataset
    / originais
        /operacao_credito
            / Projeto 1
                / Tipo documento X
                    | arquivo 1.pdf
            / Projeto 2
                / Tipo documento X
                    | arquivo N.pdf
        /processo_licitatorio
            / Projeto 1
                    /tecnico
                        / Tipo documento X
                            | arquivo 1.pdf
                            | arquivo 2.pdf
                    /juridico
                        / Tipo documento 1
                            | arquivo 1.pdf
                            | arquivo 2.pdf
                            | arquivo n.pdf
                        / Tipo documento 2
                            | arquivo 1.pdf
                            | arquivo n.pdf
                / Projeto 2
    
Os arquivos em PDF convertidos em imagens serão salvos em outra pasta com a mesma estrutura do diretório de entrada
Para extrair o texto a imagem terá sua orientação corrigida e o texto de todos os arquivos de uma pasta serão utilizados
para compor o texto de um documento, ou seja, as páginas são concatenadas com quebra de linha

O CSV de saída contera o projeto (nome da pasta), o texto extraído dos documentos e o tipo de documento (nome da pasta)

'''

import sys
sys.path.append('../')

from conversor import files_to_image
from image.ocr import image_ocr
from image.process import image_processor
from text.process import text_processor
from text.feature_extraction.text_extractor import TextExtractor, DEFAULT_MAX_PAGES
import process_dataframe
from datetime import date
from tqdm import tqdm
import pandas as pd
import os
from glob import glob
import sys
import mimetypes
import shutil

import argparse
import logging

DEFAULT_OUTPUT_DIR_IMAGES = '../../dataset/imagens/'
DEFAULT_OUTPUT_DIR_IMAGES_OPERACAO_CREDITO = os.path.join('../../dataset/imagens/', 'operacao_credito')
DEFAULT_OUTPUT_DIR_IMAGES_PROCESSO_LICITATORIO_JURIDICO = os.path.join('..', '..', 'dataset', 'imagens', 'processo_licitatorio_juridico')
DEFAULT_OUTPUT_DIR_IMAGES_PROCESSO_LICITATORIO_TECNICO = os.path.join('..', '..', 'dataset', 'imagens', 'processo_licitatorio_tecnico')

DEFAULT_OUTPUT_DIR_DATAFRAME = '../../dataset/dataframe/'


def convert_processo_licitatorio(dataset_dir, output_img_dir):
    input_dir = os.path.join(dataset_dir, 'processo_licitatorio')
    # output_dir = os.path.join(output_img_dir, 'processo_licitatorio_juridico') # pasta com as imagens dos arquivos jurídicos
    # output_dir_tecnico = os.path.join(output_img_dir, 'processo_licitatorio_tecnico') # pastas com as imagens dos arquivos técnicos

    output_dir = DEFAULT_OUTPUT_DIR_IMAGES_PROCESSO_LICITATORIO_JURIDICO # pasta com as imagens dos arquivos jurídicos
    output_dir_tecnico = DEFAULT_OUTPUT_DIR_IMAGES_PROCESSO_LICITATORIO_TECNICO # pastas com as imagens dos arquivos técnicos

    processos_licitatorios = os.listdir(input_dir)
    for processo in tqdm(processos_licitatorios):
        output_folder = os.path.join(output_dir, processo)
        
        folder_juridico = os.path.join(input_dir, processo, 'juridico')
        convert_files_to_image(folder_juridico, output_folder)

        output_folder_tecnico = os.path.join(output_dir_tecnico, processo)
        folder_tecnico = os.path.join(input_dir, processo, 'tecnico')
        # imagens técnicas são digitalizadas em baixa qualidade, usar um DPI mais alto facilita a extração de texto
        convert_files_to_image(folder_tecnico, output_folder_tecnico, dpi=500)

def convert_operacao_credito(dataset_dir, output_dir):
    """Converte os arquivos enviados para operação de crédito para imagens

    Args:
        dataset_dir (str): caminho para o diretório base em que estão salvos as operações de crédito
        output_dir (str): caminho base para o diretório base em que serão salvas as imagens dos documentos
    """
    input_dir = os.path.join(dataset_dir, 'operacao_credito')
    output_dir = os.path.join(output_dir, 'operacao_credito')
    
    operacoes = os.listdir(input_dir)
    for operacao in tqdm(operacoes):
        output_folder = os.path.join(output_dir, operacao)
        
        operacao_folder = os.path.join(input_dir, operacao)
        convert_files_to_image(operacao_folder, output_folder)


def convert_projeto(dataset_dir, output_dir):
    """Converte os documentos enviados nos projetos para imagens

    Args:
        dataset_dir (str): caminho para o diretório base em que estão salvos os projetos
        output_dir (str): caminho para o diretório base em que serão salvas as imagens dos projetos
    """
    input_dir = os.path.join(dataset_dir, 'projeto')
    output_dir = os.path.join(output_dir, 'projeto')
    
    projetos = os.listdir(input_dir)
    for projeto in tqdm(projetos):

        projeto_folder = os.path.join(input_dir, projeto)
        output_folder = os.path.join(output_dir, projeto)
        
        convert_files_to_image(projeto_folder, output_folder)



def convert_files_to_image(input_dir, output_dir, dpi=300):
    """Converte os arquivos em imagens. Se o diretório de saída já existir a conversão não prosseguirá.

    Args:
        input_dir (str): arquivo em que estão os arquivos de um projeto/processo para serem convetidos
        output_dir ([type]): diretório base para salvar os arquivos de saída
        dpi (int, optional): DPI para converter as imagens. Defaults to 300.
    """

    # não há arquivo de entrada
    if not os.path.exists(input_dir):
        return
        
    # ignora diretórios já convertidos
    if os.path.exists(output_dir):
        return 

    os.makedirs(output_dir)
    
    folders = os.listdir(input_dir)
    for folder in folders:
        full_path = os.path.join(input_dir, folder)
        output_img_folder = os.path.join(output_dir, folder)

        entries = os.listdir(input_dir)
        # Se o arquivo enviado for uma imagem apenas copia para o diretório de saída
        for entry in entries:
            file_path = os.path.join(full_path, entry)
            mime = mimetypes.guess_type(file_path)[0]
            
            if mime is None:
                continue
                
            mime_type, mime_end = mime.split('/')
            if mime_type == 'image':
                shutil.copy2(file_path, output_dir)
                continue

        try:        
            # Converte os arquivos em PDF
            files_to_image.convert_dir(full_path, output_img_folder, dpi=dpi)
        except:
            logging.error('Erro ao converter arquivo para imagem: {}'.format(full_path))
            
def convert_images_to_dataframe(img_dir, modulo):
    """Converte o texto dos arquivos de um projeto/processo para um dataframe.
    Cada documento será covertido em uma linha.

    Args:
        img_dir (str): caminho para projeto a ter o texto extraído.
        modulo (str): nome do modulo ao qual o projeto/processo pertence (ex: "projeto", "operacao_credito", etc)

    Returns:
        [DataFrame]: dataframe com os documentos convertidos
    """
    
    logging.info('Convertendo imagens em texto')
    logging.info('Diretório de entrada: {}'.format(img_dir))

    data = []
    folders = os.listdir(img_dir)
    for folder_projeto in tqdm(folders):
        
        base_folder = os.path.join(img_dir, folder_projeto)
        
        if modulo == 'processo_licitatorio_tecnico':
            lotes_folder = os.listdir(base_folder)
            for lote_folder in lotes_folder:
                base_path = os.path.join(base_folder, lote_folder)
                projeto = '{} - {}'.format(folder_projeto, lote_folder)
                data += extract_text_from_project(base_path, projeto, modulo, deskew=True)
        else:
            data += extract_text_from_project(base_folder, folder_projeto, modulo, deskew=True)
    
    df = pd.DataFrame(data)
    return df

def extract_text_from_project(base_path, projeto, modulo, deskew=False):

    folders_tipos = os.listdir(base_path)
    
    data = []
    for folder_tipo in folders_tipos:

        # Converte os documentos de um mesmo tipo em um texto único
        images_folder = os.path.join(base_path, folder_tipo)
        text = convert_images_to_text(images_folder, deskew=deskew)
        
        data.append({
            'projeto' : projeto,
            'texto'   : text,
            'tipo'    : folder_tipo,
            'modulo'  : modulo
        })

    return data

def convert_images_to_text(images_path, deskew=False):
    try:
        images = sorted(os.listdir(images_path))
        
        num_pages = min(len(images), DEFAULT_MAX_PAGES)
        text = ''
        for image_path in images[:num_pages]:
            image_path = os.path.join(images_path, image_path)
            
            # ignora pastas
            if os.path.isdir(image_path):
                continue 

            # pula uma linha ao trocar de imagem
            extracted_text = extract_text_from_image(image_path, deskew) + '\n' 

            if extracted_text is not None:
                text += extracted_text 

        return text
    except:
        return ''

def extract_text_from_image(image_path, deskew=False):
    try:
        text_extractor = TextExtractor()
        return text_extractor.extract_text_from_file(image_path)

    except Exception as e:
        logging.error('{} - Image: {}'.format(e, image_path))
        return ''

if __name__ == '__main__':
    format = " [%(levelname)s] %(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    parser = argparse.ArgumentParser(description='Converte os documentos em dataframes com o texto extraído')

    parser.add_argument('--input-dir', 
                            help='Diretório com os documentos originais para ter o texto extraído', 
                            default='../../dataset/originais/'
                    )
    
    parser.add_argument('--images-dir', 
                            help='Diretório para salvar as imagens da conversão', 
                            default='../../dataset/imagens/'
                    )
    
    parser.add_argument('--dataframe-dir', 
                            help='Diretório para salvar os dataframes com os textos extraídos', 
                            default='../../dataset/raw/'
                    )

    parser.add_argument('--operacao-credito-dataframe-name', 
                        help="Nome do dataframe de saída com texto da operação de crédito", 
                        default='{}_dataframe_operacao_credito_raw.csv'.format(date.today().strftime('%Y-%m-%d'))
                    )
                        
    parser.add_argument('--processo-licitatorio-juridico-dataframe-name', 
                        help="Nome do dataframe de saída com texto dos documentos jurídicos do processo licitatório", 
                        default='{}_dataframe_processo_licitatorio_juridico_raw.csv'.format(date.today().strftime('%Y-%m-%d'))
                    )

    parser.add_argument('--processo-licitatorio-tecnico-dataframe-name', 
                        help="Nome do dataframe de saída com texto dos documentos técnicos do processo licitatório", 
                        default='{}_dataframe_processo_licitatorio_tecnico_raw.csv'.format(date.today().strftime('%Y-%m-%d'))                
                    )

    parser.add_argument('--projeto-dataframe-name', 
                        help="Nome do dataframe de saída com texto do projeto", 
                        default='{}_dataframe_projeto_raw.csv'.format(date.today().strftime('%Y-%m-%d'))
                    )                

    parser.add_argument('--images-only', 
                        help="Se informado apenas converte as imagens, não extraindo o texto", 
                        action='store_true'
                    )

    parser.add_argument('--text-extraction-only', 
                        help="Se informado apenas extrai os textos das imagens já convertidas", 
                        action='store_true'
                    )

    parser.add_argument('--ignore-operacao-credito', 
                        help="Se informado desconsidera o processamento de Operação de Crédito", 
                        action='store_true'
                    )
    
    parser.add_argument('--ignore-processo-licitatorio', 
                        help="Se informado desconsidera o processamento do Processo Licitatorio", 
                        action='store_true'
                    )

    parser.add_argument('--ignore-projeto', 
                        help="Se informado desconsidera o processamento de Projetos", 
                        action='store_true'
                    )

    args = parser.parse_args()
    
    if not args.text_extraction_only:
        if not args.ignore_operacao_credito:
            logging.info('Convertendo PDF para Imagens (Operçaões de Crédito)...')
            convert_operacao_credito(args.input_dir, args.images_dir)
            logging.info('Operações de Crédito convertidas')
        
        if not args.ignore_processo_licitatorio:
            logging.info('Convertendo PDF para Imagens (Processo Licitatório)...')
            convert_processo_licitatorio(args.input_dir, args.images_dir)
            logging.info('Processos Licitatórios convertidos')

        if not args.ignore_projeto:
            logging.info('Convertendo para Imagens (Projetos)...')
            convert_projeto(args.input_dir, args.images_dir)
            logging.info('Projetos convertidos')

    if args.images_only:
        logging.info('Fim!')
        sys.exit()

    if not os.path.exists(args.dataframe_dir):
        os.makedirs(args.dataframe_dir)

    if not args.ignore_operacao_credito:
        df = convert_images_to_dataframe(os.path.join(args.images_dir, 'operacao_credito'), 'operacao_credito')

        output_file = os.path.join(args.dataframe_dir, args.operacao_credito_dataframe_name)
        process_dataframe.save_dataframe(df, output_file)
        logging.info('Salvando arquivo: {}'.format(output_file))
    
    if not args.ignore_processo_licitatorio:
        df = convert_images_to_dataframe(os.path.join(args.images_dir, 'processo_licitatorio_juridico'), 'processo_licitatorio_juridico')
    
        output_file = os.path.join(args.dataframe_dir, args.processo_licitatorio_juridico_dataframe_name)
        process_dataframe.save_dataframe(df, output_file)
        logging.info('Salvando arquivo: {}'.format(output_file))

        df = convert_images_to_dataframe(os.path.join(args.images_dir, 'processo_licitatorio_tecnico'), 'processo_licitatorio_tecnico')

        output_file = os.path.join(args.dataframe_dir, args.processo_licitatorio_tecnico_dataframe_name)
        process_dataframe.save_dataframe(df, output_file)
        logging.info('Salvando arquivo: {}'.format(output_file))

    if not args.ignore_projeto:
        df = convert_images_to_dataframe(os.path.join(args.images_dir, 'projeto'), 'projeto')

        output_file = os.path.join(args.dataframe_dir, args.projeto_dataframe_name)
        process_dataframe.save_dataframe(df, output_file)
        logging.info('Salvando arquivo: {}'.format(output_file))

    logging.info('Fim!')
