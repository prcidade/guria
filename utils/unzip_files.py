''' Unzip 
Descompacta os arquivos de um diretório para outro
Todos os arquivos do Dirterório serão descompactados
'''

from zipfile import ZipFile
import os
from glob import glob
import argparse
from tqdm import tqdm
import logging

DEFAULT_ZIPPED_DIR = '../../dataset/compactados'
DEFAULT_UNZIP_DIR = '../../dataset/originais/'
DEFAULT_UNZIP_DIR_OPERACAO_CREDITO = os.path.join(DEFAULT_UNZIP_DIR, 'operacao_credito')
DEFAULT_UNZIP_DIR_PROCESSO_LICITATORIO = os.path.join(DEFAULT_UNZIP_DIR, 'processo_licitatorio')
DEFAULT_UNZIP_DIR_PROJETOS = os.path.join(DEFAULT_UNZIP_DIR, 'projetos')

def unzip_recursive(input_dir, output_dir):
    """Descompacta os arquivos de um diretório de forma recursiva, de até um nível.
    Entra nas pastas do diretório 'input_dir' e descompacta os arquivos no diretório 'output_dir'.

    Args:
        input_dir (str): Diretório de entrada com os arquivos compactados
        output_dir (str): Diretório para descompactar os arquivos
    """
    list_subfolders_with_paths = [f.path for f in os.scandir(input_dir) if f.is_dir()]
    for folder in list_subfolders_with_paths:
        subfolder_name = os.path.basename(os.path.normpath(folder))
        
        unzip_to_folder = os.path.join(output_dir, subfolder_name)
        unzip_path(folder, unzip_to_folder)
        
def unzip_path(path, output_dir):
    """Descompacta todos os arquivos ZIP do diretório de entrada para o diretório indicado.

    Args:
        path (str): Caminho com arquivos compactados.
        output_dir (str): Caminho para descompactar os arquivos.
    """
    zip_files = glob(os.path.join(path, '*.zip'))
    print('Path: {}'.format(path))
    print('Num Ziped Files: {}'.format(len(zip_files)))
    
    for zip_file in tqdm(zip_files):
        
        folder = os.path.basename(os.path.normpath(zip_file)).split('.')[0]
        
        output_unziped = os.path.join(output_dir, folder)
        if not os.path.exists(output_unziped):
            os.makedirs(output_unziped)
            
        unzip(zip_file, output_unziped)

def unzip(zip_path, output_dir, create_new_folder=False):
    """Descompacta o arquivo ZIP especificado na pasta indicada

    Args:
        zip_path (str): Caminho para o arquivo compactado
        output_dir (str): Caminho onde os arquivos serão extraídos
        create_new_folder (bool, optional): Se verdadeiro, cria uma nova pasta, com o nome do arquivo compactado, para extraír os arquivos. Defaults to False.

    Returns:
        str: Caminho com os arquivos descompactados
    """
    if create_new_folder:
        folder = os.path.basename(os.path.normpath(zip_path)).split('.')[0]
        output_dir = os.path.join(output_dir, folder)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

    try:
        with ZipFile(zip_path, 'r') as zip_ref:
            zip_ref.extractall(output_dir)
    except:
        logging.info('Erro ao descompactar o arquivo: {}'.format(zip_path))
        raise

    return output_dir
    
if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")
    
    parser = argparse.ArgumentParser(description='Descompacta os arquivos zip de um diretório')
    parser.add_argument('--input-dir', default=DEFAULT_ZIPPED_DIR, help='Diretório dos arquivos zip')
    parser.add_argument('--output-dir', default=DEFAULT_UNZIP_DIR, help='Diretório para os arquivos descompactados')
    parser.add_argument('-r', '--recursive', help="Entra nos diretórios para descompactar os arquivos (apenas 1 nivel)", action='store_true')
    
    args = parser.parse_args()
    
    if args.recursive:
        unzip_recursive(args.input_dir, args.output_dir)
    else:
        unzip_path(args.input_dir, args.output_dir)
    

