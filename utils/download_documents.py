'''
Realiza o download dos documentos para serem utilizados no treinamento da IA
'''

import requests
import pandas as pd
import shutil
import logging
import os
import sys
import argparse

import process_dataframe

sys.path.append('../')


BASE_API_URL = 'http://portaldosmunicipios.pr.gov.br/api/v1'
OPERACAO_CREDITO_API_URL = '{}/operacao-credito'.format(BASE_API_URL)
PROCESSO_LICITATORIO_API_URL = '{}/processo-licitatorio'.format(BASE_API_URL)

STATUS_OPERACAO_CREDITO_FINALIZADA = '250'
STATUS_PROCESSO_LICITATORIO_FINALIZADO = '150'

OUTPUT_PATH_DOWNLOADED_FILES = os.path.join('..', '..', 'dataset', 'compactados')

DATAFRAME_NAME_OPERACAO_CREDITO = 'dataframe_operacao_credito.csv'
DATAFRAME_NAME_PROCESSO_LICITATORIO_JURIDICO = 'dataframe_processo_licitatorio_juridico.csv'


def clear_output_dir(output_dir):
    """Apaga as informações do diretório em que estão salvos os arquivos baixados

    Args:
        output_dir (str): Diretório com os arquivos baixados.
    """

    logging.info('Limpando o diretório de saída...')
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(os.path.join(output_dir, 'operacao_credito'))
    os.makedirs(os.path.join(output_dir, 'processo_licitatorio'))
    logging.info('Diretório de saída limpo')


def baixar_documentos_novas_operacoes_credito(train_dataframe_dir, output_dir):
    """Baixa os documentos compactados apresentados em Operações de Crédito que ainda não foram utilizados no treinamento.
    Os arquivos serão salvos em disco no diretório especificado em 'output_dir'.
    Processos que estejam no dataframe de treino serão desconsiderados.

    Args:
        train_dataframe_dir (str): Diretório que contém o dataframe com as amostras já utilizadas para treino.
        output_dir (str): Caminho para salvar os arquivos baixados
    """

    logging.info('Requisitando Operações de Crédito disponíveis...')
    operacoes_credito_disponiveis = get_operacoes_credito_disponiveis()
    logging.info('Requisição finalizada: {} operações de crédito carregadas'.format(len(operacoes_credito_disponiveis)))

    logging.info('Carregando Operações de Crédito já utilizadas para treinamento...')
    operacoes_credito_utilizadas_treinamento = get_operacoes_credito_utilizadas_treinamento(train_dataframe_dir)
    logging.info('Carregamento finalizado: {} operações de crédito carregadas'.format(len(operacoes_credito_utilizadas_treinamento)))
    
    logging.info('Filtrando operações de crédito já utilizadas...')
    operacoes_credito_novas = filtrar_operacoes_credito_novas(operacoes_credito_disponiveis, operacoes_credito_utilizadas_treinamento)    
    logging.info('Operações filtradas: {} novas operações'.format(len(operacoes_credito_novas)))

    baixar_documentos_novas_entradas_para(operacoes_credito_novas, os.path.join(output_dir, 'operacao_credito'))

    logging.info('Operações de crédito baixadas')

def baixar_documentos_novos_processos_licitatorios(train_dataframe_dir, output_dir):
    """Baixa os documentos compactados apresentados em Processos Licitatórios que ainda não foram utilizados no treinamento.
    Os arquivos serão salvos em disco no diretório especificado em 'output_dir'.
    Processos que estejam no dataframe de treino serão desconsiderados.

    Args:
        train_dataframe_dir (str): Diretório que contém o dataframe com as amostras já utilizadas para treino.
        output_dir (str): Caminho para salvar os arquivos baixados
    """

    logging.info('Requisitando Processos Licitatórios disponíveis...')
    processos_licitatorios_disponiveis = get_processos_licitatorios_disponiveis()
    logging.info('Requisição finalizada')

    logging.info('Carregando Processos Licitatórios já utilizados para treinamento...')
    processos_liticatorios_utilizados_treinamento = get_processos_licitatorio_utilizados_treinamento(train_dataframe_dir)
    logging.info('Carregamento finalizado: {} processos carregados'.format(len(processos_liticatorios_utilizados_treinamento)))

    logging.info('Filtrando Processos Licitatórios já utilizados...')
    processos_licitatorios_novos = filtrar_processos_licitatorios_novos(processos_licitatorios_disponiveis, processos_liticatorios_utilizados_treinamento)    
    logging.info('Processos filtradas: {} novos processos'.format(len(processos_licitatorios_novos)))

    baixar_documentos_novas_entradas_para(processos_licitatorios_novos, os.path.join(output_dir, 'processo_licitatorio'))

    logging.info('Prcessos licitatórios baixados')


def get_operacoes_credito_disponiveis():
    """Recupera as Operações de Crédito finalizadas disponíveis na API do Portal dos Municípios.

    Returns:
        list: Lista de Operações de Crédito disponíveis
    """

    response = http_get(OPERACAO_CREDITO_API_URL, {'status': STATUS_OPERACAO_CREDITO_FINALIZADA, 'include-all': 'true'})
    return response['data']

def get_processos_licitatorios_disponiveis():
    """Recupera os processos licitatórios finalizados disponíveis na API do Portal dos Municípios.

    Returns:
        list: Lista de processos licitatórios disponíveis
    """

    response = http_get(PROCESSO_LICITATORIO_API_URL, {'status': STATUS_PROCESSO_LICITATORIO_FINALIZADO, 'include-all': 'true'})
    return response['data']

def http_get(url, query_params={}):
    """Realiza um request, do tipo GET, para a URL especificada

    Args:
        url (str): url para fazer o GET
        query_params (dict, optional): Parâmetros da query para fazer o request. Defaults to {}.

    Returns:
        JSON: Json de resposta
    """
    
    r = requests.get(url, params=query_params)

    return r.json()


def get_operacoes_credito_utilizadas_treinamento(tran_dataframe_dir):
    """Carrega o dataframe com as amostras de Operação de Crédito já utilizadas para treino.

    Args:
        tran_dataframe_dir (str): Diretório que contém o dataframe utilizado para treino dos modelos

    Returns:
        list: Lista das operações de crédito únicas utilizadas para treino
    """
    
    tran_dataframe_dir = os.path.join(tran_dataframe_dir, DATAFRAME_NAME_OPERACAO_CREDITO)
    return get_projetos_unicos_do_csv(tran_dataframe_dir)

def get_processos_licitatorio_utilizados_treinamento(tran_dataframe_dir):
    """Carrega o dataframe com as amostras de Processo Licitatório já utilizadas para treino.

    Args:
        tran_dataframe_dir (str): Diretório que contém o dataframe utilizado para treino dos modelos

    Returns:
        list: Lista de processos licitatórios únicas utilizadas para treino
    """
    
    tran_dataframe_dir = os.path.join(tran_dataframe_dir, DATAFRAME_NAME_PROCESSO_LICITATORIO_JURIDICO)
    return get_projetos_unicos_do_csv(tran_dataframe_dir)

def get_projetos_unicos_do_csv(csv_path):
    """Recupera os projetos únicos de um dataframe.
        O termo "projeto" é utilizado para identificar a fonte dos dados dentro do datraframe
        Mas pode ser referir a uma Operação de Crédito, Processo Licitatório, ou qualquer outro módulo do ciclo de vida do projeto
    Returns:
        list: Projetos únicos do dataframe
    """

    if not os.path.exists(csv_path):
        return []

    df = pd.read_csv(csv_path)

    return df['projeto'].unique().tolist()


def filtrar_operacoes_credito_novas(operacoes_credito_disponiveis, operacoes_credito_utilizadas):
    """Filtra as operações de crédito novas (não utilizados no treinamento do modelo).

    Args:
        operacoes_credito_disponiveis (list): Operações de crédito disponíveis através da API do Portal dos Municípios
        operacoes_credito_utilizadas (list): Operações de crédito já utilizados para treinamento do modelo de IA.

    Returns:
        list: Operações de crédito não utilizados.
    """

    operacoes_credito_novas = []

    for operacao_credito_disponivel in operacoes_credito_disponiveis:
        identificador_operacao_credito = get_identificador_operacao_credito(operacao_credito_disponivel)
        
        if identificador_operacao_credito not in operacoes_credito_utilizadas:          
            data = {
                'identificador': identificador_operacao_credito,
                'url': operacao_credito_disponivel['url_download_documentos']
            }
            
            operacoes_credito_novas.append(data)

    return operacoes_credito_novas

def get_identificador_operacao_credito(operacao_credito):
    """Recupera o identificador de uma operação de crédito específica.
    O identificador é definido pelo Portal dos Municípios.
    O identificador tem o formato: <Nome do Município> - Operação de Crédito - <Número da Operação de Crédito do SAM>

    Args:
        operacao_credito (dict): Informações da operação de crédito disponíveis na API do Portal dos Municípios.

    Returns:
        str: Texto identificador da Operação de Créidto
    """

    return "{} - {} {}".format(operacao_credito['nome_municipio'], 'Operação de Crédito', operacao_credito['numero_sam'])


def filtrar_processos_licitatorios_novos(processos_licitatorios_disponiveis, processos_liticatorios_utilizados):
    """Filtra os processos licitatórios novos (não utilizados no treinamento do modelo).

    Args:
        processos_licitatorios_disponiveis (list): Processos licitatórios disponíveis através da API do Portal dos Municípios
        processos_liticatorios_utilizados (list): Processos licitatórios já utilizados para treinamento do modelo de IA.

    Returns:
        list: Processos licitatórios não utilizados.
    """

    processos_licitatorios_novos = []

    for processo_licitatorio_disponivel in processos_licitatorios_disponiveis:
        identificador_processo_licitatorio = get_identificador_processo_licitatorio(processo_licitatorio_disponivel)

        if identificador_processo_licitatorio not in processos_liticatorios_utilizados:
            data = {
                'identificador': identificador_processo_licitatorio,
                'url': processo_licitatorio_disponivel['url_download_documentos']
            }

            processos_licitatorios_novos.append(data)
        
    return processos_licitatorios_novos


def get_identificador_processo_licitatorio(processo_licitatorio):
    """Recupera o identificador de um processo licitatório específico.
    O identificador é definido pelo Portal dos Municípios.
    O identificador tem o formato: <Nome do Município> - proj <Nùmero do Projeto do SAM> - processo <Número do Processo Licitatório do SAM>

    Args:
        processo_licitatorio (dict): Ĩnformações do processo licitatório retornadas na API do Portal dos Municípios

    Returns:
        str: Texto identificador do processo licitatório
    """

    return "{} - proj {} - processo {}".format(
        processo_licitatorio['nome_municipio'], 
        processo_licitatorio['numero_projeto'], 
        processo_licitatorio['numero_sam']
    )

def baixar_documentos_novas_entradas_para(novas_entradas, output_folder):
    """Baixa os documentos do Portal dos Municípios para as entradas especificadas em 'novas_entradas'.
    Os arquivos baixados serão salvos no diretório especificado em 'output_folder'.
    As novas entradas representam novos inputs de treino, não utilizadas antereiormente.

    Args:
        novas_entradas (list): Lista com as novas entradas de treino para serem baixadas.
        output_folder (str): Diretório para salvar os arquivos baixados.
    """

    if not os.path.exists(output_folder):
        os.makedirs(output_folder, exist_ok=True)

    for entrada in novas_entradas:
        baixar_documentos_nova_entrada_para(entrada, output_folder)
        

def baixar_documentos_nova_entrada_para(nova_entrada, output_folder):
    """Baixa um documento através da API do Portal dos Municípios.
    O documento está compactado em formato ZIP e será salvo no diretóri o 'output_folder'.
    Os documentos são referentes a 'nova_entrada', que representa um novo input para ser utilizado no treinamento.

    Args:
        nova_entrada (dict): Informações da nova entrada de treino, não utilizada anteriormente.
        output_folder (str): Caminho para salvar o arquivo baixado.

    Returns:
        str: Caminho para o arquivo baixado.
    """

    url = nova_entrada['url']
    output = get_output_path_para_nova_entrada(nova_entrada, output_folder)

    # arquivo já existente
    if os.path.exists(output):
        return output

    return baixar_documento_para(url, output)

def get_output_path_para_nova_entrada(nova_entrada, output_folder):
    """Recupera o caminho para salvar a nova entrada de treino.

    Args:
        nova_entrada (dict): Informações da nova entrada de treino
        output_folder (str): Caminho para salvar o arquivo compactado, com os documentos, da nova entrada de treino

    Returns:
        str: Caminho para o arquivo a ser salvo em disco.
    """

    output_file_name = get_output_file_name(nova_entrada)
    output = os.path.join(output_folder, output_file_name)

    return output

def get_output_file_name(nova_entrada):
    """Recupera o nome do arquivo ZIP para ser baixado para a nova entrada (não utilizada no treino).

    Args:
        nova_entrada (dict): Informações da nova entrada de treino. Deve conter o indentificado da entrada.

    Returns:
        str: Nome do arquivo a ser salvo
    """

    identificador = nova_entrada['identificador']
    output_file_name = '{}.zip'.format(identificador)

    return output_file_name

def baixar_documento_para(url, output):
    """Baixa o arquivo especificado na URL para o arquivo de saída indicado.

    Args:
        url (str): URL para o arquivo
        output (str): Caminho completo para o arquivo a ser salvo em disco

    Returns:
        str: Caminho do arquivo salvo em disco
    """

    r = requests.get(url, allow_redirects=True, stream=True)
    with open(output, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=256):
            fd.write(chunk)

    return output

if __name__ == '__main__':
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%Y-%m-%d %H:%M:%S")

    parser = argparse.ArgumentParser(description='Baixa os arquivos para treinamento através da API o Portal dos Municípios.')
    parser.add_argument('--train-dataframe-dir', default='../../dataset/dataframe/', help='Diretório os arquivos dos dataframes utilizados para treino')
    parser.add_argument('--output-dir', default=OUTPUT_PATH_DOWNLOADED_FILES, help='Diretório para salvar os arquivos baixados')
    
    args = parser.parse_args()

    baixar_documentos_novas_operacoes_credito(args.train_dataframe_dir, args.output_dir)

    baixar_documentos_novos_processos_licitatorios(args.train_dataframe_dir, args.output_dir)    
