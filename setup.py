from setuptools import find_packages, setup

setup(
    name='guria',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires= [
        'flask',
        'pdf2image',
        'opencv-python',
        'opencv-contrib-python',
        'scikit-learn',
        'pandas',
        'pytesseract',
        'keras=2.2.4',
        'tensorflow>=2.1',
    ],
)