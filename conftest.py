import pytest
import tempfile
import os

from service.web import create_app
from service.web.db import get_db, init_db

@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()

    app = create_app({
        'TESTING' : True,
        'DATABASE' : 'sqlite:///{}'.format(db_path),
        'TRAINED_FEATURE_EXTRACTOR': './text/feature_extraction/vocabulary/tfidf_vocabulary.pkl'
    })

    with app.app_context():
        init_db()

    yield app

    # excluí arquivos temporários
    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def client(app):
    return app.test_client()

@pytest.fixture
def runner(app):
    return app.test_cli_runner()