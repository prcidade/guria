from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.stem.snowball import PorterStemmer
from nltk.stem import RSLPStemmer
from nltk.corpus import floresta
import re

import string

def get_stop_words():
    return ['amplos', 'sejam', 'deste', 'das', 'como', 'umas', 'deverá', 'devera', 'aqueles', 'la', 'estes', '`', 
            'dele', 'teve', 'seus', 'te', 'tido', 'houverem', 'pouco', 'às', 'as', 'devia', 'eram', 'alguma', 'própria',
            'propria', '=', 'tivéramos', 'tiveramos', 'depois', 'disso', 'pelo', 'poucas', '+', 'antes', 'pequenas', 
            'na', 'mas', 'todo', 'tenho', 'sua', 'estejam', 'houvéramos', 'houveramos', 'primeiro', 'também', 'tambem',
            'estava', '|', 'vocês', 'voces', '_', 'dito', 'seríamos', 'temos', 'tivemos', 'numa', 'teriam', 'nenhum',
            'com', 'feita', 'último', 'ultimo', 'aquelas', 'esta', 'muitos', 'disse', 'per', 'agora', 'elas', 'hajam', 
            'talvez', 'hei', 'qual', 'outro', 'deveria', 'toda', 'dessas', 'estive', 'do', 'poderiam', 'poderia', 
            'meus', 'quantos', 'nas', 'têm', 'tem', 'esteja', 'fosse', 'houveremos', 'seriam', 'minha', '$', 'estivesse',
            'estamos', 'pequeno', 'a', 'estivéssemos', 'estivessemos', 'tiveram', '?', 'este', 'houver', 'ninguém', 
            'ninguem', '(', 'teu', 'teria', 'mesma', 'meu', 'para', '/', 'nós', 'nos', 'estou', 'está', 'esta', 
            'houveríamos', 'houveríamos', 'ele', 'últimos', 'ultimos', '"', 'houverá', 'houvera', 'não', 'nao', 
            'tivessem', 'havia', 'era', 'dela', 'porque', 'éramos', 'eramos', '&', 'mesmos', 'dos', 'dever', 'si', 
            'outros', '{', 'e', 'tinham', '^', 'um', '#', 'estiveram', 'estas', 'tínhamos', 'tinhamos', 'isto', 'podia',
            'aquele', 'estivermos', 'da', 'já', 'ja', 'houvéssemos', 'houvessemos', 'tudo', 'fôramos', 'foramos',
            'pela', 'mesmas', 'suas', 'pequenos', 'algum', 'feitas', 'sobre', 'amplo', 'os', 'próprias', 'proprias', 
            'ante', '<', 'tivera', 'feitos', 'aquilo', 'poucos', 'estivéramos', 'estiveramos', 'estivessem', 'até', 
            'ate', 'lo', 'vendo', 'somos', 'enquanto', 'serão', 'serao', 'deverão', 'deverao', 'tua', 'uns', 'houvesse',
            'pelas', 'nestas', 'todos', 'sido', 'ao', 'por', 'próprios', 'proprios', 'nos', 'desse', 'se', 'quanto', 
            'fomos', '@', 'eu', 'ter', 'tiverem', 'estavam', '.', '[', 'lhes', 'fazer', 'pequena', 'diz', 'houverão',
            'houverão', 'delas', 'teríamos', 'teriamos', 'primeiros', 'estivera', 'nem', 'minhas', 'tuas', 'sempre', 
            'são', 'sao', 'deveriam', 'estiver', 'deles', 'terá', 'tera', 'teremos', 'eles', 'ti', 'através', 'atraves',
            'coisa', '!', ',', 'esse', 'o', 'ou', 'disto', 'outra', '\\', 'ser', 'tivermos', 'contudo', 'lhe', 'quais', 
            'entre', 'contra', 'seu', 'desta', 'tive', 'uma', 'ela', 'as', 'sendo', 'todavia', 'pelos', 'tém', 'no', 
            'esteve', 'grandes', 'vós', 'vos' 'foram', 'formos', 'ver', 'alguns', 'últimas', 'ultimas', 'perante', ':', 
            'houveriam', 'nossos', 'pude', 'ampla', 'forem', 'tivesse', 'me', 'cada', 'teus', 'destes', 'será', 'sera', 
            'houvermos', 'sou', 'nessas', 'quando', 'vindo', 'há', 'ha', 'amplas', 'tu', 'desses', 'nessa', 'coisas', 
            'próprio', 'proprio', '%', 'haja', 'podendo', 'tivéssemos', 'tivessemos', 'havemos', 'estiverem', 
            'daqueles', 'fui', 'sob', 'só', 'so', 'à', 'houvera', 'muitas', 'tinha', 'houve', 'quem', 'tenham', 
            'houveria', 'vez', 'terão', 'terao', 'nossas', 'seria', 'fôssemos', 'fossemos', 'podiam', 'isso', 'nosso', 
            'aos', 'muito', 'sem', '~', '>', 'terei', 'ainda', 'estivemos', 'tiver', 'destas', 'pode', 'de', 'essa', 
            'lá', 'la', '-', 'tenha', '*', 'nunca', 'houverei', 'vir', 'devem', ']', 'posso', 'estejamos', "'", 'última',
            'ultima', 'num', 'você', 'voce', 'porém', 'porem', 'que', 'essas', 'fossem', 'tenhamos', ';', 'algumas', 
            'estão', 'estao', 'nesta', 'pois', 'aquela', 'tendo', 'hão', 'hao', 'devendo', 'outras', 'poder', 'feito', 
            'todas', 'dizem', 'estávamos', 'estávamos', 'fora', 'tampouco', 'daquele', 'pouca', 'mais', 'houvemos', 
            'for', '}', 'deviam', 'esses', 'após', 'apos', 'hajamos', 'nossa', 'houvessem', 'sejamos', 'grande', 
            'houveram', ')', 'serei', 'muita', 'fazendo', 'em', 'seja', 'alguém', 'alguem', 'mesmo', 'dessa', 'foi', 
            'deve', 'seremos', '//t', '—', '...', '/', '-', 'ú', 'ù', 'é', 'è', 'ì', 'í', '§', '¢', 'r$', '£', 'pra', 
            'pro'
        ]
    
def clear_words(words, min_len=3, as_list = False):

    # Substitui caracteres indesejados por espaco
    words = words.replace('.',' ') \
                .replace('!',' ') \
                .replace('?',' ') \
                .replace(':',' ') \
                .replace(';',' ') \
                .replace(',',' ') \
                .replace('|',' ') \
                .replace('#',' ') \
                .replace('@',' ') \
                .replace('\r',' ') \
                .replace('\\r',' ') \
                .replace('\\',' ') \
                .replace('\n',' ') \
                .replace('\\n',' ') \
                .replace('\t',' ') \
                .replace('\\t',' ') \
                .replace('...',' ') \
                .lower()

    words = re.sub(r'\s+', ' ', words)
    words = re.sub(r'\n+', ' ', words)

    tokens = tokenize(words)
    clear_tokens = remove_stopwords(tokens)

    if min_len > 0:
        clear_tokens = [token for token in clear_tokens if len(token) >= min_len]
    
    if as_list:
        return clear_tokens

    return ' '.join(clear_tokens)


def remove_unknown_tokens(tokens):
    vocabolary = set(floresta.words()) # palavras do vocabulário foresta
    vocabolary = [ v.lower() for v in vocabolary ] # converte para minuscula
    
    return [token for token in tokens if token in vocabolary]

def stem_sentence(text, remove_stopwords=False, stemmer=None):
    tokens = tokenize(text)
    
    if remove_stopwords:
        tokens = remove_stopwords(tokens)
        
    if stemmer is None:
        stemmer = RSLPStemmer()
        
    stemmed_tokens = [stem_token(token, stemmer=stemmer) for token in tokens]
        
    return ' '.join(stemmed_tokens)

def stem_token(token, stemmer=None):
    if stemmer is None:
        stemmer = RSLPStemmer()
    return stemmer.stem(token)

def tokenize(text):
    return word_tokenize(text.lower(), language='portuguese')


def split_sentences(text):
    return sent_tokenize(text, language='portuguese')

def remove_stopwords(tokens):
    stop_words = get_stop_words()
    return [token for token in tokens if token not in stop_words]
