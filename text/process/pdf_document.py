import io
from enum import Enum, unique

from pdfminer.high_level import extract_pages
from pdfminer.layout import LTTextContainer, LTImage, LTFigure, LTPage


from  PIL import Image
import tempfile

from conversor import files_to_image

class PDFDocument:

    def __init__(self, file_path, load_pages = False):
        self.file_path = file_path
        self.pages = []
        self.pdf_pages = []

        if load_pages:
            self.load_pages()

    def load_pages(self, max_pages : int = None):
        """Carrega o conteúdo das páginas para a memória.

        Args:
            max_pages (int, optional): número máximo de páginas para serem carregadas. Defaults to None.
        """
        self.pages = []

        for page_number, page in enumerate(self.get_pdf_pages(max_pages=max_pages), start=1):
            self.pages.append({
                'page': page_number,
                'text': page.get_text() 
            })

    def get_text(self, page_number : int = None):
        """Recupera o texto do arquivo PDF

        Args:
            page_number (int, optional): Número da página para recuperar o texto. Defaults to None.

        Raises:
            IndexError: Exceção lançada caso a página informada não exista.

        Returns:
            str: texto extraido
        """
        
        if len(self.pages) == 0:
            self.load_pages()

        # Ajusta o indice da página
        page_index = page_number - 1
        if page_index < 0 or page_index > len(self.pages):
            raise IndexError('Página não encontrada')

        if page_number is None:
            return '\n'.join([ page['text'] for page in self.pages ])

        return self.pages[page_number - 1]['text']

    def get_pdf_pages(self, max_pages=None):
        """Recupera as páginas do arquivo em PDF

        Args:
            max_pages (int, optional): número máximo de páginas para serem recuperadas. Defaults to None.

        Returns:
            list: Lista com objetos PDFPage representando cada página
        """    
        if max_pages is None:
            max_pages = 0

        if len(self.pdf_pages) > 0:
            return self.pdf_pages

        pages = extract_pages(self.file_path, maxpages=max_pages)

        self.pdf_pages = []
        for page_number, page in enumerate(pages, start=1):
            self.pdf_pages.append(PDFPage(self.file_path, page, number=page_number))

        return self.pdf_pages

    def get_content(self):
        """Recupera uma lista com o conteúdo do texto, cada item da lista representa uma página do documento,
        contento um dicionário com uma chave para o texto e uma chave para o número da página.

        Returns:
            list: conteúdo das páginas do documento
        """
        return self.pages


class PDFPage:

    def __init__(self, file_path, page, number=0):
        self.file_path = file_path
        self.page = page
        self.number = number
        # Limiares de ocupação de página para determinar se a página é ocupada por uma imagem
        self.th_width_page_is_image = 0.8
        self.th_height_page_is_image = 0.8


    def is_image(self) -> bool:
        """Determina se o conteúdo da página é uma image.
        É considerada imagem uma página que possua um único elemento que ultrapasse os limites definidos. 

        Returns:
            Boolean: verdadeiro se a maior parte do conteúdo da página for uma imagem
        """
        for element in self.page:
            if isinstance(element, LTFigure):
                for img in element:    
                    if not isinstance(img, LTImage):
                        continue

                    page_width = self.page.x1 - self.page.x0
                    img_width = img.x1 - img.x0

                    page_height = self.page.y1 - self.page.y0
                    img_height = img.y1 - img.y0
                    
                    width_percent = img_width / page_width
                    height_percente = img_height / page_height
                    
                    # imagem ocupa mais que 80% da página
                    if width_percent >= self.th_width_page_is_image or height_percente >= self.th_height_page_is_image:
                        return True
            
        return False

    def get_image(self):
        """Recupera a primeira imagem da página.

        Returns:
            PIL.Image: Imagem da página
        """
        return self.get_images()[0]
        
    def get_images(self) -> list:
        """Recupera as imagens da página.

        Returns:
            list: Imagens da página
        """
        images = []
        for element in self.page:
            if not isinstance(element, LTFigure):
                continue
            
            for img in element:
                
                if not isinstance(img, LTImage):
                    continue
            
                # Converte a página do PDF para imagem se não for possível extraí-la com o pdfminer
                with tempfile.TemporaryDirectory() as tmp_dir:
                    image = files_to_image.convert_pdf(self.file_path, output_path=tmp_dir, dpi=300, first_page=self.number, last_page=self.number)
                    # Ajuste para o caso da conversão não respeitar a página que deve ser extraída
                    index = 0
                    if len(image) >= self.number:
                        index = self.number - 1
                
                images.append(image[index])
                
        return images

    def has_text(self) -> bool:
        """Verifica se a página possui conteúdo textual não digitalizado

        Returns:
            bool: verdadeiro se houver conteúdo textual.
        """
        for element in self.page:
            if isinstance(element, LTTextContainer):
                return True
        
        return False

    def get_text(self) -> str:
        """Recupera o conteúdo textual da página.

        Returns:
            str: texto da página.
        """
        text = ''

        for element in self.page:
            if isinstance(element, LTTextContainer):
                text += element.get_text()

        return text

    def save_as_image(self, path : str):
        """Converte a página para uma imagem JPEG. A página será salva no diretório informado.

        Args:
            path (str): caminho em que a página será salva

        Returns:
            bytes: Imagem convertida
        """
        image = files_to_image.convert_pdf(self.file_path, output_path=path, dpi=300, first_page=self.number, last_page=self.number)

        return image[0]
