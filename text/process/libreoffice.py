import os
import tempfile
import subprocess

class LibreOffice:
    """Encapsula a chamada de sistema do LibreOffice.
    É preciso que o programa do libreoffice estaja instalado na máquina para poder utilizar essa classe
    """

    def get_plain_text(self, file_path : str) -> str:
        params = ['--cat', file_path]

        return self.call(params)

    def convert_doc_to_pdf_file(self, file_path : str, output_path : str):

        original_name = os.path.basename(file_path)
        input_dir = os.path.dirname(file_path)

        file_name = os.path.basename(file_path)
        output_dir = output_path
        
        # se informado um arquivo separa as informações de nome e diretório
        if output_path.endswith('.pdf'):
            file_name = os.path.basename(output_path)
            output_dir = os.path.dirname(output_path)
            
        params = ['--convert-to', 'pdf', file_path, '--outdir', output_dir]
        self.call(params)

        extension = os.path.splitext(original_name)[1]
        output_file = os.path.join(output_dir, original_name.replace(extension, '.pdf'))
        # renomeia o arquivo de saída
        if original_name != file_name:
            original_output_file = output_file
            final_output_file = os.path.join(output_dir, file_name)

            os.rename(original_output_file, final_output_file)
            output_file = final_output_file

        return output_file

    def call(self, params=[]):
        """Realiza uma chamada de sistema do programa libreoffice

        Args:
            params (list, optional): Argumentos a serem utilizados na invocação. Defaults to [].

        Raises:
            FileNotFoundError: Exceção lançada caso o libreoffice não esteja instalado na máquina

        Returns:
            str: saída de texto produzida pela invocação
        """
        try:
            call_process = ['libreoffice', '--headless'] + params
            result = subprocess.run(call_process, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except FileNotFoundError:
            raise FileNotFoundError('O programa libreoffice não foi encontrado') 

        return result.stdout.decode('utf-8')
