import os
import tempfile
import mimetypes
import subprocess
import docx
from text.process.pdf_document import PDFDocument
from conversor import files_to_pdf

class WordDocument:

    def __init__(self, file_path):
        self.file_path = file_path
        self.document = self.resolve_document_type(file_path)

    def resolve_document_type(self, file_path):
        mime = mimetypes.guess_type(file_path)[0]

        if mime == 'application/msword':
            return DocDocument(self.file_path)
        elif mime == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            return DocxDocument(self.file_path)
        else:
            raise Exception('O formato de arquivo não é conhecido')

    def get_content(self):
        return self.document.get_content()

    def get_plain_text(self):
        return self.document.get_plain_text()
    
    @classmethod
    def has_support_for_mime(cls, mime):
        return mime == 'application/msword' or mime == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'

class DocxDocument():
    def __init__(self, file_path):
        self.file_path = file_path

    def get_content(self):
        """Recupera o texto de um documento word docx.
        O textos dos parágrafos são concatenados e separados por uma quebra de linha.
        Para cada parágrafo é buscado no XML uma identificação de quebra de página para identificar uma nova página.
        Se nenhuma quebra de página for encontrada todo o texto será associado a primeira página.

        Returns:
            list: lista contendo dicionários com o texto e o número da página de cada página extraída.
        """

        doc = docx.Document(self.file_path)
        
        content = []
        has_break_page = False
        page = 1
        page_content = ''
        for p in doc.paragraphs:
            
            page_content += p.text + '\n'
            for run in p.runs:
                
                # Busca quebra de página no documento
                if ('lastRenderedPageBreak' in run._element.xml) or ('w:br' in run._element.xml and 'type="page"' in run._element.xml):  
                    content.append({
                        'text' : page_content,
                        'page': page,
                    })

                    page += 1
                    page_content = ''
                    
                    has_break_page = True
                    break

        # Não encontrou quebra de página
        if not has_break_page:
            content.append({
                'text': page_content,
                'page': page,
            })

        return content

    def get_plain_text(self):
        doc = docx.Document(self.file_path)

        return '\n'.join([p.text for p in doc.paragraphs])



class AntiWord:
    """Encapsula o programa antiword para processamento de textos de documentos do word
    """

    def __init__(self):
        self.config = {}

    def get_plain_text(self, file_path : str) -> str:
        """Extrai o texto de um arquivo do word em disco

        Args:
            file_path (string): caminho para o arquivo em disco

        Raises:
            Exception: Uma exceção é lançada se o arquivo não for suportado pelo antiword

        Returns:
            (str): saída do programa
        """

        mime = mimetypes.guess_type(file_path)[0]
        if mime != 'application/msword':
            raise Exception('Antiword suporta apenas documentos do MS Word 2003 ou anteriores.')

        output =  self.call( self.config_to_process_parameters() + [ file_path ])

        antiword_unable_to_process_error = file_path + ' is not a Word Document.'

        if antiword_unable_to_process_error in output:
            raise Exception('Não foi possível processar o arquivo. Antiword suporta apenas documentos do MS Word 2003 ou anteriores.')

        return output


    def to_pdf_document(self, file_path : str, char_mapping : str = '8859-1', paper_size : str ='a4'):
        """Converte um arquivo .doc em um objeto PDF com as páginas na memória.

        Args:
            file_path (str): caminho para o arquivo .doc
            char_mapping (str, optional): mapeamento de caracteres para a conversão em PDF. Defaults to '8859-1'.
            paper_size (str, optional): tamanho do papel utilizado. Defaults to 'a4'.

        Returns:
            PDFDocument: Representação do documento convertido para PDF em um objeto
        """

        fd, path = tempfile.mkstemp(suffix=".pdf")
        try:
            self.to_pdf_file(file_path, path, char_mapping=char_mapping, paper_size=paper_size)
            pdf_document = PDFDocument(path, load_pages=True)
            return pdf_document
        finally:
            os.remove(path)


    def to_pdf_file(self, file_path : str, output_file : str, char_mapping : str ='8859-1', paper_size : str ='a4'):
        """Converte um arquivo .doc em um PDF, salvando no arquivo especificado

        Args:
            file_path (str): caminho para o arquivo .doc de entrada
            output_file (str): caminho para o arquivo .pdf de saída
            char_mapping (str, optional): mapeamento de caractéres para conversão em PDF. Defaults to '8859-1'.
            paper_size (str, optional): tamanho do papel para o PDF. Defaults to 'a4'.

        Raises:
            ValueError: É esperado que o caminho de saída seja um arquivo com extensão .pdf

        Returns:
            str: Caminho para o arquivo .pdf gerado. Mesmo valor do argumento output_file.
        """
        self.set_mapping(char_mapping)
        self.set_paper_size(paper_size)
        text = self.get_plain_text(file_path)

        with open(output_file, 'w') as fp:
            fp.write(text)

        return output_file


    def call(self, params=[]):
        """Invoca o antiword com os parâmetros informados

        Args:
            params (list, optional): Argumentos passados para a invocação do programa. Defaults to [].

        Raises:
            FileNotFoundError: Exceção lançada quando o programa antiword não é encontrado

        Returns:
            string: Saída da execução do programa
        """
        try:
            call_process = ['antiword'] + params
            result = subprocess.run(call_process, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except FileNotFoundError:
            raise FileNotFoundError('O programa antiword não foi encontrado') 

        # Há configuração específica para encode dos carácteres
        if 'm' in self.config:
            return result.stdout.decode('iso-{}'.format(self.config['m']))

        return result.stdout.decode('utf-8')

        
    def set_mapping(self, mapping):
        """Define o parâmetro de mapeamento de caracteres do antiword.

        Args:
            mapping (str): encoding para mapear os caractéres
        """
        self.config['m'] = mapping


    def set_paper_size(self, paper_size):
        """Define o parâmetro de tamanho do papel utilizado pelo antiword.

        Args:
            paper_size (str): Tamanho do papel utilizado. Aceita valores como: a4, letter or legal
        """
        self.config['a'] = paper_size


    def config_to_process_parameters(self):
        """Converte os parâmetros de configuração para argumentos na invocação do processo do antiword.

        Returns:
            list: parâmetros a serem passados para o processo.
        """
        parameters = []
        for key in self.config:
            parameters.append('-{}'.format(key))
            parameters.append(self.config[key])

        return parameters


class DocDocument():

    def __init__(self, file_path):
        super().__init__()
        self.file_path = file_path
    

    def get_content(self):
        """Recupera uma lista com o conteúdo do texto, cada item da lista representa uma página do documento,
        contento um dicionário com uma chave para o texto e uma chave para o número da página.

        Returns:
            list: conteúdo das páginas do documento
        """
        doc_as_pdf = files_to_pdf.convert_doc_to_pdfdocument_object(self.file_path)

        return doc_as_pdf.get_content()


    def to_pdf_file(self, file_path : str, output_file : str) -> str:
        """Converte um arquivo .doc de entrada em um arquivo .pdf

        Args:
            file_path (str): caminho para o arquivo de entrada
            output_file (str): caminho para o arquivo de saída

        Returns:
            str: caminho de saída do documento, mesmo que o output_file
        """
        return files_to_pdf.convert_doc(file_path, output_file)