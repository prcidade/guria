import pickle
import os
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import logging
from text.feature_extraction.text_extractor import TextExtractor  
from text.process import text_processor

class TfidfFileExtractor(TfidfVectorizer):
    """Textrai o TF-IDF de arquivos do disco se informado o caminho
    Caso contrário extrái o texto.
    """

    def __init__(self, clear_text=True, token_pattern=r'\w{3,}', max_features=1000, vocabulary=None):
        super().__init__(analyzer='word', token_pattern=token_pattern, max_features=max_features, vocabulary=vocabulary)

        self.clear_text = clear_text
        self.text_extractor = TextExtractor()

    def transform(self, raw_documents, copy="deprecated"):
        
        if type(raw_documents) == str:
            # extrai o texto do arquivo informado
            if os.path.isfile(raw_documents):
                raw_documents = self.text_extractor.transform(raw_documents)
            
            # limpa o texto
            if self.clear_text:
                raw_documents = text_processor.clear_words(raw_documents)

            raw_documents = [ raw_documents ]

        transformed_data = super().transform(raw_documents, copy=copy) 
        transformed_data.sort_indices()

        return transformed_data

    def save(self, path : str):
        """Salva o extrator em um arquivo em disco

        Args:
            path (str): caminho para salvar o arquivo
        """
        
        with open(path, 'wb') as output:
            pickle.dump(self.__dict__, output, pickle.HIGHEST_PROTOCOL)


    def load(self, path : str):
        """Carrega um extrator do disco

        Args:
            path (str): caminho para o arquivo do extrator salvo

        Returns:
            TfidfFileExtractor: instancia carregada do disco
        """
        with open(path, 'rb') as f:
            tmp_dict = pickle.load(f)

            self.__dict__.update(tmp_dict) 
        
        return self

    def is_fitted(self):
        """Verifica se o extrator já foi treinado

        Returns:
            bool: verdadeiro se já estiver treinado
        """
        return hasattr(self, 'vocabulary_') and self.vocabulary_ is not None