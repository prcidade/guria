"""Extrai texto de arquivos
    Aceita zip, PDF e imagens
    Se for informado um zip os arquivos serão descompactados em um diretório temporário
    Os documentos em PDF serão convertidos para imagem e terão o texto extraído através de um OCR.
    Pode-se aumentar o número de DPI na conversão de PDF para imagens para melhorar a extração do texto.
    Imagens terão o texto extraído através de um OCR, por padrão a será aplicado deskew no documento antes da extração.
"""

import mimetypes
import tempfile
import numpy as np
from zipfile import ZipFile
import PIL
from glob import glob
import os
import io
from enum import Enum, unique
from functools import reduce

from conversor import files_to_image
from image.ocr import image_ocr
from image.process import image_processor
from text.process.pdf_document import PDFDocument, PDFPage
from text.process.word_document import WordDocument

# Tipos aceitos pelo extrator
ALLOWED_MIMES = ['zip', 'pdf', 'image']

# Número padrão de páginas a serem analisadas
DEFAULT_MAX_PAGES = 5

# valor do DPI padrão 
DEFAULT_IMAGES_DPI = 300

IMAGE_SIZE = (1024, 1024)

@unique
class ReadMode(Enum):
    """Modo de leitura dos arquivos enviados.
    ocr converte os arquivos para imagens e extrai através de um OCR.
    scrape, para PDF, tenta ler o arquivo primeiro e se constatar que a página é (quase) toda imagem realiza um OCR.
    """
    OCR_MODE = 'ocr'
    SCRAPE_MODE = 'scrape'

@unique
class ExtractionType(Enum):
    """Formas de extração de texto dos arquivos enviados.
    text-only retorna apenas o texto extraído.
    text-and-page retorna uma lista de dicionários, contendo o texto e o número da página que o texto foi extraído.
    """
    EXTRACT_TYPE_TEXT_ONLY = 'text-only'
    EXTRACT_TYPE_TEXT_AND_PAGE = 'text-and-page'

class TextExtractor:

    def __init__(self, max_pages=DEFAULT_MAX_PAGES, image_dpi=DEFAULT_IMAGES_DPI, read_mode=ReadMode.OCR_MODE, extraction_type=ExtractionType.EXTRACT_TYPE_TEXT_ONLY):
        if max_pages is None:
            self.max_pages = -1
        else:
            self.max_pages = max_pages
        
        self.image_dpi = image_dpi
        self.read_mode = read_mode
        self.extraction_type = extraction_type

    def transform(self, file_path):
        """Alias para extract_text_from_file
        Extraí o texto a partir de um arquivo em disco

        Args:
            file_path (str): caminho para o arquivo a ser analizado
        """

        return self.extract_text_from_file(file_path)


    def extract_text_from_file(self, file_path : str):
        """Extraí o texto de um arquivo informado. São aceitos documentos zip, PDF e imagens.
        O formato do retorno é  definido pelo valor do atributo extract_type, pondendo retornar apenas texto ou uma lista.

        Args:
            file_path (str): caminho para o arquivo em disco

        Returns:
            str|list: texto extraído do arquivo ou lista de dicionário com o texto e o número da página
        """
        mime = mimetypes.guess_type(file_path)[0]

        if mime is None:
            return ''
            
        content = None
        mime_type, mime_end = mime.split('/')
        if mime_end == 'zip':
            return self.extract_text_from_zip(file_path)
        
        elif mime_end == 'pdf':
            content = self.extract_text_from_pdf(file_path)
        
        elif mime_type == 'image':
            image = image_processor.load_img(file_path)
            content = self.extract_text_from_images( [image] )

        elif WordDocument.has_support_for_mime(mime):
            content = self.extract_text_from_word_document(file_path)
        
        # Define se será extraido somente o texto ou se será retornado o número da página
        if self.extraction_type is ExtractionType.EXTRACT_TYPE_TEXT_ONLY:
            if content is None:
                return ''

            return reduce(lambda x, y : x + y['text'] + '\n', content, '')
        
        elif self.extraction_type is ExtractionType.EXTRACT_TYPE_TEXT_AND_PAGE:
            return content


        raise ValueError('Modo de extração não reconhecido')

    def extract_text_from_zip(self, file_path):
        """Extraí o texto dos arquivos compactados em um ficheiro zip
        Os arquivos são descompactados em um diretório temporário.
        Apenas arquivos são considerados.

        Args:
            file_path (str): caminho até o ficheiro zip

        Returns:
            str: texto extraído dos arquivos dentro do zip
        """
        with tempfile.TemporaryDirectory() as tmp_dir:

            with ZipFile(file_path, 'r') as zip_file:
                zip_file.extractall(tmp_dir)

            text = []
            if self.extraction_type is ExtractionType.EXTRACT_TYPE_TEXT_ONLY:
                text = ''

            files = glob(os.path.join(tmp_dir, '*'))

            num_files_to_extract_text = min(len(files), self.max_pages)
            for file_path in files[:num_files_to_extract_text]:
                if os.path.isfile(file_path):
                    text += self.extract_text_from_file(file_path)

        return text

    def extract_text_from_pdf(self, file_path):
        """Extrai o texto de um documento PDF

        Args:
            file_path (str): caminho para o PDF

        Returns:
            list: Lista de dicionários contento o texto extraído e a página
        """

        if self.read_mode is ReadMode.OCR_MODE:
            
            return self.extract_text_from_pdf_with_ocr(file_path)

        elif self.read_mode is ReadMode.SCRAPE_MODE:
            try:
                return self.extract_text_from_pdf_scrape(file_path)
            except:
                # Fallback para OCR
                return self.extract_text_from_pdf_with_ocr(file_path)
        else:
            raise ValueError('O modo de leitura informado não é válido')

    def extract_text_from_pdf_with_ocr(self, file_path):
        """Extrai o texto do arquivo PDF com os recursos de OCR.
        As páginas do PDF serão convertidas para imagens em um diretório temporário e então passarão por um OCR.
        O DPI das imagens é definido pelo atributo image_dpi.
        O número máximo de páginas convertidas para imagens pode ser definido no atributo max_pages
        Args:
            file_path (string): caminho para o arquivo em PDF em disco.

        Returns:
            list: Lista de dicionários contento o texto extraído e a página
        """
        pages = []

        # utiliza o disco para evitar crash com arquivos grandes
        with tempfile.TemporaryDirectory() as tmp_dir:
            max_pages = self.max_pages
            if max_pages is not None and max_pages < 0:
                max_pages = None 
            try:
                pages = files_to_image.convert_pdf(file_path, output_path=tmp_dir, dpi=self.image_dpi, last_page=max_pages)
            except PIL.Image.DecompressionBombError:
                pages = files_to_image.convert_pdf(file_path, output_path=tmp_dir, dpi=self.image_dpi//2, last_page=max_pages)
                
        return self.extract_text_from_images(pages)
        

    def extract_text_from_pdf_scrape(self, file_path):
        """Extrai o texto de um arquivo PDF priorizando o conteúdo textual.

        Args:
            file_path (string): caminho para o arquivo

        Returns:
            list: Lista de dicionários contento o texto extraído e a página
        """
        pdf = PDFDocument(file_path)
        pages = pdf.get_pdf_pages()
        num_files_to_extract_text = min(len(pages), self.max_pages)
        pages_to_extract = pages[:num_files_to_extract_text]
        file_content = []
        if num_files_to_extract_text > 0:
            pages = pages[:num_files_to_extract_text]

        for page_number, pdf_page in enumerate(pages, start=1):
            file_content.append(self.get_text_from_pdf_page(pdf_page))

        return file_content


    def get_text_from_pdf_page(self, pdf_page : PDFPage):
        """Recupera o texto de uma página do PDF

        Args:
            pdf_page (PDFPage): Página do PDF

        Returns:
            [type]: Dicionário contendo o texto e o número da página
        """
        text = ''
            
        if pdf_page.is_image():
            text += self.extract_text_from_image( pdf_page.get_image() )
        elif pdf_page.has_text():
            text += pdf_page.get_text()

        # Caso não tenha conseguido extrair o texto converte para imagem e realiza um OCR
        if text == '':
            with tempfile.TemporaryDirectory() as tmp_dir:
                image = pdf_page.save_as_image(tmp_dir)
                text += self.extract_text_from_image( image )

        return {'text': text, 'page': pdf_page.number}        

    def extract_text_from_images(self, images):
        """Extrai o texto de um conjunto de imagens. Cada imagem da lista é considerada uma página.

        Args:
            images (list): Lista de imagens

        Returns:
            list: Lista de dicionários contento o texto extraído e a página
        """
        file_content = []

        max_pages = self.max_pages
        if max_pages is None or max_pages < 0:
            max_pages = len(images)

        num_files_to_extract_text = min(len(images), max_pages)
        for page_number, img in enumerate(images[:num_files_to_extract_text], start=1):
            text = self.extract_text_from_image(img)

            file_content.append({'text': text, 'page': page_number})


        return file_content

    def extract_text_from_image(self, img):
        """Extrai o texto de uma única imagem em memória.
        A imagem deve ser uma representação de PIL.Image

        Args:
            image (PIL.image): imagem para ter o texto extraído

        Returns:
            str: texto extraído da imagem
        """
        img = np.asarray(img)
        img = image_processor.convert_to_grayscale(img)
        
        if image_processor.is_img_gt_max_size(img):
            img = image_processor.thumbnail(img, IMAGE_SIZE)
            img = np.asarray(img)

        angle = 360.0 - image_ocr.get_angle_text_orientation(img, ignore_errors=True)
        img = image_processor.rotate_bound(img, angle)
       
        return image_ocr.get_text_from_image(img)


    def extract_text_from_word_document(self, file_path):
        """Extrai o texto de documentos do Word

        Args:
            file_path (str): caminho para o arquivo

        Returns:
            list: Lista de dicionários contendo o texto extraído e a página
        """
        doc = WordDocument(file_path)

        content = doc.get_content()

        if self.max_pages is not None:
            max_pages = min(len(content), self.max_pages)
            return content[:max_pages]

        return content

    def set_image_dpi(self, image_dpi : int):
        """Altera o número de DPI para ser utilizado na conversão de PDF para Imagem
        Quanto maior o DPI maior a resolução

        Args:
            image_dpi (int): número de DPI para ser utilizado na conversão de imagens
        """
        self.image_dpi = image_dpi


    def set_max_pages(self, max_pages : int):
        """Define o número máximo de páginas para ter o texto extraído

        Args:
            max_pages (int): Número de máximo de páginas para extrair o texto
        """
        self.max_pages = max_pages
