import sys
# sys.path.append('../')

import logging

from flask import Blueprint, request, render_template, jsonify, current_app, abort
from werkzeug.utils import secure_filename 
from conversor import files_to_image
from image.ocr import image_ocr
from text.process import text_processor
from text.feature_extraction.feature_extractor import TfidfFileExtractor
from text.feature_extraction.text_extractor import TextExtractor, ReadMode

from . import get_model
from service.io.upload.uploaded_file import UploadedFile
from service.classificador.ClassificadorDocumento import ClassificadorDocumento
from service.texto.ExtratorTexto import ExtratorTexto
from service.io.cache.cache_driver import get_cache_driver


import os

ALLOWED_EXTENSIONS_DEMO_CLASSIFICACAO = list(set(['pdf']))
ALLOWED_EXTENSIONS_DEMO_EXTRACAO = list(set(['pdf', 'jpg', 'jpeg', 'png', 'tif', 'tiff', 'doc', 'docx']))

bp = Blueprint('guria', __name__)

@bp.route('/')
def index():
    return render_template('index.html')


@bp.route('/documento/classificar/demo/', methods=['POST'])
def demo():
    data = {'success' : False, 'predictions': []}
    
    try:

        file = request.files['documento']

        uploaded_file = UploadedFile(file)

        if not uploaded_file.is_extension_allowed(ALLOWED_EXTENSIONS_DEMO_CLASSIFICACAO):
            abort(422, 'Extensão não permitida')

        dirpath, file_path = uploaded_file.save_temporary()

        feature_extractor = TfidfFileExtractor()
        feature_extractor.load(
            current_app.config['TRAINED_FEATURE_EXTRACTOR']
        )

        classificador = ClassificadorDocumento(get_model(), feature_extractor)
        predicted = classificador.classify_file(file_path)

        data['success'] = True
        data['predicted'] = predicted

        uploaded_file.delete()
        
    except Exception as e:
        logging.exception(e)
        pass
    
    return jsonify(data)


@bp.route('/documento/extrair-texto/demo/', methods=['POST'])
def extrair_texto():

    data = {'success': False}
    code = 200

    try:
        file = request.files['documento']
        uploaded_file = UploadedFile(file)

        if not uploaded_file.is_extension_allowed(ALLOWED_EXTENSIONS_DEMO_EXTRACAO):
            abort(422, 'Extensão não permitida')

        dirpath, file_path = uploaded_file.save_temporary()
        
        extrator = ExtratorTexto(cache_driver=get_cache_driver())

        data['success'] = True
        data['data'] = extrator.extract_text(file_path=file_path, **dict(request.form))

        uploaded_file.delete()
        
    except Exception as e:
        data['message'] = str(e)
        data['success'] = False
        logging.exception(e)
        code = 400

    return jsonify(data), code
