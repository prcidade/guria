'''
Middleware para tratar proxy reverso
A ideia é adicionar uma prefixo à URL da aplicação para atender o mapeamento
Ler mais em:
https://stackoverflow.com/questions/18967441/add-a-prefix-to-all-flask-routes/36033627#36033627
https://blog.macuyiko.com/post/2016/fixing-flask-url_for-when-behind-mod_proxy.html
'''
class PrefixMiddleware():

    def __init__(self, app, prefix=None, scheme=None, server=None):
        self.app = app
        self.prefix = prefix
        self.scheme = scheme
        self.server = server

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '') or self.prefix
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]
        scheme = environ.get('HTTP_X_SCHEME', '') or self.scheme
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        server = environ.get('HTTP_X_FORWARDED_SERVER', '') or self.server

        if server:
            environ['HTTP_HOST'] = server
        return self.app(environ, start_response)
