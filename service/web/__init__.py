import os

from flask import Flask, g
from .middleware.PrefixMiddleware import PrefixMiddleware
UPLOAD_FOLDER = 'tmp/uploaded'

model = None

def get_model():
    global model
    return model

def create_app(test_config=None):

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY = 'dev',
        DATABASE = 'sqlite:///{}'.format(os.path.join(app.instance_path, 'guria.sqlite')),
        UPLOAD_FOLDER = UPLOAD_FOLDER,
        URL_PREFIX = '',
        TRAINED_FEATURE_EXTRACTOR = '{}'.format(os.path.join(app.instance_path, 'tfidf_vocabulary.pkl'))
    )

    # Sobreescreve a chave
    app.secret_key = os.environ.get('SECRET_KEY', 'dev')

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    try:
        os.makedirs(app.config['UPLOAD_FOLDER'])
    except OSError:
        pass
    
    url_prefix = app.config['URL_PREFIX']
    app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix=url_prefix)


    from .api import client
    client.init_app(app)

    from . import db
    db.init_app(app)

    from service.io.cache import cache_driver
    cache_driver.init_app(app)

    from . import guria
    app.register_blueprint(guria.bp)
    app.add_url_rule('/', endpoint='index')

    from .api import auth
    app.register_blueprint(auth.bp)

    from .api import classificador_documento
    app.register_blueprint(classificador_documento.bp)

    from .api import extrator_texto
    app.register_blueprint(extrator_texto.bp)

    # Inicializa o modelo  
    from model.tiny_rna import init_base_model
    global model
    model = init_base_model()
    
    return app