import dataset

import click
from flask import current_app, g
from flask.cli import with_appcontext
import os

def get_db():
    if 'db' not in g:
        g.db = dataset.connect(current_app.config['DATABASE'])

    return g.db

def close_db(e=None):
    db = get_current_connection()

    if db is not None:
        del db

def get_current_connection():
    db = g.pop('db', None)
    return db

def init_db():
    db = get_db()
    client_table = db.create_table('client')
    client_table.create_column('name', db.types.text)
    client_table.create_index('name', unique=True)
    
    cache_table = db.create_table('cache')
    cache_table.create_column('key', db.types.text)
    cache_table.create_column('value', db.types.text)
    cache_table.create_column('created_at', db.types.datetime)
    cache_table.create_column('expires_at', db.types.datetime)

    client_table.create_index('key', unique=True)


@click.command('init-db')
@with_appcontext
def init_db_command():
    init_db()
    click.echo('Banco de dados inicializado')
    click.echo('Database {}'.format(current_app.config['DATABASE']))

def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
