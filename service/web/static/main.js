$(document).ready(function()
{
    let upload_classificacao_url = $('.classificacao .uploader').data('url');
    let upload_extracao_texto_url = $('.extrair-texto .uploader').data('url');

    $("#demo .classificacao .uploader").uploadFile({
        url: upload_classificacao_url,
        fileName: "documento",
        // maxFileCount:1,
        maxFileSize: 200 * 1024 * 1024, // 2MB
        acceptFiles: "application/pdf",
        dragDrop: false,
        dragDropStr: "Arraste um arquivo PDF aqui",
        cancelStr: "Cancelar",
        uploadStr: "Carregar documento <span>(Máx. 2MB)</span>",
        abortStr: "Cancelar",
        onSubmit:function(files)
        {
            $('#demo .result > div').each(function(index, elem){
                if(!$(elem).hasClass('displaynone')){
                    $(elem).addClass('displaynone');
                }
            })
        },
        onSuccess:function(files,data,xhr,pd)
        {   
            if(!data['success']){
                $('#demo .classificacao .result .unspected-error').removeClass('displaynone');
                return;
            }

            let prediction = data['predicted']['max']
            if(prediction.probability < 0.6){
                $('#demo .classificacao .result .unknown-file').removeClass('displaynone');
                return
            }

            let label = prediction['label'] 
            $('#demo .classificacao .result .predict .result-label').text(label);
            $('#demo .classificacao .result .predict').removeClass('displaynone');
        },
        customProgressBar: function(obj,s)
        {
            this.statusbar = $("<div class='upload-file'></div>");
            this.preview = $("<img class='ajax-file-upload-preview' />").width(s.previewWidth).height(s.previewHeight).appendTo(this.statusbar).hide();
            this.filename = $("<div class='upload-filename'></div>").appendTo(this.statusbar);
            this.progressDiv = $("<div class='ajax-file-upload-progress'>").appendTo(this.statusbar).hide();
            this.progressbar = $("<div class='ajax-file-upload-bar'></div>").appendTo(this.progressDiv);
            this.abort = $("<div>" + s.abortStr + "</div>").appendTo(this.statusbar).hide();
            this.cancel = $("<div>" + s.cancelStr + "</div>").appendTo(this.statusbar).hide();
            this.done = $("<div>" + s.doneStr + "</div>").appendTo(this.statusbar).hide();
            this.download = $("<div>" + s.downloadStr + "</div>").appendTo(this.statusbar).hide();
            this.del = $("<div>" + s.deleteStr + "</div>").appendTo(this.statusbar).hide();
            
            this.abort.addClass("custom-red");
            this.done.addClass("custom-green");
            this.download.addClass("custom-green");            
            this.cancel.addClass("custom-red");
            this.del.addClass("custom-red");
            
            if(obj.count++ %2 ==0)
                this.statusbar.addClass("even");
            else
                this.statusbar.addClass("odd"); 
            return this;
        }	
    });



    $("#demo .extrair-texto .uploader").uploadFile({
        url: upload_extracao_texto_url,
        fileName: "documento",
        // maxFileCount:1,
        maxFileSize: 2 * 1024 * 1024, // 2MB
        acceptFiles: "application/pdf, image/x-png, image/jpeg, image/jpg, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        dragDrop: false,
        dragDropStr: "Arraste um arquivo PDF aqui",
        cancelStr: "Cancelar",
        uploadStr: "Carregar documento <span>(Máx. 2MB)</span>",
        abortStr: "Cancelar",
        dynamicFormData: function()
        {
            var data = { 
                read_mode: $('#demo .extrair-texto .options [name="read_mode"]:checked').val(), 
                clear_text: $('#demo .extrair-texto .options [name="clear_text"]:checked').val(), 
                extraction: $('#demo .extrair-texto .options [name="extraction"]:checked').val(), 
                split_sentences: $('#demo .extrair-texto .options [name="split_sentences"]:checked').val(), 
                stemming: $('#demo .extrair-texto .options [name="stemmimg"]:checked').val(),
            }
            return data;
        },
        onSubmit: function(files)
        {
            $('#demo .extrair-texto .result > div').each(function(index, elem){
                if(!$(elem).hasClass('displaynone')){
                    $(elem).addClass('displaynone');
                }
            })

            $('#demo .extrair-texto .result .document-text').text('');
            $('#demo .extrair-texto .clear-result').addClass('displaynone');

        },
        onSuccess:function(files,data,xhr,pd)
        {
            let content = data['data'];

            let html = ''
            for(let c of content) {
                let text = c['text'];
                let page = c['page'];
                
                if(page > 0) {
                    html += `<h2>Página ${page}</h2>`; 
                }

                if( typeof(text) === 'string') {
                    html += '<p>';
                    
                    text = text.replace(/\n\s*\n/g, '\n');
                    
                    html += text.replace('\n', '<br/>');
                    html += '</p>';
                } else {

                    for(let sentence of text) {
                        html += `<p>${sentence}</p>`;
                    }
                }
                
            }   

            $('#demo .extrair-texto .result .document-text').html(html);
            // $('#demo .extrair-texto .result .document-text').html(data['data'].replace(/\n\s*\n/g, '\n').replaceAll('\n', '<br/>'));
            $('#demo .extrair-texto .result .document-text').removeClass('displaynone');
            $('#demo .extrair-texto .clear-result').removeClass('displaynone');
        },
        customProgressBar: function(obj,s)
        {
            this.statusbar = $("<div class='upload-file'></div>");
            this.preview = $("<img class='ajax-file-upload-preview' />").width(s.previewWidth).height(s.previewHeight).appendTo(this.statusbar).hide();
            this.filename = $("<div class='upload-filename'></div>").appendTo(this.statusbar);
            this.progressDiv = $("<div class='ajax-file-upload-progress'>").appendTo(this.statusbar).hide();
            this.progressbar = $("<div class='ajax-file-upload-bar'></div>").appendTo(this.progressDiv);
            this.abort = $("<div>" + s.abortStr + "</div>").appendTo(this.statusbar).hide();
            this.cancel = $("<div>" + s.cancelStr + "</div>").appendTo(this.statusbar).hide();
            this.done = $("<div>" + s.doneStr + "</div>").appendTo(this.statusbar).hide();
            this.download = $("<div>" + s.downloadStr + "</div>").appendTo(this.statusbar).hide();
            this.del = $("<div>" + s.deleteStr + "</div>").appendTo(this.statusbar).hide();
            
            this.abort.addClass("custom-red");
            this.done.addClass("custom-green");
            this.download.addClass("custom-green");            
            this.cancel.addClass("custom-red");
            this.del.addClass("custom-red");
            
            if(obj.count++ %2 ==0)
                this.statusbar.addClass("even");
            else
                this.statusbar.addClass("odd"); 
            return this;
        }	
    });


    $('#demo .extrair-texto .clear').on('click', function(e) {
        $('#demo .extrair-texto .result .document-text').text('');
        $('#demo .extrair-texto .result .document-text').addClass('displaynone');
        $('#demo .extrair-texto .clear-result').addClass('displaynone');

    })


    // Oculta o demo de extração de texto
    $('#demo .extrair-texto').hide();

    // Exibe o demo de extração de texto e oculta o demo de classificação
    $('#demo .classificacao .show-extracao-texto').on('click', function(e){
        $('#demo .classificacao').hide();
        $('#demo .extrair-texto').show();
    });

    // Exibe o demo de classificação e oculta o demo de extração de texto
    $('#demo .extrair-texto .show-classificacao').on('click', function(e){
        $('#demo .extrair-texto').hide();
        $('#demo .classificacao').show();
    });

    $('#demo .extrair-texto .options').hide();

    $('#demo .extrair-texto .show-more').on('click', function(e){
        $('#demo .extrair-texto .options').toggle();
        
        let text = $(this).text();

        text = text === 'Opções avançadas' ? 'Ocultar opções' : 'Opções avançadas';
        $(this).text(text);
    });

});