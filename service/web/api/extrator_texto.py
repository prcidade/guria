import sys
import logging
from flask import Blueprint, request, render_template, jsonify, current_app, abort

from service.io.upload.uploaded_file import UploadedFile
from service.texto.ExtratorTexto import ExtratorTexto
from service.web.api.token_handler import token_required
from service.io.cache.cache_driver import get_cache_driver

import os


bp = Blueprint('extrator_texto', __name__)

ALLOWED_EXTENSIONS = set(['pdf', 'jpg', 'jpeg', 'png','bmp', 'tif', 'tiff', 'zip', 'doc', 'docx'])

@bp.route('/documento/extrair-texto', methods=['POST'])
@token_required
def extrair_texto(current_client):

    data = {'success': False}
    code = 200

    try:
        file = request.files['documento']
        uploaded_file = UploadedFile(file)

        if not uploaded_file.is_extension_allowed(ALLOWED_EXTENSIONS):
            abort(422, 'Extensão não permitida')

        dirpath, file_path = uploaded_file.save_temporary()

        extrator = ExtratorTexto(cache_driver=get_cache_driver())

        data['success'] = True
        data['data'] = extrator.extract_text(file_path=file_path, **dict(request.form))

        uploaded_file.delete()
        
    except Exception as e:
        data['message'] = str(e)
        logging.exception(e)
        code = 400

    return jsonify(data), code

