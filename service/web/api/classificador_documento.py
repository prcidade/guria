import sys
import logging

from flask import Blueprint, request, render_template, jsonify, current_app, abort
from werkzeug.utils import secure_filename 
from text.feature_extraction.feature_extractor import TfidfFileExtractor 
from ..guria import get_model
from ...classificador.ClassificadorDocumento import ClassificadorDocumento
from ...io.upload.uploaded_file import UploadedFile
from .token_handler import token_required
import shutil

import os

bp = Blueprint('classificador_documento', __name__)

ALLOWED_EXTENSIONS = set(['pdf', 'jpg', 'jpeg', 'png', 'tif', 'tiff', 'zip'])

@bp.route('/documento/classificar', methods=['POST'])
@token_required
def classificar_documento(current_client):

    data = {'success': False}
    code = 200
    try:
        file = request.files['documento']
        uploaded_file = UploadedFile(file)

        if not uploaded_file.is_extension_allowed(ALLOWED_EXTENSIONS):
            abort(422, 'Extensão não permitida')

        dirpath, file_path = uploaded_file.save_temporary()

        feature_extractor = TfidfFileExtractor()
        feature_extractor.load(current_app.config['TRAINED_FEATURE_EXTRACTOR'])

        classificador = ClassificadorDocumento(get_model(), feature_extractor)
        predicted = classificador.classify_file(file_path)

        data['success'] = True
        data['predicted'] = predicted

        uploaded_file.delete()
        
    except Exception as e:
        data['message'] = str(e)
        logging.exception(e)
        code = 400

    return jsonify(data), code