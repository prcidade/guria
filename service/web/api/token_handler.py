from flask import current_app, request, jsonify
from functools import wraps
import jwt
import datetime

from .client import Client

def token_required(f):
    @wraps(f)
    def check_token(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message': 'Token não informado'}), 400

        try:
            client = get_client_by_token(token)
            if client.id is None:
                raise Exception('Cliente não encontrado')
        except:
            return jsonify({'message': 'Cliente não identificado'}), 400
        
        return f(client, *args, **kwargs)

    return check_token

def get_client_by_token(token):
    
    token = decode(token)

    client_id = token['client_id']
    client = Client()
    return client.find(client_id)

def get_expiration_time_from_token(token):
    decoded_token = decode(token)

    return decoded_token['exp']

def decode(encoded_token):
    return jwt.decode(encoded_token, current_app.config['SECRET_KEY'], algorithms='HS256')

def encode(payload={}, expiration_time=30):
    expire_in = datetime.datetime.utcnow() + datetime.timedelta(minutes=expiration_time)
    default_payload = {'exp' : expire_in}
    token = jwt.encode({**payload, **default_payload}, current_app.config['SECRET_KEY'])

    return token