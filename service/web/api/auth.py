from flask import Blueprint, request, render_template, jsonify, current_app
import datetime
from .token_handler import encode as token_encode
from .token_handler import get_expiration_time_from_token
from .client import Client

bp = Blueprint('auth', __name__)

@bp.route('/api/authenticate/', methods=['POST'])
def authenticate():
    client_id = request.form.get('client_id', None)
    secret = request.form.get('secret', None)

    if client_id is None:
        return jsonify({'message': 'O ID do cliente é obriatório'}), 422

    if secret is None:
        return jsonify({'message': 'O segredo do cliente é obrigatório'}), 422

    client = Client()
    if not client.authenticate(client_id, secret):
        return jsonify({'message': 'Credenciais inválidas'}), 422


    token = token_encode({'client_id': client.id})
    expire_in = get_expiration_time_from_token(token)
    
    return jsonify({'token' : token.decode('utf8'), 'expire_in': expire_in})