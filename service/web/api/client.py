import click
from flask.cli import with_appcontext
from service.web.db import get_db
from werkzeug.security import generate_password_hash, check_password_hash

class Client:

    def __init__(self):
        self.table_name = 'client'
        self.db = get_db() 
        self.db_table = self.db[self.table_name]
        self._db_entry_to_object(None)

    def authenticate(self, client_id, secret):
        client = self.db_table.find_one(id = client_id)
        if client is None:
            return False
        
        if not check_password_hash(client['secret'], secret):
            return False

        self._db_entry_to_object(client)
        return True

    def create(self, name, password, description=None):
        self.id = self.db_table.insert(dict(name=name, secret=generate_password_hash(password), description=description))
        self.name = name
        self.description = description

    def find(self, id):
        entry = self.db_table.find_one(id=id)
        
        return self._db_entry_to_object(entry)

    def find_by_name(self, name):
        entry = self.db_table.find_one(name=name)
        
        return self._db_entry_to_object(entry)

    def delete_by_name(self, name):
        self.db_table.delete(name=name)

        return self._db_entry_to_object(None)

    def all(self):
        return list(self.db_table.all())

    def _db_entry_to_object(self, db_entry):
        """Mapeia uma entrada do banco de dados para o objeto
        Se a entrada for None todos os campos serão None

        Args:
            db_entry (Dict): dicionário com representação dos dados no banco de dados 

        Returns:
            [Client]: instancia do objecto com atributos preenchidos com informações do banco de dados
        """
        if db_entry is None:
            self.id = None
            self.name = None
            self.description = None
            
            return self

        self.id = db_entry['id']
        self.name = db_entry['name']
        self.description = db_entry['description']

        return self

@click.command('create-client')
@with_appcontext
@click.option('--name', prompt="Nome do Cliente (username)")
@click.option('--description', prompt="Descrição do Cliente")
@click.password_option()
def create_client_command(name, description, password):
    click.echo('Cliente: {}'.format(name))
    
    client = Client()
    client.create(name, password, description=description)
    
    click.echo('Cliente criado com sucesso')


def init_app(app):
    app.cli.add_command(create_client_command)