# Número máximo de páginas para serem processadas
DEFAULT_MAX_PAGES = 5

class ClassificadorDocumento:
    """Realiza a classficação de um documento informado dentro das classes conhecidas pelo modelo
    O resultado da predição terá a probabilidade para o documento pertencer a cada classe e também a maior probabilidade
    e a classe associada a esta probabilidade.
    """

    def __init__(self, model, feature_extractor):
        self._model = model
        self._feature_extractor = feature_extractor

    def classify_file(self, file_path):
        """Realiza a classificação do documento dentro dos tipos conhecidos

        Args:
            file_path (str): Caminho até o arquivo para ser classificado

        Returns:
            list: resultado da classificação
        """
        features = self._feature_extractor.transform(file_path)

        return self.classify(features)

    def classify(self, X):
        """Classifica as features extraídas em tipos de documentos

        Args:
            X (array): Features extraídas

        Returns:
            list: lista contendo os dados da predição para o documento analisado
        """
        predicted = self._model.predict(X)
        predictions = self._model.decode_predictions(predicted)

        # classificação de um único documento
        return predictions[0]