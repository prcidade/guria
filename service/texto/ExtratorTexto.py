""" Serviço de extração de texto de arquivos enviados.
Encapsula a ferramenta de extração e as configurações passadas para ela.
"""
from text.process import text_processor
from text.feature_extraction.text_extractor import TextExtractor, ReadMode, ExtractionType
from service.io.hash.hash_generator import HashFile

class ExtratorTexto:

    def __init__(self, cache_driver=None):
        self.extrator = TextExtractor(max_pages=None)
        self.cache_driver = cache_driver

    def extract_text(self, file_path, **kwargs):
        """Extrai o texto de um documento informado e realiza o processamento do texto.
        O método aceita os seguintes parâmetros nomeados: 
        clear_text: para limpeza do texto
        stemming: para realizar o stemming do texto
        slipt_sentences: para separar o texto em sentenças
        read_mode: aceita o valor ocr ou scrape para definir o modo de extração do texto de arquivos PDF
        extraction: aceita o valor text-and-page e o valor text-only. Se informado text-only todo o texto será retornado como pertencente a página zero.
        ignore_cache: Se verdadeiro, ignorará o cache da extração de testo, mesmo se o driver de cache estiver ativo

        Args:
            file_path (str): caminho para o arquivo a ser processado.

        Returns:
            list: lista contendo dicionários com o texto e a página.
        """
        
        self.clear_text = kwargs.get('clear_text', False)
        self.stemming = kwargs.get('stemming', False)
        self.split_sentences = kwargs.get('split_sentences', False)

        read_mode = ReadMode.OCR_MODE
        if kwargs.get('read_mode') == 'scrape':
            read_mode = ReadMode.SCRAPE_MODE

        self.extrator.read_mode = read_mode

        extraction_type = ExtractionType.EXTRACT_TYPE_TEXT_ONLY
        if kwargs.get('extraction') == 'text-and-page':
            extraction_type = ExtractionType.EXTRACT_TYPE_TEXT_AND_PAGE

        self.extrator.extraction_type = extraction_type
        
        # Verifica se o cache está ativo
        if self.cache_driver is not None and not kwargs.get('ignore_cache', False):
            try:
                # Verifica se há valor definido no cache
                hash_file = HashFile()
                hash_value = hash_file.hash(file_path)
                content = self.cache_driver.remember(hash_value, self.extrator.transform(file_path))
            
            except Exception as e:
                # Em caso de erro do cache processa novamente o arquivo
                content = self.extrator.transform(file_path)

        else:
            # Processa o arquivo sem passar pelo cache
            content = self.extrator.transform(file_path)

        # Quando o extrator capturar apenas texto formata a saída para ter sempre o mesmo formato
        if type(content) == str:
            return [{'text': self.process_text(content), 'page': 0}]

        output = []
        for c in content:
            text = self.process_text(c['text'])

            output.append({'text' : text, 'page': c['page']})

        return output


    def process_text(self, text):
        """Processa o texto extraído com as preferências escolhidas pelo usuário.
        O texto poderá ser separado em sentenças, limpo (remoção de stopwords e pontuação e convertido para minúsculas), e passar por stemming.
        Se separado em sentenças, o processo de limpeza e stemming será aplicado a cada sentença.
        Se o texto não for separado em sentenças, todo o texto será considerado uma sentença única.

        Args:
            text (str): texto para ser processado

        Returns:
            list: sentenças processadas.
        """
        
        sentences = [ text ]
        if self.convert_input_value_to_bool(self.split_sentences):
            sentences = text_processor.split_sentences(text)

        for i in range(len(sentences)):
            text = sentences[i]
            if self.convert_input_value_to_bool(self.clear_text):
                text = text_processor.clear_words(text)
            
            if self.convert_input_value_to_bool(self.stemming):
                text = text_processor.stem_sentence(text)

            sentences[i] = text

        return sentences

    def convert_input_value_to_bool(self, value):
        """Converte o input do HTML para um valor booleano.
        Aceita o valor booleano em formato texto ou com os valores 1 ou 0 em sua presentação de string.

        Args:
            value (str): Representação textual de true e false; ou 1 ou 0

        Returns:
            bool: Representação booleana do valor informado.
        """
        if type(value) == bool:
            return value

        value = value.lower().strip()
        
        if value == '':
            return False

        if value == 'false':
            return False
        elif value == 'true':
            return True

        return bool(int(value))