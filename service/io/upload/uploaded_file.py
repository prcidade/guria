from werkzeug.utils import secure_filename 
import os
import tempfile
import shutil
  
def store_file_tmp_dir(file):
    """Armazena o arquivo enviado pelo usuário em um diretório temporário

    Args:
        file (File): arquivo enviado pelo usuário

    Returns:
        tuple: diretório temporário e diratório para o arquivo
    """
    dirpath = tempfile.mkdtemp()
    return dirpath, store_file_in_folder(file, dirpath)

def store_file_in_folder(file, folder):
    """Armazena o arquivo enviado pelo usuário no diretório informado

    Args:
        file (File): arquivo enviado pelo usuário
        folder (str): caminho para a pasta que o arquivo deve ser armazenado

    Returns:
        str: Caminho para o arquivo em disco
    """
    filename = secure_filename(file.filename)
    file_path = os.path.join(folder, filename)

    file.save(file_path)

    return file_path


class UploadedFile:

    def __init__(self, file):
        self._file = file
        self._file_path = None 
        self._file_folder = None
        self._is_tmp_file = False
        
    def save(self, dir_path):
        """Salva o arqiuvo em um diretório informadop

        Args:
            dir_path (string): caminho para o diretório para salvar o arquivo

        Returns:
            string: caminho até o arquivo
        """

        # Se tiver salvo como arquivo temporário excluí
        if self.is_tmp_file():
            self.delete()
        
        self._file_path = store_file_in_folder(self._file, dir_path)
        self._file_folder = dir_path
        self._is_tmp_file = False
        
        return self._file_path

    def save_temporary(self):
        """Salva um arquivo em disco em um diretório temporário

        Returns:
            tuple: diretório salvo e caminho do arquivo salvo
        """
        if self.is_tmp_file():
            self.delete()
        
        self._file_folder, self._file_path = store_file_tmp_dir(self._file)
        self._is_tmp_file = True

        return self._file_folder, self._file_path

    def delete(self):
        """Se o arquivo foi armazenado em disco realiza uma exclusão
        Se for um arquivo temporário o diretório será apagado junto
        """
        
        # remove o arquivo do disco
        if self._file_path is not None and os.path.exists(self._file_path):
            os.remove(self._file_path)

        # remove o diretório temporário
        if self._is_tmp_file and self._file_folder is not None and os.path.exists(self._file_folder):
            shutil.rmtree(self._file_folder)

        self._file_path = None
        self._file_folder = None

    def is_extension_allowed(self, allowed_extensions=None):
        """Verifica se a extensão do arquivo é permitida para envio.
        Por padrão todas as extensões são aceitas

        Args:
            allowed_extensions (array, optional): Lista de extensões permitidas em formato string. Defaults to None.

        Returns:
            bool: verdadeiro se a extensão for permitida
        """
        if allowed_extensions is None:
            return True

        return '.' in self._file.filename and \
           self._file.filename.rsplit('.', 1)[1].lower() in allowed_extensions


    def is_tmp_file(self):
        return self._is_tmp_file

    def get_file_path(self):
        return self._file_path

    def get_file_folder(self):
        return self._file_folder


