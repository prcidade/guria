import hashlib
import os

HASH_FILE_BUFFER_SIZE = 65536  # 64kb chunks

class HashFile:

    def __init__(self, algorithm : str ='sha1') -> None:
        self.algorithm = algorithm

    def hash(self, file):

        if type(file) == str and os.path.isfile(file):
            with open(file, 'rb') as f:
                return self.hash(f)

        hash_function = hashlib.new(self.algorithm)
        file.seek(0)
        while True:
            # Reading is buffered, so we can read smaller chunks.
            chunk = file.read(HASH_FILE_BUFFER_SIZE)
            if not chunk:
                break
            hash_function.update(chunk)

        return hash_function.hexdigest()
        