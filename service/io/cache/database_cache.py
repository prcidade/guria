
import click
from flask import current_app, g
from datetime import datetime, timedelta
import pickle

# Número de segundos em um mês
DEFAULT_CACHE_TTL = 30 * 24 * 60 * 60

class Cache:

    def __init__(self, db, cache_table='cache') -> None:
        self.db = db
        self.cache_table = cache_table

    def put(self, key : str, value, ttl : int = None):
        """Armazena um valor no cache do banco de dados

        Args:
            key (str): Chave para recuperar o valor posteriormente;
            value (object): Valor para ser armazenado.
            ttl (int, optional): tempo, em segundos, para considerar o cache válido. Defaults 1 mês.
        """
        
        if ttl is None:
            ttl = DEFAULT_CACHE_TTL

        expires_at = datetime.now() + timedelta(seconds=ttl)
        if self.has(key):
            self.db[self.cache_table].update({'key': key, 'value': pickle.dumps(value), 'expires_at': expires_at}, ['key'])
        else:
            self.db[self.cache_table].insert({'key': key, 'value': pickle.dumps(value), 'expires_at': expires_at, 'created_at' : datetime.now()})


    def get(self, key, default=None):
        """Recupera um valor do cache. Se não encontrado o valor definido no parâmetro 'default' será retornado.

        Args:
            key (str): chave para o valor em cache
            default (mixed, optional): Valor padrão para ser retornado, se for uma função será invocada com o parâmetro 'key'. Defaults to None.

        Returns:
            mixed: Valor armazenado em cache
        """

        entry = self.db[self.cache_table].find_one(key=key)
        
        if entry is None or entry['expires_at'] < datetime.now():
            if callable(default):
                return default(key)
            
            return default

        return pickle.loads(entry['value'])


    def has(self, key : str):
        """Verifica se uma entrada existe no cache.

        Args:
            key (str): Chave para o cache armazenado

        Returns:
            bool: Verdadeiro se encontrar no cache
        """
        entry = self.get(key)

        return entry is not None
        

    def remember(self, key : str, default,  ttl : int = None):
        """Recupera um valor em cache, se houver, caso contrário armazena o valor definido no parâmetro 'default' no
        cache pelo tempo definido em 'ttl'. Caso o parâmetro 'default' seja uma função, a mesma será invocada com o 
        parâmetro 'key' e seu retorno armazenado em cache e retornado. 

        Args:
            key (str): Chave do cache
            default (mixed): valor padrão para ser retornado, ou armazenado no cache caso não exista
            ttl (int): Tempo de validade do cache, em segundos

        Returns:
            object: Valor armazenado em cache, ou default.
        """
        entry = self.get(key)
        if entry is not None:
            return entry

        value = default
        if callable(default):
            value = default(key)
        
        if ttl is None:
            ttl = DEFAULT_CACHE_TTL

        self.put(key, value, ttl)
        return value


    def flush(self):
        """Limpa todas as entradas do cache
        """
        self.db[self.cache_table].delete()


    def forget(self, key):
        """Exclui uma entrada específica do cache.

        Args:
            key (str): Chave a ser excluída do cache
        """
        self.db[self.cache_table].delete(key=key)
