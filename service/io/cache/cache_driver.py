import click
from flask import current_app, g
from flask.cli import with_appcontext
from service.web.db import get_db
from service.io.cache.database_cache import Cache

def get_cache_driver():
    """Recupera o driver de cache para ser utilizado pela aplicação.
    Se o driver não estiver definido cria uma nova instancia do driver.

    Returns:
        object: Driver de cache para ser utilizado pela aplicação.
    """
    if 'cache_driver' not in g:
        g.cache_driver = resolve_cache_driver()

    return g.cache_driver

def get_current_cache_driver():
    """Recupera a instancia do driver de cache instanciado para a aplicação.

    Returns:
        object: Cache driver instanciado
    """
    return g.pop('cache_driver', None)

def resolve_cache_driver(cache_driver_name : str = 'database'):
    """Resolve o driver de cache a ser utilizado pela aplicação.
    O driver pode ser definido nas configurações do flask.
    Se o driver for desconhecido será retornado None.

    Args:
        cache_driver_name (str, optional): Nome do driver de cache a ser instanciado. Defaults to 'database'.

    Returns:
        object: Instância do driver de cache utilizado
    """
    if cache_driver_name is None:
        cache_driver_name = current_app.config['cache_driver_name']

    if cache_driver_name == 'database':
        return Cache(get_db())

    return None

@click.command('cache:flush')
@with_appcontext
def flush_cache_command():
    cache_driver = get_cache_driver()
    try:
        click.echo('Limpando cache')
        cache_driver.flush()
        click.echo('Cache limpo')
    except Exception as e:
        click.echo('Um erro inesperado ocorreu: {}'.format(e))
        

def init_app(app):
    app.cli.add_command(flush_cache_command)