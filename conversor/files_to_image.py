"""Conversor de PDF para imagens

Depende da biblioteca pdf2image

"""
from pdf2image import convert_from_path, convert_from_bytes
import os
import glob
import PIL.Image
PIL.Image.MAX_IMAGE_PIXELS = round(PIL.Image.MAX_IMAGE_PIXELS * 2.5)

def convert_from_buffer(buffer, extension='JPEG', output_page_name='page', dpi=100):
    """  Converte um buffer de arquivo PDF para uma lista de imagens
    Cada página será convertida para uma imagem com a extensão informada
    O buffer será retornado para memória

    Args:
        buffer (bytes): buffer do arquivo PDF
        extension (str, optional): Extensão dos arquivos de saída (JPG ou JPEG). Defaults to 'JPEG'.
        output_page_name (str, optional): Nome do arquivo das imagens para as páginas convertidas. Defaults to "page".1
    """
    pages = convert_from_bytes(buffer, dpi=dpi, fmt=extension, output_file=output_page_name)

    return pages
    

def convert_pdf(file_path, output_path='./', extension='JPEG', output_page_name='page', dpi=100, last_page=5, **kwargs):
    """Converte um arquivo PDF para imagem. 
    Cada página será convertida em uma imagem diferente utilizando o output_page_name seguido de um número

    Args:
        file_path (str): caminho para o PDF a ser convertido
        output_path (str, optional): Caminho de saída das páginas convertidas. Defaults to './'.
        extension (str, optional): Extensão dos arquivos de saída (JPG ou JPEG). Defaults to 'JPEG'.
        output_page_name (str, optional): Nome do arquivo das imagens para as páginas convertidas. Defaults to "page".1

    Returns:
        list: Lista de arquivos com as imagens convertidas
    """
    pages = convert_from_path(file_path, dpi=dpi, output_folder=output_path, fmt=extension, output_file=output_page_name, last_page=last_page, **kwargs)

    return pages



def convert_dir(input_dir, output_path='./', extension='JPEG', dpi=100):
    """Converte os arquivos PDF de um diretório para imagem
        O nome base dos arquivos de saída será o nome do arquivo original em PDF

    Args:
        input_dir (str): diretório de entrada dos PDF
        output_path (str, optional): Diretório de saída das imagens. Defaults to './'.
        extension (str, optional): Extensão dos arquivos de saída. Defaults to 'JPEG'.

    Returns:
        dict: Dicionário de arquivos de saída com a chave sendo o nome do arquivo e os valores os arquivos convertidos
    """
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    entries = os.listdir(input_dir)
    converted_files = []
    for entry in entries:
        full_path = os.path.join(input_dir, entry)
        if os.path.isfile(full_path):
            #ignora arquivos que não são PDF
            if not full_path.endswith('.pdf'):
                continue
            
            file_name = os.path.splitext(entry)[0]
            converted_file = convert_pdf(full_path, 
                                            output_path=output_path, 
                                            extension=extension, 
                                            output_page_name=file_name,
                                            dpi=dpi)
            
            converted_files.append(converted_files)
        else:
            # Entra no diretório para converter imagem
            folder = os.path.basename(os.path.normpath(full_path))
            converted_files += convert_dir(full_path, output_path=os.path.join(output_path, folder), extension=extension, dpi=dpi)

    return converted_files
