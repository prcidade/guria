import os
import tempfile
from text.process.pdf_document import PDFDocument
from text.process.libreoffice import LibreOffice


def convert_doc_to_pdfdocument_object(file_path : str): 
    """Converte um arquivo .doc em um objeto PDF com as páginas na memória.

    Args:
        file_path (str): caminho para o arquivo .doc
    Returns:
        PDFDocument: Representação do documento convertido para PDF em um objeto
    """
    
    fd, path = tempfile.mkstemp(suffix=".pdf")
    try:
        convert_doc(file_path, path)
        pdf_document = PDFDocument(path, load_pages=True)
        return pdf_document
    finally:
        os.remove(path)

def convert_doc(file_path : str, output_file : str):
    """Converte um arquivo .doc em um PDF, salvando no arquivo especificado

    Args:
        file_path (str): caminho para o arquivo .doc de entrada
        output_file (str): caminho para o arquivo .pdf de saída
    Raises:
        ValueError: É esperado que o caminho de saída seja um arquivo com extensão .pdf

    Returns:
        str: Caminho para o arquivo .pdf gerado. Mesmo valor do argumento output_file.
    """

    if not output_file.endswith('.pdf'):
        raise ValueError('O arquivo de saída deve conter a extensão .pdf')

    libreoffice = LibreOffice()
    return libreoffice.convert_doc_to_pdf_file(file_path, output_file)