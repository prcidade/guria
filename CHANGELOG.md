# Changelog

As maiores mudanças do projeto serão notificadas neste arquivo.

## [0.4.1]
### Adicionado
- Função para resolver o driver de cache utilizado pela aplicação
- Comando para limpar o cache

### Alterado
- A URL de demonstração e o endpoint de extração de texto utilizam cache

## [0.4.0]
### Adicionado
- Computação de um valor hash para arquivos por meio da classe `HashFile`
- Cache no formato chave-valor utilizando a base de dados como driver

### Alterado
- A classe de serviço `ExtratorTexto` foi alterada para utilizar cache dos textos extraídos dos documentos

## [0.3.0]
### Adicionado
- Suporte para extração de texto de documentos `.tif`(através do tesseract)
- Suporte para extração de texto de documento `.doc` (através do libreoffice).
- Classe para a extração de texto e número da página de documentos `.doc` (classe `DocDocument`)
- Suporte para extração de texto de documento `.docx` (através do python-docx).
- Classe para manipular arquivos do Word em `.docx` (classe `DocxDocument`).
- Classe para invocar o antiword através do código (classe `AntiWord`).
- Classe para invocar o libreoffice (headless) através do código (classe `LibreOffice`)
- Classe para recuperar o texto e número da página de arquivos do word, para `.doc` e para `.docx` (classe `WordDocument`).
- Conversão de arquivos em `.doc` para `.pdf` (através do `libreoffice`).
- Opção de escolha no modo de extração do texto de PDF, se por OCR ou por scrape (`pdfminer`).
- Classe para manipular a extração de texto e número da página de arquivos em PDF (classe `PDFDocument` e `PDFPage`).
- Suporte para extração de texto de PDF sem a necessidade de OCR (através do `pdfminer`).
- Método `extract_text_from_pdf_with_ocr` da classe `TextExtractor` para extrair texto de arquivos PDF com o modo OCR.
- Método `extract_text_from_pdf_scrape` da classe `TextExtractor` para permitir extrair o texto com o `pdfminer` quando possível.
- Método `extract_text_from_image` da classe `TextExtractor` para extrair o texto de uma única imagem.
- Método `extract_text_from_images` da classe `TextExtractor` passou a retornar uma lista de dicionários, contento o número da página e o texto extraído.
- Método `extract_text_from_word_document` da classe `TextExtractor` para encapsular a extração de texto de documentos do Word.
- Classe de serviço para processar requisições de extração e processamento de texto (`ExtratorTexto`). 
- API para extração de texto com opções de processamento de texto.
- Demo de extração de texto de documentos.

### Alterado
- Classe `TextExtractor` permite escolher o modo de extração de arquivos em PDF (OCR ou scrape) e se será retornado apenas texto ou texto e número da página.
- Alterado o método `extract_text_from_file` da classe `TextExtractor` para permitir retornar o texto e o número da página.

### Removido
- Função de verificação de extensão do módulo referente a página de demonstração.

## [0.2.0] - 2021-02-04
### Alterado
- Marcação RC removida para tag definitiva

## [0.2.0-rc] - 2021-01-14
### Adicionado
- Arquivo para registro de mudranças (este arquivo).
- Classe para extração do TF-IDF dos arquivos enviados.
- Classe para extração do texto.
- Classificação de documentos da parte técnica do Processo Licitatório.
- Aumentado para 27 documentos reconhecidos pela RNA.
- Correção na rotação da imagem antes da extração do texto.
- Injeção de dependência de um extrator de características ao classificador de documentos. O extrator de características deve implementar o método `transform`, que recebe o caminho para o documento e retorna as características extraídas.

### Mudanças
- Aumento do número de neurônios da RNA (de 8 para 16).
- Aumento no DPI para conversão de PDF para imagem
- Funções substituídas por classes
- Alteração no otimizador utilizado para treinar a rede (de Adam para SGD).
- Alteração no script de treinamento para salvar em uma pasta diária os resultados do treinamento e salvar o estado da classe pronta para realizar o TF-IDF.

### Removido
- Funções de TF-IDF que foram substituídas por classes.
- Funções de processamento de imagens não utilizadas.

## [0.1.0] - 2020-10-19
### Adicionado
- Página de apresentação Web.
- Texto extraído de imagens.
- Arquivos PDF convertidos em imagens.
- Arquivoz zip descompactados para extração do texto.
- Remoção de stopwords do texto.
- TF-IDF do texto extraído.
- Classificação dos documentos, de Operação de Crédito e documentação jurídica do Processo Licitatório, em 23 tipos distintos.
- Uso de uma RNA para classificação, com 8 neurônios.
- API Web.
- Cadastro de cliente da API Web via linha de comando.
- Autenticação da API Web através de token no cabeçalho das requisições.
- Testes automatizados.
- Script para download dos arquivos do Portal dos Municípios.
- Script para extração dos arquivos baixados.
- Script para conversão dos arquivos para imagens.
- Script para treinamento do modelo a ser utilizado.