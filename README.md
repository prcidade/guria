# Guria

Uma Rede Neural Artificial (RNA) para classificação de documentação técnica do PARANACIDADE.

Os municípios encaminham documentos para o PARANACIDADE através do Portal dos Municípios, a entrega desses documentos é feita em duas etapas, a primeira chamada *"Deferimento"* que caracteriza a entrega de documentos ao PARANACIDADE e a segunda chamada de *"Análise"* que é a análise qualitativa da documentação encaminhada. Em tese a fase qualitativa só é realizada após a conclusão da fase quantitativa, assim os técnicos empenham seu tempo de análise apenas quando há a certeza de que a documentação mínima foi encaminhada.

Durante a fase de *Deferimento* é apresentada ao município uma listagem com todos os documentos que devem ser apresentados ao PARANACIDADE para que a análise possa ser realizada. Nessa primeira etapa os técnicos fazem uma validação do documento (aprovado/recusado) indicando que a tipologia do documento é condizente com o esperado (o que o município diz ser um edital é de fato um edital, uma matrícula do imóvel é realmente uma matrícula , etc), a principio, sem verificar se as informações estão corretas (como a matrícula do imovel condizer com o lote em questão, por exemplo). Durante essa fase o PARANACIDADE também pode dispensar o município de apresentar determinados documentos conforme o caso.

O intuito inicial desse repositório é fazer a classificação dos documentos encaminhados pelos municípios e verificar se são condizentes com o esperado pelo PARANACIDADE, facilitando a fase de *Deferimento*.


## Extração de Textos

Para verificar os documentos, o conteúdo é extraído em formato texto e passa por uma limpeza. Para isso todos os documentos em PDF são convertidos para imagens (visto que há pdf que são gerados pela digitalização de documentos), e então o texto é extraído através de um OCR (tesseract/pytesseract). Com o texto extraído é feita uma remoção de *stopwords* e por fim o texto é transformado com o TF-IDF.

Além do modo OCR a GurIA fornece meios de obter o texto sem a necessidade de conversão para imagens, esse modo é chamado de *scrape* no código. Quando utilizado desse modo, serão empregadas diferentes bibliotécas e programas para obter o texto contido dentro dos documentos. Por exemplo, um PDF pode ser extraído com o `pdfminer` quando for identificado que há texto na página e com o modo OCR quando a página for identificada como uma imagem, documentos `.docx` serão extraídos com a biblioteca `python-docx` enquanto documentos `.doc` serão convertidos com o `libreoffice` para PDF para só então ter o texto extraído.

## Modelo

O modelo foi construído utiliuzando a biblioteca Keras (2.2) e o Tensorflow (2.1), sendo construída uma RNA de uma única camada com apenas 16 neurônios (ReLu), dropout e batch normalization e uma camada de saída com uma função softmax, sendo a saída a classe do documento codificada com *OneHotEncode*. Para treinamento foi utilizado um batchsize de 32 amostras, com aproximandamente 6000 exemplos, divididas em 80/20, com o melhor modelo escolhido através do conjunto de teste. Após a escolha do melhor modelo utilizou-se o dataset completo para treinamento e os pesos foram salvos.

## Dependências

O projeto foi desenvolvido utilizando ```Python==3.7```. Para conversão de PDF para imagem é utilizada a biblioteca ```pdf2image```, que por sua vez depende da biblioteca ```poppler```. Esta última deve ser instalada no sistema que hospedará a aplicação. A biblioteca ```pytesseract``` também é um encapsulamento da biblioteca de sistema ```tesseract``` que deve estar instalada no sistema. Também são utilizadas as blibliotecas ```opencv```, ```scikit-learn```, ```keras==2.2```, ```tensorflow=2.1```, ```nltk```, ```pytesseract```, ```pdfminer.six``` e ```python-docx```.

Há uma página web de aplicação para ser exibida para os usuários como apresentação, sendo desenvolvida em Flask, e também um serviço web para ser utilizado por outras aplicações. Esse serviço utiliza o ```sqlite``` (através da biblioteca ```dataset```) para armazenar os clientes autorizados e também é necessário realizar uma autenticação antes de utilizar o serviço.

A lista completa de dependências está listada no arquivo ```requirements.txt``` e pode ser instalada através do comando ```pip install -r requirements.txt```. É recomandado a criação de um ambiente virtual para o desenvolvimento e deploy.

Além dessas é necessário instalar o tesseract no sistema:
```
sudo apt install tesseract-ocr
sudo apt install tesseract-ocr-por
sudo apt install libtesseract-dev
```

Como mencionado, a biblioteca ```pdf2image``` depende da biblioteca ```poppler```, as distribuições Linux podem instalar essa biblioteca através do pacote ```poppler-utils```, ou alternativamente com o comando ```conda```:

```
sudo apt install poppler-utils

# ou 

conda install -c conda-forge poppler
```

Para extrair texto de documentos ```.doc``` é utilizado o programa ```antiword``` e o ```libreoffice``` para converter o arquivo para PDF e então ter seu texto extraído com as bibliotecas voltadas a PDF. Para instalar o ```antiword``` e o ```libreoffice``` utilize o comando:
```
sudo apt install antiword

sudo apt-get install libreoffice --no-install-recommends --no-install-suggests
```

Também são necessários alguns pacotes do nltk, sendo eles:

- averaged_perceptron_tagger
- floresta
- mac_morpho
- machado
- punkt
- stopwords
- wordnet
- words

Os pacotes podem ser instalados através do modo interativo do python:
```
python      # inicia o modo interativo
import nltk
nltk.download()
```

## Testes

Os testes são realizados através da biblioteca ```pytest```. Os scripts de testes estão localizados no diretório ```tests``` e podem ser executados através do comando.

```
pytest
```

Para verificar a porcentagem de cobertura dos testes pode-se utilizar os seguintes comandos:
```
coverage run -m pytest
coverage report
```

## Desenvolvimento

Para realizar o desenvolvimento dessa ferramenta pode-se clonar este repositório e instalar suas dependências. Na pasta raiz deste repositório execute os comandos:
```
export FLASK_APP=wsgi
export FLASK_ENV=development
flask run --host=0.0.0.0
```

Isso inicializará um servidor de desenvolvimento do Flask com o debug ativo. O parâmetro ```--host=0.0.0.0``` permite que outras máquinas da rede se conecte no servidor, esse parâmetro pode ser omitido para permitir apenas a máquina local se conectar no serviço. **Atenção**: não rodar estes comandos no ambiente de produção.

## Deploy

Esse repositório pode ser utilizado como um serviço web para ser utilizado por outras aplicações, para isso é necessário fazer o deploy em um servidor WSGI. Considere criar um ambiente virutal para realizar o deploy. Para isso pode se utilizar os seguintes comandos:
```
sudo apt update
sudo apt install python3-pip python3-venv
python3.7 -m venv venv
python3 -m pip install -U pip
source venv/bin/activate
pip install -r requirements.txt
```

O deploy pode ser feito em um servidor **gunicorn** e então ser feito um proxy com o servidor Apache. Também é necessário criar um serviço para ser iniciado junto do sistema caso haja falhas, para isso pode-se criar um arquivo no diretório ```/etc/systemd/system/guria.service``` com o seguinte texto (alterando-se os diretórios):
```
[Unit]
Description=guria
After=network.target

[Service]
User=www-data
Restart=on-failure
WorkingDirectory=/var/www/wsgi/guria
ExecStart=/var/www/wsgi/guria/venv/bin/python /var/www/wsgi/guria/venv/bin/gunicorn -c /var/www/wsgi/guria/gunicorn.conf.py -b 0.0.0.0:3000 wsgi:app

[Install]
WantedBy=multi-user.target
```

O arquivo acima utiliza um ambiente virutal criado na pasta do projeto e assume que o projeto está na pasta ```/var/www/wsgi/guria```. O comando ```ExecStart=/var/www/wsgi/guria/venv/bin/python``` é utilizado para iniciar o ambiente virtual para o serviço. Uma vez criado o arquivo, pode-se ativar o serviço através do comando: 

```systemctl daemon-reload``` 

Após isso rode o comando para habilitar o serviço ao iniciar o sistema:

```systemctl enable guria```

Os comandos ```systemctl start guria``` e ```systemctl stop guria``` também podem ser utilizados.

Os arquivos de configuração não estão versionados, mas há um arquivo de exemplo ```gunicorn.conf.py.example``` que pode ser utilizado como base para criação de um arquivo apropriado. 

É importante manter em mente que o processo é relativamente pesado, então considere alterar o timeout do servidor. Pode-se aumentar o tempo de timeout através do arquivo de confirguração, definindo, por exemplo:
```
timeout = 120
```

Ao utilizar este repositório como serviço web é criado um banco de dados local para salvar os clientes autorizados a utilizar o serviço. Por simplicidade está sendo utilizado o ```sqlite```, é criado um arquivo do banco de dados em ```sercice/instance```, essa pasta não é versionada. É necessário dar as devidas permissões para utilização desta pasta.

Na pasta ```sercice/instance``` também pode ser adicionado um arquivo ```config.py``` com configurações de ambiente, como a ```SECRET_KEY``` do serviço, para o serviço. Caso o serviço esteja sendo utilizado através de um Proxy Reverso o arquivo de configuração pode conter, se necessário, a definição da variável ```URL_PREFIX``` com a subpasta do Proxy Reverso. O arquivo deve ser um script python válido, podendo definir as constantes utilizadas durante a execução da aplicação.

**Lembre-se de definir um diretório para logs**. Considere ajustar as permissões para a pasta de logs definida no arquivo de configuração:
```
sudo find /var/www/wsgi/guria -type f -exec chmod 664 {} \;    
sudo find /var/www/wsgi/guria -type d -exec chmod 775 {} \;
sudo chgrp -R www-data logs service/instance
sudo chmod -R ug+rwx logs service/instance
```

Com isso o repositório poderá ser utilizado por outros serviços através da web.


## Utilização como Serviço

Após realizar o deploy é possível utilizar o classificador de documentos através da web, para isso é necessário que o cliente esteja cadastrado no banco de dados local. Para iniciar o banco de dados utilize o comando:

```
flask init-db
```

Isto criará um banco de dados local para ser utilizado pela aplicação. Após a criação é necessário cadastrar um novo cliente, utilize o seguinte comando:

```
flask create-client
```

Preencha os dados solicitados e cadastre um *username* e uma senha para o cliente. Essas credenciais serão necessárias para utilizar o serviço web.

Para utilizar o serviço web é necessário estar autenticado, para isso utilize envie, via ```POST``` o ID do cliente (campo ```client_id```) gerado pelo sistema ao cadastrar o cliente e a senha (campo ```secret```) cadastradas para a URL ```/authenticate```, se as credenciais forem válidas será retornado um token para utilização dos métodos do serviço. O token gerado deve ser enviado no cabeçalho das requisições seguinte no campo ```x-access-token```, se não for informado as requisições serão recusadas.

Após a autenticação no serviço pode-se fazer o envio de um documento em formato de imagem ou PDF ou Zip para a URL ```/documento/classificar```. O serviço retornará a probabilidade do documento pertencer a cada classe conhecida e também qual é a classe de maior probabilidade (juntamente com a probabilidade).

**Atenção:** O serviço é uma função relativamente pesada e pode sofrer lentidão e quedas, considere implementar um sistema de cache na aplicação cliente.





