import pytest

import io
import os
from image.ocr import image_ocr
import tempfile
from PIL import Image

def test_get_angle_text_orientation(mocker):
    """Testa a extração da orientação do ângulo do texto
    O ângulo é obtido através do parsing do retorno do pytesseract (mock)
    """

    img = Image.new('RGB', (360, 360))

    return_image_to_osd = '''Page number: 0
        Orientation in degrees: 270
        Rotate: 20
        Orientation confidence: 21.27
        Script: Latin
        Script confidence: 4.14
    '''
    mocker.patch('image.ocr.image_ocr.pytesseract.image_to_osd', return_value=return_image_to_osd)

    angle = image_ocr.get_angle_text_orientation(img)

    image_ocr.pytesseract.image_to_osd.assert_called_once()
    assert angle == 20.0


def test_get_angle_from_blank_metadata(mocker):
    """"Testa a extração da rotação de uma metadata inválida, sem a devida orientação informada
    Deverá ser lançada uma exceção
    """

    return_image_to_osd = ''
    mocked_image_to_osd = mocker.patch('image.ocr.image_ocr.pytesseract.image_to_osd', return_value=return_image_to_osd)
    with pytest.raises(Exception) as context:
        img = Image.new('RGB', (360, 360))

        angle = image_ocr.get_angle_text_orientation(img)

    mocked_image_to_osd.assert_called_once()


def test_ignore_error_on_getting_angle_from_blank_metadata(mocker):
    """Testa a extração da rotação de uma metadata inválida, sem a devida orientação informada
    A exceção deverá ser omitida, retornando como valor zero (sem rotação)
    """

    img = Image.new('RGB', (360, 360))

    return_image_to_osd = ''
    mocker.patch('image.ocr.image_ocr.pytesseract.image_to_osd', return_value=return_image_to_osd)

    angle = image_ocr.get_angle_text_orientation(img, ignore_errors=True)

    image_ocr.pytesseract.image_to_osd.assert_called_once()
    assert angle == 0.0



def test_get_text_from_image(mocker):
    """Testa a extração do texto de uma imagem
    A extração é feita através da biblioteca pytesseract que está em um mock neste teste
    """
    
    img = Image.new('RGB', (360, 360))
    mocker.patch('image.ocr.image_ocr.pytesseract.image_to_string')
    
    text = image_ocr.get_text_from_image(img)

    image_ocr.pytesseract.image_to_string.assert_called_once()

