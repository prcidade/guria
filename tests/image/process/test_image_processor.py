import pytest

from image.process import image_processor
import tempfile
import os
import numpy as np
from PIL import Image

def test_load_image():
    """Testa a abertura de uma imagem em disco para a memória
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        img = Image.new('RGB', (360, 360))
        image_path = os.path.join(tmp_dir, 'img_test.jpg')
        img.save(image_path, "JPEG")

        image = image_processor.load_img(image_path)

    assert type(image) is np.ndarray


def test_load_image_not_found():
    """Testa a abertura de uma imagem inexistente
    Uma exceção deverá ser lançada
    """

    with pytest.raises(FileNotFoundError):
        img = image_processor.load_img('not_found.jpg')

def test_rotate_image():
    """Verifica a rotação da imagem
    """
    img = np.zeros((5,5))
    # preenche uma coluna
    img[:, 2] = 1

    image_rotated = image_processor.rotate_bound(img, 90.0)
    
    # verifica se há valores diferentes de zero na segunda linha
    assert image_rotated[2].sum() > 0

def test_show_image(mocker):
    """Verifica a exibição de imagem. 
    Testa as chamadas de função ao opencv
    Deverá ser chamada a função de exibição da imagem e a função para aguardar a interação do usuário
    Args:
        mocker (Mocker): mocker de chamadas de função
    """
    
    # Mock da função do OpenCV
    def mocked_show(title, img_temp):
        return title

    # Mock da função do OpenCV
    def mocked_waitKey(value=0):
        return value

    with tempfile.TemporaryDirectory() as tmp_dir:
        img = Image.new('RGB', (3, 3))
        image_path = os.path.join(tmp_dir, 'img_test.jpg')
        img.save(image_path, "JPEG")

        image = image_processor.load_img(image_path)
        imshow = mocker.patch('cv2.imshow', side_effect=mocked_show)
        waitkey = mocker.patch('cv2.waitKey', side_effect=mocked_waitKey)

        image_processor.show_img(image, size=(3,3), title="Teste")

        assert imshow.called == True
        assert waitkey.called == True 


def test_show_image_without_wait(mocker):
    """Verifica a exibição de imagem sem a necessidade de aguardar a interação do usuário para fechar a janela. 
    Testa as chamadas de função ao opencv
    Deverá ser chamada a função de exibição da imagem
    A interação do usuário não é necessária para fechar a janela

    Args:
        mocker (Mocker): mocker de chamadas de função
    """
    
    # Mock da função do OpenCV
    def mocked_show(title, img_temp):
        return title

    # Mock da função do OpenCV
    def mocked_waitKey(value=0):
        return value

    with tempfile.TemporaryDirectory() as tmp_dir:
        img = Image.new('RGB', (3, 3))
        image_path = os.path.join(tmp_dir, 'img_test.jpg')
        img.save(image_path, "JPEG")

        image = image_processor.load_img(image_path)
        imshow = mocker.patch('cv2.imshow', side_effect=mocked_show)
        waitkey = mocker.patch('cv2.waitKey', side_effect=mocked_waitKey)

        image_processor.show_img(image, size=(3,3), title="Teste", wait_key=False)

        assert imshow.called == True
        # não necessita da interação com o usuário para fechar a janela
        assert waitkey.called == False 


def test_convert_to_grayscale(mocker):
    """Testa a conversão de uma imagem RGB para escala de cinza
    A imagem original deve ter 3 canais enquanto a imagem em escala de cinza apenas um
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        # salva uma imagem em disco para simular a abertura da mesma
        img = Image.new('RGB', (5, 5))
        image_path = os.path.join(tmp_dir, 'img_test.jpg')
        img.save(image_path, "JPEG")

        image = image_processor.load_img(image_path)
        gray = image_processor.convert_to_grayscale(image)

        assert image.shape == (5, 5, 3)
        assert gray.shape == (5, 5)


