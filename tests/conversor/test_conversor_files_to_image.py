import pytest

import io
import os
import pdf2image
import conversor
import tempfile


def test_convert_from_buffer(mocker):
    """Testa a conversão de um buffer em imagem
    A conversão é feita através da biblioteca pdf2image, que está com um mock 
    """
    buffer = io.BytesIO(b'Arquivo PDF')

    # mocker.patch('conversor.files_to_image.convert_from_bytes')
    mocker.patch('conversor.files_to_image.convert_from_bytes')

    conversor.files_to_image.convert_from_buffer(buffer.read())
    
    conversor.files_to_image.convert_from_bytes.assert_called_once()

def test_convert_pdf_from_disk(mocker):
    """Testa a conversão de um arquivo pdf em disco em uma imagem
        O arquivo gerado será escrito em disco
    """

    def mock_write_file(file_path='./', dpi=100, output_folder='./', fmt='JPEG', output_file='page', last_page=5):
        #simula a escrita do arquivo gerado em disco
        with open(os.path.join(output_folder, '{}.{}'.format(output_file, fmt)), 'w+') as output:
            output.write('') 

    mocker.patch('conversor.files_to_image.convert_from_path', side_effect=mock_write_file)
    with tempfile.TemporaryDirectory() as tmp_dir:
        buffer = io.BytesIO(b'Arquivo PDF')
        #escreve o buffer em disco para simular um arquivo
        temp_pdf = os.path.join(tmp_dir, 'arquivo.pdf')
        with open(temp_pdf, 'w+') as pdf_file:
            pdf_file.write( str(buffer.read()) )

        extension = 'JPEG'
        output_page_name = 'page'

        conversor.files_to_image.convert_pdf(temp_pdf, output_path=tmp_dir, extension=extension, output_page_name=output_page_name)
        conversor.files_to_image.convert_from_path.assert_called_once_with(temp_pdf, dpi=100, output_folder=tmp_dir, fmt=extension, output_file=output_page_name, last_page=5)
        assert os.path.exists(os.path.join(tmp_dir, '{}.{}'.format(output_page_name, extension))) == True


def test_convert_dir(mocker):
    """Testa a conversão de um diretório de PDF em imagens
    Deve ser gerado uma imagem para cada página do PDF. Todos os arquivos PDF do diretório devem ser convertidos
    """

    def mock_write_file(file_path='./', dpi=100, output_folder='./', fmt='JPEG', output_file='page', last_page=5):
        #simula a escrita do arquivo gerado em disco
        with open(os.path.join(output_folder, '{}.{}'.format(output_file, fmt)), 'w+') as output:
            output.write('') 

    mocked_convert_from_path = mocker.patch('conversor.files_to_image.convert_from_path', side_effect=mock_write_file)
    with tempfile.TemporaryDirectory() as tmp_dir:
        buffer = io.BytesIO(b'Arquivo PDF')
        #escreve o buffer em disco para simular um arquivo
        temp_pdf = os.path.join(tmp_dir, 'arquivo.pdf')
        with open(temp_pdf, 'w+') as pdf_file:
            pdf_file.write( str(buffer.read()) )
        
        #simula outro arquivo em PDF
        temp_pdf2 = os.path.join(tmp_dir, 'arquivo2.pdf')
        with open(temp_pdf2, 'w+') as pdf_file:
            pdf_file.write( str(buffer.read()) )

        #simula uma imagem
        temp_img = os.path.join(tmp_dir, 'my_image.jpeg')
        with open(temp_img, 'w+') as pdf_file:
            pdf_file.write( str(buffer.read()) )

        extension = 'JPEG'
        output_page_name = 'page'

        with tempfile.TemporaryDirectory() as tmp_output_dir:

            conversor.files_to_image.convert_dir(tmp_dir, output_path=tmp_output_dir, extension=extension)

            assert mocked_convert_from_path.call_count == 2 # verifica número de chamadas para o método
            assert len(os.listdir(tmp_output_dir)) == 2 # verifica se há o mesmo número de arquivos no diretório de saída

        
def test_convert_nested_dir(mocker):
    """Testa a conversão de diretórios e subdiretórios de PDF para imagens.
    Todos os arquivos, devem ser convertidos em imagens em disco
    """

    def mock_write_file(file_path='./', dpi=100, output_folder='./', fmt='JPEG', output_file='page', last_page=5):
        #simula a escrita do arquivo gerado em disco
        with open(os.path.join(output_folder, '{}.{}'.format(output_file, fmt)), 'w+') as output:
            output.write('') 

    mocked_convert_from_path = mocker.patch('conversor.files_to_image.convert_from_path', side_effect=mock_write_file)
    with tempfile.TemporaryDirectory() as tmp_dir:
        buffer = io.BytesIO(b'Arquivo PDF')
        #escreve o buffer em disco para simular um arquivo
        temp_pdf = os.path.join(tmp_dir, 'arquivo.pdf')
        with open(temp_pdf, 'w+') as pdf_file:
            pdf_file.write( str(buffer.read()) )
        
        #cria um diretório aninhado
        temp_dir_nested = os.path.join(tmp_dir, 'sub_dir')
        os.makedirs(temp_dir_nested)

        #simula outro arquivo em PDF
        temp_pdf2 = os.path.join(temp_dir_nested, 'arquivo2.pdf')
        with open(temp_pdf2, 'w+') as pdf_file:
            pdf_file.write( str(buffer.read()) )

        extension = 'JPEG'
        output_page_name = 'page'

        with tempfile.TemporaryDirectory() as tmp_output_dir:

            conversor.files_to_image.convert_dir(tmp_dir, output_path=tmp_output_dir, extension=extension)
            print(os.listdir(tmp_output_dir))
            print([ d for d in os.listdir(tmp_output_dir) if os.path.isdir(os.path.join(tmp_output_dir, d)) ])
            
            # verifica número de chamadas para o método
            assert mocked_convert_from_path.call_count == 2 
            # verifica se foi criado um sub-diretório no diretório de saída
            assert len([ d for d in os.listdir(tmp_output_dir) if os.path.isdir(os.path.join(tmp_output_dir, d)) ]) == 1 
            # verifica se há um arquivo no diretório de saída
            assert len([ f for f in os.listdir(tmp_output_dir) if os.path.isfile(os.path.join(tmp_output_dir, f)) ]) == 1
            # Verifica se um arquivo foi criado no subdiretório de saída
            assert len([ f for f in os.listdir(os.path.join(tmp_output_dir, 'sub_dir')) if os.path.isfile(os.path.join(tmp_output_dir, 'sub_dir', f)) ]) == 1
