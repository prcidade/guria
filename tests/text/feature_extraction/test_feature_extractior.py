import pytest

import io
import os

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

from text.feature_extraction.feature_extractor import TfidfFileExtractor
import tempfile

CORPUS = [
        'Olá Mundo',
        'Teste de Documentos',
        'Mundo de Teste'
    ]


def test_extract_tfidf():
    """Testa aplicar o TF-IDF em uma sequencia de termos
    Por padrão o extrator desconsidera tokens menores do que 3 caracteres
    Para o corpus do teste deve haver 3 sentenças e 4 únicas palavras mariores que 3 caracteres
    """

    extractor = TfidfFileExtractor()
    X = extractor.fit_transform(CORPUS)

    assert extractor.is_fitted() == True 
    assert X.shape == (3, 4)


def test_save_trained_extractor():
    """Testa salvar um extrator de TF-IDF em disco para ser utilizado posteriormente.
    A classe deverá ser armazenada em disco
    """
    with tempfile.TemporaryDirectory() as tmp_dir:
        extractor = TfidfFileExtractor()
        X = extractor.fit_transform(CORPUS)

        extractor.save(os.path.join(tmp_dir, 'extractor.pkl'))

        assert os.path.exists(os.path.join(tmp_dir, 'extractor.pkl')) == True


def test_load_extractor():
    """Testa carregar um extrator de TF-IDF já treinado
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        extractor = TfidfFileExtractor()
        X = extractor.fit_transform(CORPUS)

        extractor.save(os.path.join(tmp_dir, 'extractor.pkl'))

        # Nova instância para ser carregada
        extractor = TfidfFileExtractor()
        extractor.load(os.path.join(tmp_dir, 'extractor.pkl'))

        assert extractor.is_fitted() == True


def test_extract_tfidf_from_str():
    """Testa a transformação de uma string em tf-idf 
    Deve gerar a saída do tf-idf com base no corpus de treino
    resultando em uma sentença e 4 tokens (1, 4)
    """

    # Treina o extrator
    extractor = TfidfFileExtractor()
    extractor.fit(CORPUS)
    
    X = extractor.transform('Olá Mundo Teste')

    assert X.shape == (1, 4)


def test_extract_tfidf_from_file(mocker):
    """Simula a extração do TF-IDF de um arquivo em disco
    É criada uma imagem para simular o arquivo em disco 
    e um mock representando o retorno (parcial) da extração do texto desse arquivo.
    O TF-IDF deve ser aplicado sobre o texto extraído
    """

    with tempfile.TemporaryDirectory() as tmp_dir:

        # Cria um arquivo em disco para ter o texto extraido
        img = Image.new('RGB', (500, 500), color = (255, 255, 255))

        font = ImageFont.truetype("arial.ttf", 26)
        d = ImageDraw.Draw(img)
        d.text((10,10), "Olá Mundo", fill=(0,0,0), font=font)
        
        file_path = os.path.join(tmp_dir, 'img.png')
        img.save(file_path)
        

        # Treina o extrator
        extractor = TfidfFileExtractor()
        extractor.fit(CORPUS)
        
        # Simula retorno da extração do texto
        mocker.patch('text.feature_extraction.text_extractor.TextExtractor.transform', return_value='Mundo')
        
        X = extractor.transform(file_path)

        assert X.shape == (1, 4)
        

