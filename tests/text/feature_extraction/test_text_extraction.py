import pytest

import io
import os
from PIL import Image
import tempfile
import shutil
import random
import string
import zipfile

from text.feature_extraction.text_extractor import TextExtractor
from image.process import image_processor


def write_image(path):
    """Salva uma imagem "dummy" em disco para ser usada nos testes
    """
    img = Image.new('RGB', (500, 500), color = (255, 255, 255))

    if os.path.isdir(path):
        path = os.path.join(path, '{}.png'.format(generate_random_string()))
    
    img.save(path)

    return path

def write_fake_pdf(path):
    """Escreve um arquivo falso em disco para simular um PDF
    """
    buffer = io.BytesIO(b'Arquivo PDF')
    path = os.path.join(path, '{}.pdf'.format(generate_random_string()))

    with open(path, 'w+') as pdf_file:
        pdf_file.write( str(buffer.read()) )

    return path

def zip_dir(in_path, out_path):
    """Compacta um diretorio em um arquivo zip
    """
    zip_to_dir = out_path
    zip_to_dir = os.path.join(out_path, '{}'.format(generate_random_string()))

    zip_name = shutil.make_archive(zip_to_dir, 'zip', in_path)

    return os.path.join(out_path, zip_name)

def create_empty_zip(path):
    path = os.path.join(path, '{}.zip'.format(generate_random_string()))
    
    zf = zipfile.ZipFile(path, 'w')
    zfi = zipfile.ZipInfo('test1/')
    zf.writestr(zfi, '')
    zf.close()

    return path

def generate_random_string():
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(10))

def open_image(path):
    """Carrega uma imagem do disco para a memória
    """
    return image_processor.load_img(path)

def test_change_dpi_used():
    """Testa a alteração do DPI utilizado para converter PDF em Imagens antes da extração
    O atributo do objeto deve ser diferente do inicial
    """

    extractor = TextExtractor()
    extractor.set_image_dpi(500)

    assert extractor.image_dpi == 500

def test_change_max_pages_processed():
    """Testa a alteração do número máximo de páginas a serem processadas
    """
    extractor = TextExtractor()
    extractor.set_max_pages(42)

    assert extractor.max_pages == 42


def test_extract_text_from_single_image(mocker):
    """Testa a extração de texto de uma única imagem
    A Imagem deve ter sua rotação corrigida e então terá o texto extraído através do OCR
    """
    with tempfile.TemporaryDirectory() as tmp_dir:
        
        img_path = write_image(tmp_dir)
        img = open_image(img_path)

        # encapsula invocação dos métodos do OCR
        mocker.patch('image.ocr.image_ocr.get_angle_text_orientation', return_value=0.0)
        get_text_mocked = mocker.patch('image.ocr.image_ocr.get_text_from_image', return_value='Olá Mundo')
    
        extractor = TextExtractor()
        text = extractor.extract_text_from_image(img)

        assert 'Olá Mundo' in text
        assert get_text_mocked.called == True

def test_extract_text_from_multiples_images(mocker):
    """Testa a extração de texto de vários arquivos
    """

    num_files = 3
    extractor = TextExtractor()

    # encapsula invocação dos métodos do OCR
    mocker.patch('image.ocr.image_ocr.get_angle_text_orientation', return_value=0.0)
    get_text_mocked = mocker.patch('image.ocr.image_ocr.get_text_from_image', return_value='Olá Mundo')

    with tempfile.TemporaryDirectory() as tmp_dir:
        images = []
        for i in range(num_files):
            img_path = os.path.join(tmp_dir, '{}.png'.format(i))
            write_image(img_path)
            img = open_image(img_path)
            images.append(img)
        
        content = extractor.extract_text_from_images(images)

        # junta todo o texto extraído
        text = ''
        for c in content:
            text += c['text'] + '\n'

    assert ( 'Olá Mundo\n' * num_files ) == text
    assert 'page' in content[0]
    assert get_text_mocked.called == True


def test_limit_of_pages_of_text_extracted_from_images(mocker):
    """Testa o limite de páginas na extração de textos das imagens.
    O limite de páginas definido pela na classe deve ser respeitado.
    """

    num_files = 3 # total de páginas
    num_max_pages = 2 # limite de páginas

    extractor = TextExtractor()
    extractor.set_max_pages(num_max_pages)


    # encapsula invocação dos métodos do OCR
    mocker.patch('image.ocr.image_ocr.get_angle_text_orientation', return_value=0.0)
    get_text_mocked = mocker.patch('image.ocr.image_ocr.get_text_from_image', return_value='Olá Mundo')

    with tempfile.TemporaryDirectory() as tmp_dir:
        images = []
        for i in range(num_files):
            # cria um arquivo em disco para simular a leitura
            img_path = write_image(tmp_dir)
            img = open_image(img_path)
            images.append(img)
        
        content = extractor.extract_text_from_images(images)
        
        # junta todo o texto extraído
        text = ''
        for c in content:
            text += c['text'] + '\n'
    
    assert ( 'Olá Mundo\n' * num_max_pages ) == text
    assert get_text_mocked.called == True


def test_extract_text_from_pdf(mocker):
    """Testa a extração de texto de arquivos em PDF.
    O arquivo em PDF deve ser convertido em imagens e então aplicado o OCR.
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        # escreve o buffer em disco para simular um arquivo
        temp_pdf = write_fake_pdf(tmp_dir)        

        # escreve uma imagem em disco para simular a conversão do PDF em imagem
        img_path = write_image(tmp_dir)
        img = open_image(img_path)

        # encapsula as invocações de outros métodos
        convert_mocked = mocker.patch('conversor.files_to_image.convert_pdf', return_value=[img])
        get_text_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_images', return_value=[{'text': 'Olá Mundo', 'page': 1}])

        extractor = TextExtractor()
        text = extractor.extract_text_from_pdf(write_fake_pdf)

        assert convert_mocked.called == True
        assert get_text_mocked.called == True
        assert 'Olá Mundo' == text[0]['text']
        assert 1 == text[0]['page']


def test_extract_text_from_zip(mocker):
    """Testa a extração de texto de um arquivo compactado (zip).
    Os arquivos devem ser descompactados e ter seu texto extraído.
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        with tempfile.TemporaryDirectory() as tmp_dir_zip:
            # cria um pdf e uma imagem para serem compactados
            write_fake_pdf(tmp_dir)        
            write_image(tmp_dir)

            # cria um arquivo zip com o conteúdo do diretório
            zip_file = zip_dir(tmp_dir, tmp_dir_zip)
            
            # encapsula as invocações de outros métodos
            get_text_from_pdf_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_pdf', return_value=[{'text': 'Olá Mundo', 'page': 1}])
            get_text_from_image_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_images', return_value=[{'text': 'Olá Mundo', 'page': 1}])
            
            extractor = TextExtractor()
            text = extractor.extract_text_from_zip(zip_file)

            get_text_from_pdf_mocked.assert_called_once()
            get_text_from_image_mocked.assert_called_once()
            assert ('Olá Mundo\n' * 2) == text



def test_extract_text_from_nested_zip(mocker):
    """Testa extrair o texto de arquivos compactados aninhados.
    Os arquivos internos devem ser descompactados e devem ter seu texto extraído.
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        # cria um pdf e uma imagem para serem compactados
        write_fake_pdf(tmp_dir)        
        write_image(tmp_dir)

        with tempfile.TemporaryDirectory() as tmp_nested_dir:

            # cria um arquivo zip com o conteúdo do diretório
            zip_file = zip_dir(tmp_dir, tmp_nested_dir)
            
            # cria outra imagem para ser compactada junto com o zip
            write_image(tmp_dir)

            with tempfile.TemporaryDirectory() as tmp_dir_zip:

                zip_file = zip_dir(tmp_dir, tmp_nested_dir)
                    
                # encapsula as invocações de outros métodos
                get_text_from_pdf_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_pdf', return_value=[{'text': 'Olá Mundo', 'page': 1}])
                get_text_from_image_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_images', return_value=[{'text': 'Olá Mundo', 'page': 1}])
                
                extractor = TextExtractor()
                text = extractor.extract_text_from_zip(zip_file)

                get_text_from_pdf_mocked.assert_called_once()
                assert get_text_from_image_mocked.call_count == 2
                assert ('Olá Mundo\n' * 3) == text


def test_extract_text_from_empty_zip(mocker):
    """Testa extrair o texto de um arquivo zip vazio.
    Deve ser retornado uma string vazia
    """
    with tempfile.TemporaryDirectory() as tmp_dir:
        zip_file = create_empty_zip(tmp_dir)

        get_text_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_file', return_value='Olá Mundo')
        
        extractor = TextExtractor()
        text = extractor.extract_text_from_zip(zip_file)

        get_text_mocked.assert_not_called() # a conversão para texto não é invocada
        assert '' == text

def test_extract_text_guessing_image_mime(mocker):
    """Testa extrair o texto de uma imagem em disco descobrindo o mime do arquivo.
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        # escreve uma imagem em disco
        img_path = write_image(tmp_dir)

        # mock do método que deve ser invocado para a extração do mime em questão
        get_text_from_image_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_images', return_value=[{'text': 'Olá Mundo', 'page': 1}])
        
        extractor = TextExtractor()
        content = extractor.extract_text_from_file(img_path)
        
        get_text_from_image_mocked.assert_called_once()
        
        assert 'Olá Mundo\n' == content
        
        
def test_extract_text_guessing_pdf_mime(mocker):
    """Testa extrair o texto de um PDF em disco descobrindo o mime do arquivo.
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        # escreve uma imagem em disco
        img_path =  write_fake_pdf(tmp_dir)

        get_text_from_pdf_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_pdf', return_value=[{'text': 'Olá Mundo', 'page': 1}])
        
        extractor = TextExtractor()
        content = extractor.extract_text_from_file(img_path)
        
        get_text_from_pdf_mocked.assert_called_once()
        assert 'Olá Mundo\n' == content



def test_extract_text_guessing_zip_mime(mocker):
    """Testa extrair o texto de um zip em disco descobrindo o mime do arquivo.
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        with tempfile.TemporaryDirectory() as tmp_dir_zip:
            # cria um pdf e uma imagem para serem compactados
            write_fake_pdf(tmp_dir)        
            write_image(tmp_dir)

            # cria um arquivo zip com o conteúdo do diretório
            zip_file = zip_dir(tmp_dir, tmp_dir_zip)
            
            # encapsula as invocações de outros métodos
            get_text_from_pdf_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_pdf', return_value=[{'text': 'Olá Mundo', 'page': 1}])
            get_text_from_image_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_images', return_value=[{'text': 'Olá Mundo', 'page': 1}])
            
            extractor = TextExtractor()
            text = extractor.extract_text_from_file(zip_file)

            get_text_from_pdf_mocked.assert_called_once()
            get_text_from_image_mocked.assert_called_once()
            assert ('Olá Mundo\n' * 2) == text


def test_extract_text_from_unknown_mime():
    """Testa extrair o texto de um arquivo que o extrator não conhece.
    Deve ser retornado uma string vazia
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        # escreve um arquivo com extensão não conhecida pelo extrator
        file_path = os.path.join(tmp_dir, '{}.{}'.format(generate_random_string(), 'xpto'))
        with open(file_path, 'w+') as output:
            output.write('TESTE') 

        extractor = TextExtractor()
        text = extractor.extract_text_from_file(file_path)

        assert '' == text


def test_extract_text_from_unsuported_mime():
    """Testa extrair o texto de um arquivo que o extrator reconhece, mas não oferece suporte.
    Deve ser retornado uma string vazia
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        # escreve um arquivo com extensão não conhecida pelo extrator (.txt)
        file_path = os.path.join(tmp_dir, '{}.{}'.format(generate_random_string(), '.txt'))
        with open(file_path, 'w+') as output:
            output.write('TESTE') 

        extractor = TextExtractor()
        text = extractor.extract_text_from_file(file_path)

        assert '' == text


def test_extract_text_from_file_with_encapsuled_method(mocker):
    """Testa a extração de texto através de uma chamada do método 'transform' que é o encapsulamento do métoro 'extract_text_from_file'.
    O método 'extract_text_from_file' deve ser invocado.
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        img_path = write_image(tmp_dir)
    
        get_text_mocked = mocker.patch('text.feature_extraction.text_extractor.TextExtractor.extract_text_from_file', return_value='Olá Mundo')


        extractor = TextExtractor()
        text = extractor.transform(img_path)

        get_text_mocked.assert_called_once()
        assert 'Olá Mundo' == text
