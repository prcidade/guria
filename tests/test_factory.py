from service.web import create_app, get_model

def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing


def test_model_loaded():
    """Testa a criação do modelo que será utilizado pelo sistema
    """
    model = get_model()
    
    assert model is not None