import pytest
from unittest.mock import patch

import numpy as np
from service.classificador.ClassificadorDocumento import ClassificadorDocumento

import os
import tempfile
from PIL import Image

@patch('model.tiny_rna.TinyRNA')
@patch('text.feature_extraction.feature_extractor.TfidfFileExtractor')
def test_instanciate_classificador(model, extractor):
    """Testa a instanciação do classficador.
    O Classificador é instanciado utilizando um modelo preditor e um extrator de características.
    """
    classificador = ClassificadorDocumento(model, extractor)

    assert type(classificador) == ClassificadorDocumento

@patch('text.feature_extraction.feature_extractor.TfidfFileExtractor')
def test_classify(extractor):
    """Testa a classificação das caracteristicas extraídas de um único documento.
    Deve ser devolvido um dicionario contendo a chave 'max' que representa a classificação de probabilidade máxima
    e a chave 'all' que contém a classificação para todas as classes conhecidas.
    A classificação é dada através da predição do modelo. 
    """

    features = [ [0, 1, 2] ]
    with patch('model.tiny_rna.TinyRNA') as MockedModel:
        model = MockedModel.return_value
        model.decode_predictions.return_value = [{
            'all': [ {'label': 'foo', 'probability': 0.1}, {'label': 'bar', 'probability': 0.9} ],
            'max': { 'label': 'bar', 'probability': 0.9 }
        }]

        classificador = ClassificadorDocumento(model, extractor)
        predictions = classificador.classify(features)

        assert 'max' in predictions
        assert 'all' in predictions
        model.predict.assert_called_once()

@patch('model.tiny_rna.TinyRNA')
@patch('text.feature_extraction.feature_extractor.TfidfFileExtractor')
def test_classify_file(model, extractor):
    """Testa a classificação de um documento salvo em disco.
    O classificador deve invocar a extração de características e a predição do modelo base.
    """

    classificador = ClassificadorDocumento(model, extractor)

    with tempfile.TemporaryDirectory() as tmp_dir:
        # salva uma imagem em disco para a classificação
        img = Image.new('RGB', (5, 5))
        image_path = os.path.join(tmp_dir, 'img_test.jpg')
        img.save(image_path, "JPEG")

        classificador.classify_file(image_path)

        extractor.transform.assert_called_once()
        model.predict.assert_called_once()

