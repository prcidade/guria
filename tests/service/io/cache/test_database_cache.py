import pytest
from service.io.cache.database_cache import Cache
from service.web.db import get_db
import time

def test_store_cache_value(app):
    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)
        cache.put('foo', 'bar')
        assert cache.get('foo') == 'bar'

def test_store_cache_ttl(app):

    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)
        cache.put('foo', 'bar', ttl=1)
        value = cache.get('foo')
        assert value == 'bar'
        time.sleep(1)
        assert cache.get('foo') is None

def test_cache_has_value(app):

    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)
        cache.put('foo', 'bar')

        assert cache.has('foo') == True
        assert cache.has('spam') == False

def test_cache_forget(app):

    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)
        cache.put('foo', 'bar')
        cache.forget('foo')

        assert cache.has('foo') == False

def test_cache_replace_value(app):

    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)
        cache.put('foo', 'bar')
        cache.put('foo', 'baz')

        assert cache.get('foo') == 'baz'


def test_get_default_value_from_cache(app):
    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)

        assert cache.get('foo', 'default') == 'default'


def test_get_default_value_with_function_from_cache(app):
    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)

        assert cache.get('foo', lambda x : 'default') == 'default'


def test_flush_cache(app):
    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)
        cache.put('foo', 'bar')
        cache.put('spam', 'eggs')

        cache.flush()

        assert cache.has('foo') == False
        assert cache.has('spam') == False


def test_get_cached_value_or_store_in_cache(app):
    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)
        ttl = 10
        value = cache.remember('foo', lambda x : 'default', ttl)

        assert value == 'default'
        assert cache.get('foo') == 'default'


def test_ttl_of_cache_remember(app):
    with app.app_context():
        db_connection = get_db()
        
        cache = Cache(db_connection)
        ttl = 1
        value = cache.remember('foo', lambda x : 'default', ttl)
        time.sleep(1)
        
        assert value == 'default'
        assert cache.get('foo') is None







