import pytest

from service.io.cache.cache_driver import get_cache_driver, resolve_cache_driver, get_current_cache_driver

def test_resolve_cache_driver_by_default_configuration(app):
    """Testa a resolução do driver de cache padrão.
    Deve ser retornada uma instância do driver de cache.

    Args:
        app (object): Contexto da aplicação flask
    """
    with app.app_context():
        cache_driver = resolve_cache_driver()
    
        assert cache_driver is not None

def test_resolve_cache_driver_by_name(app):
    """Testa a resolução de um driver de cache pelo nome explicito.
    Deve ser retornado uma instância do driver de cache.

    Args:
        app (object): Contexto da aplicação flask
    """
    with app.app_context():
        cache_driver = resolve_cache_driver('database')
    
        assert cache_driver is not None

def test_resolve_unknown_cache_driver(app):
    """Testa a resolução de um driver de cache desconhecido.
    Deve ser retornado None.

    Args:
        app (object): Contexto da aplicação flask
    """
    with app.app_context():
        cache_driver = resolve_cache_driver('')
    
        assert cache_driver is None


def test_get_cache_driver_instance(app):
    """Testa recuperar a instância global do driver de cache.
    Após a resolução do driver de cache, uma instância do mesmo deve ser armazenada no dicionário global.

    Args:
        app (object): Contexto da aplicação flask
    """
    with app.app_context():
        
        cache_driver = get_current_cache_driver()
        assert cache_driver is None

        cache_driver = get_cache_driver()
        assert cache_driver is get_current_cache_driver()
        

def test_cache_flush_command(runner):
    """Testa o comando de limpar o cache da aplicação.
    """
    result = runner.invoke(args=['cache:flush'])
    assert 'Cache limpo' in result.output
