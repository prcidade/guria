import pytest
import io
import os
import tempfile
from service.io.upload.uploaded_file import UploadedFile, store_file_tmp_dir, store_file_in_folder

class FakeFile:
    """
        Simula um arquivo enviado pelo usuário
    """
    def __init__(self, filename='teste.txt'):
        self.filename = filename
        self.filepath = ''

    def save(self, path):
        self.filepath = path
        with open(path, 'w+') as f:
            f.write('teste')

    def delete(self):
        os.remove(self.filepath)

def test_store_file_in_folder():
    """Testa armazenar um arquivo em um diretório
    """
    file = FakeFile()

    with tempfile.TemporaryDirectory() as tmp_dir:
        store_file_in_folder(file, tmp_dir)

        assert os.path.exists(tmp_dir)

        file.delete()

def test_store_file_in_tmp_dir():
    """Testa armazenar um arquivo em um diretório temporário
    """

    file = FakeFile()

    dirpath, file_path = store_file_tmp_dir(file)
    
    # verifica se foi criado o diretório temporário
    assert os.path.exists(dirpath)
    assert file_path == file.filepath

    file.delete()

    assert not os.path.exists(file_path)
    os.removedirs(dirpath)

def test_save_uploaded_file():
    """Testa salvar o arquivo em um diretório informado
    """
    file = FakeFile()
    uploaded_file = UploadedFile(file)

    with tempfile.TemporaryDirectory() as tmp_dir:
        file_path = uploaded_file.save(tmp_dir)

        assert file_path == uploaded_file.get_file_path()
        assert tmp_dir == uploaded_file.get_file_folder()
        assert os.path.exists(file_path)


def test_save_uploaded_temporary():
    """Testa salvar o arquivo em um diretório temporário
    """
    
    file = FakeFile()
    uploaded_file = UploadedFile(file)


    tmp_dir, file_path = uploaded_file.save_temporary()

    assert os.path.exists(tmp_dir)
    assert os.path.isdir(tmp_dir)
    assert os.path.exists(file_path)
    assert os.path.isfile(file_path)
    assert uploaded_file.is_tmp_file() == True
    assert uploaded_file.get_file_folder() == tmp_dir
    assert uploaded_file.get_file_path() == file_path

    uploaded_file.delete()
    assert not os.path.exists(tmp_dir)


def test_overlap_temporary_with_another_temporary_file():
    """Testa armazenar novamente um arquivo temporário
    """
    
    file = FakeFile()
    uploaded_file = UploadedFile(file)

    tmp_dir, file_path = uploaded_file.save_temporary()
    tmp_dir2, file_path2 = uploaded_file.save_temporary()

    assert not os.path.exists(file_path)
    assert not os.path.exists(tmp_dir)
    assert uploaded_file.get_file_folder() == tmp_dir2
    assert uploaded_file.get_file_path() == file_path2


def test_overlap_temporary_saving_file():
    """Testa salvar um arquivo em um diretório permanente após salvar o arquivo temporário
    O arquivo temporário deve ser excluído e o novo diretório deve ser persistido
    """

    file = FakeFile()
    uploaded_file = UploadedFile(file)

    tmp_dir, file_path = uploaded_file.save_temporary()

    with tempfile.TemporaryDirectory() as tmp_dir2:
        file_path2 = uploaded_file.save(tmp_dir2)
        
        assert not uploaded_file.is_tmp_file()
        assert uploaded_file.get_file_folder() != tmp_dir
        assert uploaded_file.get_file_path() != file_path

    # diretório temporário foi apagado
    assert not os.path.exists(tmp_dir)

def test_remove_already_removed():
    """Testa excluir um arquivo que já foi removido do disco
    """

    file = FakeFile()
    uploaded_file = UploadedFile(file)
    
    tmp_dir, file_path = uploaded_file.save_temporary()

    os.remove(file_path)
    os.removedirs(tmp_dir)

    uploaded_file.delete()
    assert uploaded_file.get_file_path() is None
    assert uploaded_file.get_file_folder() is None


def test_extension_allowed():
    """Testa a verificação de uma extensão permitida para o arquivo
    """

    file = FakeFile()
    uploaded_file = UploadedFile(file)
    
    assert uploaded_file.is_extension_allowed() == True


def test_extension_not_allowed():
    """Testa a verificação de uma extensão não permitida para o arquivo
    """

    file = FakeFile()
    uploaded_file = UploadedFile(file)
    
    assert uploaded_file.is_extension_allowed(['pdf']) == False

