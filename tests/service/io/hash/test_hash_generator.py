import pytest
from service.io.hash.hash_generator import HashFile
import tempfile
import os


def test_hash_file_as_string():
    """Testa se o retorno da função hash é uma string
    """

    with tempfile.TemporaryFile('w+b') as file:
        file.write(b'teste')
        hash_file = HashFile()

        assert type(hash_file.hash(file)) == str

def test_hash_file_values_diffent_to_files():
    """Testa se o valor da função hash é diferente para arquivos diferentes
    """

    with tempfile.NamedTemporaryFile('w+b') as file1:
        with tempfile.NamedTemporaryFile('w+b') as file2:

            file1.write(b'file 1')
            file2.write(b'new file')


            hash_file = HashFile()

            hash_value_file1 = hash_file.hash(file1)   
            hash_value_file2 = hash_file.hash(file2)   

            assert hash_value_file1 != hash_value_file2


def test_hash_file_values_equals_same_content():
    """Testa se o valor da função hash é igual para o mesmo conteúdo
    """

    with tempfile.TemporaryFile('w+b') as file1:
        with tempfile.TemporaryFile('w+b') as file2:

            file1.write(b'file 1')
            file2.write(b'file 1')        

            hash_file = HashFile()

            hash_value_file1 = hash_file.hash(file1)   
            hash_value_file2 = hash_file.hash(file2)   

            assert hash_value_file1 == hash_value_file2


def test_hash_file_values_different_algorithms():
    """Testa se o valor hash é distinto ao mudar o algoritmo hash aplicado nos arquivos
    """


    with tempfile.TemporaryFile('w+b') as file1:
        with tempfile.TemporaryFile('w+b') as file2:

            file1.write(b'file 1')
            file2.write(b'file 1')
            
            hash_file = HashFile('md5')
            hash_value_file1 = hash_file.hash(file1)

            hash_file = HashFile('sha1')
            hash_value_file2 = hash_file.hash(file2)   

            assert hash_value_file1 != hash_value_file2


def test_hash_stored_file():


    hash_file = HashFile()
    with tempfile.TemporaryDirectory() as tmpdirname:
        file_path = os.path.join(tmpdirname, 'teste.txt')

        with open(file_path, 'w+b') as file1:   
            file1.write(b'file 1')
            file1.seek(0)
            hash_value_file1 = hash_file.hash(file1)

        hash_value = hash_file.hash(file_path)
        assert hash_value == hash_value_file1