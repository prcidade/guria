import pytest
import numpy as np
from model.tiny_rna import TinyRNA, N_FEATURES, N_CLASSES, init_base_model


def test_init_model():
    model = init_base_model()
    
    assert isinstance(model, TinyRNA)

def test_instantiate_tiny_rna():
    """Verifica a instanciação da RNA pequena
    A instanciação do modelo propriamente dita deve ser lazy então não deve ser carregado na instanciação
    """
    rna = TinyRNA()

    assert rna is not None
    assert rna.model is None

def test_build_model():
    """Testa a construção da RNA com os parâmetros informados
    A rede deve ter a dimensão da entrada informada e o número de classes de saída
    Inicialmente o modelo deve ser None e após construído deverá ter a rede construída
    """

    rna = TinyRNA()
    initial_model = rna.model

    input_dim = 128
    n_classes = 2

    rna.build(input_dim, n_classes)
    
    assert initial_model is None
    assert rna.model is not None
    assert rna.input_dim == input_dim
    assert rna.n_classes == n_classes


def test_load_tiny_rna_model():
    """Testa carregar um modelo pré-treinado a partir do nome
    O modelo deve ser carregado com as dimensões de entrada e saída definidos
    """

    rna = TinyRNA()
    rna.build(N_FEATURES, N_CLASSES)
    loaded = rna.load('tiny_rna')

    assert rna.model is not None
    assert loaded == True
    assert rna.input_dim == N_FEATURES
    assert rna.n_classes == N_CLASSES

def test_load_unknown_model():
    """Testa carregar um modelo desconhecido pelo nome
    Ao utilizar um nome desconhecido os pesos não deverão ser carregados e uma exceção deverá ser lançada
    """
    with pytest.raises(Exception):
        rna = TinyRNA()
        rna.build(N_FEATURES, N_CLASSES)
        loaded = rna.load('foo_bar')


def test_load_tiny_rna_encoder():
    """Os modelos pré-treinados devem ter um codificador para preparar poder traduzir a saída do modelo para um formato
    utilizável.
    Os codificadores são carregados de forma lazy, ficando disponíveis apenas após o carregamento da rede ou de seu 
    carregamento explicito
    """
    rna = TinyRNA()
    initial_encoder = rna.encoder

    rna.load_encoder('tiny_rna')

    assert initial_encoder is None
    assert rna.encoder is not None


def test_load_unknown_encoder():
    """Testa carregar um codificador pré-treinado por um nome não existênte
    O codificicador não deve ser carregado e uma exceção deve ser gerada
    """
    with pytest.raises(Exception):

        rna = TinyRNA()
        initial_encoder = rna.encoder
        rna.load_encoder('foo_bar')


def test_predict_unload_model():
    """Testa realizar a predição em um modelo não carregado
    """
    with pytest.raises(Exception):

        rna = TinyRNA()
        samples = np.random.rand(N_CLASSES)

        rna.predict(samples)
    
def test_predict():
    """Testa a predição da rede
    Deve retornar um array com o número de amostras e para cada amostra o número de classes correspondentes
    """
    rna = TinyRNA()
    rna.build(N_FEATURES, N_CLASSES)
    rna.load('tiny_rna')
    samples = np.random.rand(1, N_FEATURES)

    y = rna.predict(samples)

    assert y.shape[0] == samples.shape[0]
    assert y.shape[1] == N_CLASSES



def test_decode_predictions():
    """Testa a decodificação das predições para a labels amigáveis
    O resultado decodificado deve conter o resultado probabilistico de cada classe individual
    Também deve conter uma chave para o resultado de probabilidade máxima, trazendo a label e a probabilidade
    """
    rna = TinyRNA()
    rna.build(N_FEATURES, N_CLASSES)
    rna.load('tiny_rna')
    samples = np.random.rand(1, N_FEATURES)

    y = rna.predict(samples)
    predictions = rna.decode_predictions(y)
    
    assert len(predictions) == 1
    assert 'all' in predictions[0] # verifica se há chave com resultado de todas as predições
    assert 'max' in predictions[0] # verifica se há chave com a probabilidade máxima


def test_decode_prediction():
    """Testa a decodificação de uma única predição para uma label amigável
    O resultado deve trazer uma lista de dicionários contedo a label em formáto amigável e a probabilidade para a mesma
    """
    rna = TinyRNA()
    rna.build(N_FEATURES, N_CLASSES)
    rna.load('tiny_rna')
    samples = np.random.rand(1, N_FEATURES)

    y = rna.predict(samples)
    prediction = rna.decode_prediction(y[0])

    assert len(prediction) == N_CLASSES
    assert 'label' in prediction[0] # verifica se há label na predição
    assert 'probability' in prediction[0] # verifica se há probabilidade na predição


def test_get_base_model():
    """Testa recuperar o modelo base encapsulado pela classe
    """
    rna = TinyRNA()

    # Deve iniciar como None
    base_model = rna.get_base_model()
    assert base_model is None

    # Deve ser constuido um modelo base
    rna.build(N_FEATURES, N_CLASSES)
    base_model = rna.get_base_model()
    assert base_model is not None


