import pytest
import io
import conversor
import image
import json
import logging
import service
from service.web.guria import ALLOWED_EXTENSIONS_DEMO_CLASSIFICACAO

def test_home(client):
    response = client.get('/')
    assert 'guria' in response.data.decode('utf8').lower()


def test_demo(client, mocker):
    data = dict(
        documento=(io.BytesIO(b'Arquivo'), 'documento.pdf')
    )

    class FakeModel:
        called = False 
    
    mocker.patch('service.io.upload.uploaded_file.UploadedFile.save_temporary', return_value=('/tmp/', '/tmp/documento.pdf'))
    mocker.patch('service.io.upload.uploaded_file.UploadedFile.delete')

    return_classification = [{'max': {'label': 'Teste', 'prob': 0.8}}]
    mocker.patch('service.classificador.ClassificadorDocumento.ClassificadorDocumento.classify_file', return_value=return_classification)


    response = client.post('/documento/classificar/demo/', content_type='multipart/form-data', data=data)

    data = json.loads(response.data)
    assert 'success' in data
    assert data['success'] == True



def test_demo_extension_not_allowed(client, mocker):
    """Simula um envio de um documento com extensão não permitida
    """
    data = dict(
        documento=(io.BytesIO(b'Arquivo'), 'documento.tif')
    )

    mocker.patch('logging.exception')
    response = client.post('/documento/classificar/demo/', content_type='multipart/form-data', data=data)

    logging.exception.assert_called_once()
    data = json.loads(response.data)
    assert 'success' in data
    assert data['success'] == False
