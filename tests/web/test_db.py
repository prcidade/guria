import pytest

from service.web.db import get_db, get_current_connection, close_db


def test_get_close_db(app):
    with app.app_context():
        db = get_current_connection()
        assert db is None

        db = get_db()
        assert db is get_current_connection()

        close_db()
        db = get_current_connection()
    
        assert db is None


def test_db_init_command(runner, monkeypatch):
    class Recorder(object):
        called = False

    def fake_init_db():
        Recorder.called = True

    monkeypatch.setattr('service.web.db.init_db', fake_init_db)
    result = runner.invoke(args=['init-db'])
    assert 'inicializado' in result.output
    assert Recorder.called
