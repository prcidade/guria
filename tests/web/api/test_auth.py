import pytest
from service.web.api.client import Client
import json

def init_app_client(name="teste", secret="123456"):
    app_client = Client()
    app_client.create(name, secret)

    return app_client
  
def test_auth_client_without_secret(client, app):
    with app.app_context():
        app_client = init_app_client()
        response = client.post('/api/authenticate/', data={ 'client_id': app_client.id })
        data = json.loads(response.data)

        assert 'message' in data
        assert 'O segredo do cliente é obrigatório' in data['message']

def test_auth_client_without_client_id(client, app):
    with app.app_context():
        app_client = init_app_client()
        response = client.post('/api/authenticate/', data={ 'secret': 'SECRET TEST' })
        data = json.loads(response.data)

        assert 'message' in data
        assert 'O ID do cliente é obriatório' in data['message']


def test_auth_wrong_credentials(client, app):

    with app.app_context():
        secret = '123456'
        app_client = init_app_client(secret=secret)

        response = client.post('/api/authenticate/', data={ 'client_id': app_client.id, 'secret': 'WRONG SECRET' })
        data = json.loads(response.data)

        assert 'message' in data
        assert 'Credenciais inválidas' in data['message']
    
def test_auth_client(client, app):

    with app.app_context():
        client_secret = '123456'
        app_client = init_app_client(secret=client_secret) 

    response = client.post('/api/authenticate/', data={
        'client_id': app_client.id, 
        'secret': client_secret
    })
    
    data = json.loads(response.data)

    assert 'token' in data
