import pytest
import sqlalchemy
from service.web.api.client import Client


def test_create_client(app):
    with app.app_context():
        client = Client()
        client.create('teste', '123456')

        client_in_db = client.find_by_name('teste')

        assert client_in_db.name == 'teste'

def test_create_duplicated_client(app):
    with app.app_context():
        client = Client()
        with pytest.raises(sqlalchemy.exc.IntegrityError) as e:
            client.create('teste', '123456')
            client.create('teste', '123456')
        

        
        assert len(client.all()) == 1

def test_find_uncreated_client(app):
    with app.app_context():
        client = Client()
        client_in_db = client.find_by_name('aaaa')

        assert client_in_db.name is None

def test_delete_client(app):
    with app.app_context():
        client = Client()
        client.create('teste', '123456')

        client.delete_by_name('teste')

        client_in_db = client.find_by_name('teste')
        assert client_in_db.name is None


def test_authenticate_client(app):
    with app.app_context():
        client = Client()
        client.create('teste', '123456')

        new_client = Client()
        assert new_client.authenticate(client.id, '123456') == True

def test_authenticate_client_not_found(app):
    """Testa a authenticação de um cliente que não é encontrado
    O cliente não encontrado não pode ser autenticado
    """
    with app.app_context():
        client = Client()
        assert client.authenticate(0, 'teste') == False

def test_find_client_by_id(app):

    with app.app_context():
        client = Client()
        client.create('teste', '123456')

        new_client = Client()
        assert new_client.find(client.id).id == client.id

def test_create_client_command(runner, monkeypatch):
    class Recorder(object):
        called = False

    def fake_create_client(self, name, password, description=None):
        Recorder.called = True

    monkeypatch.setattr('service.web.api.client.Client.create', fake_create_client)
    result = runner.invoke(args=['create-client'], input='test_client\nTeste Client\n123456\n123456\n')
    assert 'criado' in result.output
    assert Recorder.called
