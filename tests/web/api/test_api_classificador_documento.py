import pytest
from service.web.api.client import Client
import json

import io

def create_file_in_memory(name='teste.jpg'):
    """Cria um buffer em memória para simular o envio de um arquivo
    """
    return ( io.BytesIO(b"abcdef"), name )

def init_app_client(name="teste", secret="123456"):
    """Registra um cliente na API"""
    app_client = Client()
    app_client.create(name, secret)

    return app_client


def authenticate_client(client, client_id, secret):
    """Autentica um cliente na API.
    """
    response = client.post('/api/authenticate/', data={
        'client_id': client_id, 
        'secret': secret
    })
    
    data = json.loads(response.data)
    
    return data


def test_post_file_without_authenticate(client, app):
    """Testa enviar um arquivo sem informar o client cadastrado.
    Deve ser retornada uma mensagem de erro.
    """
    with app.app_context():

        payload = {}
        payload['documento'] = create_file_in_memory()

        response = client.post('/documento/classificar', data=payload)
        data = json.loads(response.data)

        assert 'message' in data
        assert 'Token não informado' in data['message']

def test_post_file_with_invalid_token(client, app):
    """Testa enviar um arquivo com um token inválido.
    Deve ser retornado uma mensagem de erro para o usuário.
    """
    with app.app_context():

        with app.test_request_context():
            payload = {}
            payload['documento'] = create_file_in_memory()

            response = client.post('/documento/classificar', data=payload, headers={'x-access-token': 'abc'})
            data = json.loads(response.data)

        assert 'message' in data
        assert 'Cliente não identificado' in data['message']


def test_post_invalid_file_extension(client, app):
    """Testa enviar um arquivo com extensão inválida.
    Deve ser retornada uma mensagem de erro.
    """

    with app.app_context():
        # Cria um cliente na API
        app_client = init_app_client(secret='123456')
        token = authenticate_client(client, app_client.id, '123456')['token']

        with app.test_request_context():
            payload = {}
            payload['documento'] = create_file_in_memory('teste.xpto')

            response = client.post('/documento/classificar', data=payload, headers={'x-access-token': token})
            data = json.loads(response.data)

        assert 'message' in data
        assert 'Extensão não permitida' in data['message']


def test_classify_file(client, app, mocker):
    """Testa a classificação de um documento.
    Deve ser invocado o método de classificação.
    """

    with app.app_context():
        # Cria um cliente na API
        app_client = init_app_client(secret='123456')
        token = authenticate_client(client, app_client.id, '123456')['token']

        with app.test_request_context():
            payload = {}
            payload['documento'] = create_file_in_memory()

            mocker.patch('text.feature_extraction.feature_extractor.TfidfFileExtractor.load')
            mocker.patch('service.web.guria.get_model')
            mocked_classify = mocker.patch('service.classificador.ClassificadorDocumento.ClassificadorDocumento.classify_file', return_value={})

            response = client.post('/documento/classificar', data=payload, headers={'x-access-token': token})
            data = json.loads(response.data)

        assert 'success' in data
        assert data['success'] == True
        mocked_classify.assert_called_once()