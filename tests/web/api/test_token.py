import pytest
from service.web.api.client import Client
from service.web.api import token_handler 
import json

def init_app_client(name="teste", secret="123456"):
    """Cria um cliente no banco de dados com um nome e uma senha

    Args:
        name (str, optional): Nome do Client. Defaults to "teste".
        secret (str, optional): Senha do client. Defaults to "123456".

    Returns:
        Client : instância do Cliente cadastrado no banco de dados
    """
    app_client = Client()
    app_client.create(name, secret)

    return app_client


@token_handler.token_required
def fake_request(client=None):
    """Simula um request que deve ser feito com o token informado no header
    Se o token estiver correto o client será passado por parâmetro para a função

    Args:
        client (Client, optional): Client registrado na aplicação. Defaults to None.

    Returns:
        Dict: dicionário com o cliente encontrado
    """
    return {'client': client}

def test_encode_token(app):
    """Testa a codificação do token com dados para serem enviados

    Args:
        app (App): Aplicação
    """
    with app.app_context():
        encoded_token = token_handler.encode({'data': 'test'})
        decoded_data = token_handler.decode(encoded_token)

    assert 'exp' in decoded_data
    assert 'data' in decoded_data


def test_get_client_by_token(app):
    """Testa recuperar um cliente a partir de um token codificado
    O cliente é recuperado através da chave "client_id" e deve existir no banco de dados

    Args:
        app (App): Aplicação
    """
    with app.app_context():
        app_client = init_app_client()

        encoded_token = token_handler.encode({'client_id': app_client.id})
        client_founded = token_handler.get_client_by_token(encoded_token)

        assert client_founded is not None
        assert client_founded.id == app_client.id



def test_no_authorization_token_in_header(app):
    """Testa a realização de uma requisição sem o token de autenticação informado
    Reverá ser retornado um erro, informado que não há token informado
    Args:
        app (App): aplicação
    """

    with app.test_request_context():

        response = fake_request()

        response_data = json.loads(response[0].data)
        assert response_data['message'] == 'Token não informado'

def test_authorization_token_in_header(app):
    """Testa a localização de um client pelo token do header
    O cliente deve ser localizado pela chave 'client_id' 
    e se localizado será passado como parâmetro para a função do request

    Args:
        app (App): aplicação
    """

    with app.app_context():

        app_client = init_app_client()
        encoded_token = token_handler.encode({'client_id': app_client.id})

        with app.test_request_context(headers={'x-access-token': encoded_token}):

            response = fake_request()

            assert 'client' in response
            assert response['client'] is not None
            assert response['client'].id == app_client.id


def test_authorization_invalid_token_in_header(app):
    """Testa o envio de um token inválido no cabeçalho da requisição
    A requisição deve responder com um código de erro
    Nenhum cliente deve ser encontrado

    Args:
        app (App): Aplicação
    """

    with app.test_request_context(headers={'x-access-token': 'abc'}):
        
        response = fake_request()

        response_data = json.loads(response[0].data)
        assert response_data['message'] == 'Cliente não identificado'


def test_authorization_client_not_found_by_token_in_header(app):
    """Testa recuperar um client que não está no banco de dados
    O cliente não será encontrado e a requisição retornará um erro

    Args:
        app (App): Aplicação
    """

    with app.app_context():

        encoded_token = token_handler.encode({'client_id': -1})

        with app.test_request_context(headers={'x-access-token': encoded_token}):

            response = fake_request()

            response_data = json.loads(response[0].data)
            assert response_data['message'] == 'Cliente não identificado'
