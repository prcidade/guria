import sys
import logging
logging.basicConfig(filename="logs/guria.log")

from service.web import create_app

app = create_app()
